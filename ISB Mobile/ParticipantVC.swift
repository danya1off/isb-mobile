//
// Created by  Simberg on 9/28/17.
// Copyright (c) 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD

class ParticipantVC: UITableViewController {

    var navigationTitle: String?
    var isParticipants = true
    var participants = [Participant]()
    var insurers = [Insurer]()
    var participantInfo = ParticipantInfo()
    
    private var menuManager: MenuManager!

    override func viewDidLoad() {
        super.viewDidLoad()
        menuManager = MenuManager(participantInfo: participantInfo, view: self)
        
        //create main UI
        createUI()

    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isParticipants {
            return participants.count
        } else {
            return insurers.count
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.participantCell) as! ParticipantCell
        
        if isParticipants {
            let participant = participants[indexPath.row]
            cell.configure(withName: participant.name, andTin: participant.tin)
        } else {
            let insurer = insurers[indexPath.row]
            cell.configure(withName: insurer.name, andTin: insurer.tin)
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isParticipants {
            let participant = participants[indexPath.row]
            if (participant.insurers.count == 1) {
                let insurer = participant.insurers.first!
                participantInfo.insurerName = insurer.name
                participantInfo.insurerTin = insurer.tin
                participantInfo.participantName = participant.name
                participantInfo.participantTin = participant.tin
                participantInfo.hasAsanImza = participant.hasAsanImza
                ApplicationManager.saveParticipantInfo(withInfo: participantInfo)
                menuManager.openOperation(withCode: participantInfo.operation.code, participantName: participantInfo.participantName, insurerName: participantInfo.insurerName)
            } else {
                let viewController = ParticipantVC()
                viewController.navigationTitle = "Sığortaçılar"
                viewController.isParticipants = false
                participantInfo.participantName = participant.name
                participantInfo.participantTin = participant.tin
                participantInfo.hasAsanImza = participant.hasAsanImza
                viewController.participantInfo = participantInfo
                viewController.insurers = participant.insurers
                navigationController?.pushViewController(viewController, animated: true)
            }
        } else {
            let insurer = insurers[indexPath.row]
            participantInfo.insurerName = insurer.name
            participantInfo.insurerTin = insurer.tin
            ApplicationManager.saveParticipantInfo(withInfo: participantInfo)
            menuManager.openOperation(withCode: participantInfo.operation.code, participantName: participantInfo.participantName, insurerName: participantInfo.insurerName)
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 90
        } else {
            return 55
        }
    }
    
    @objc func close() {
        ApplicationManager.redirectToMainMenu()
    }
    
}
