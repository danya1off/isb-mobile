//
//  MenuVC+Ext.swift
//  ISB Mobile
//
//  Created by Jeyhun Danyalov on 9/19/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

extension MenuVC {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func createUI() {
        
        CustomProgressHUD.customize()
        
        view.backgroundColor = UIColor.white
        createHeaderView()
        createIconImage()
        createSettingsButton()
        createContractsButton()
        createTableView()
        createButtonsSplitter()
        createButtonsTopView()
        
    }

    private func createHeaderView() {
        
        view.addSubview(headerView)
        headerView.backgroundColor = UIColor.mainAppColor
        headerView.translatesAutoresizingMaskIntoConstraints = false
        
        // init constraints
        headerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        headerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        headerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        headerView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.36).isActive = true
        
    }

    private func createIconImage() {
        
        view.addSubview(iconImage)
        iconImage.translatesAutoresizingMaskIntoConstraints = false
        iconImage.image = UIImage(named: "Logo")
        
        participantLbl.translatesAutoresizingMaskIntoConstraints = false
        participantLbl.textColor = UIColor.white
        participantLbl.text = "İcbari Müqavilə Sistemi"
        participantLbl.font = UIFont(name: Constants.helveticaRegular, size: 17)
        view.addSubview(participantLbl)
        
        // init constraints
        iconImage.topAnchor.constraint(equalTo: view.topAnchor, constant: 50).isActive = true
        iconImage.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.27).isActive = true
        iconImage.heightAnchor.constraint(equalTo: iconImage.widthAnchor, multiplier: 1).isActive = true
        iconImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true

        participantLbl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        participantLbl.topAnchor.constraint(equalTo: iconImage.bottomAnchor, constant: 15).isActive = true
        
    }

    private func createSettingsButton() {
        
        view.addSubview(settingsBtn)
        settingsBtn.translatesAutoresizingMaskIntoConstraints = false
        settingsBtn.addTarget(self, action: #selector(MenuVC.settingsAction), for: .touchUpInside)
        settingsBtn.titleLabel?.font = UIFont(name: Constants.helveticaCondensed, size: 16)
        settingsBtn.setTitleColor(UIColor.mainTextColor, for: .normal)
        settingsBtn.backgroundColor = UIColor.white
        settingsBtn.setTitle("AYARLAR", for: .normal)
        
        // init constraint
        settingsBtn.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        settingsBtn.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        settingsBtn.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.055).isActive = true
        settingsBtn.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5).isActive = true
        
    }

    private func createContractsButton() {
        
        view.addSubview(contractsBtn)
        contractsBtn.translatesAutoresizingMaskIntoConstraints = false
        contractsBtn.addTarget(self, action: #selector(MenuVC.openMyContracts), for: .touchUpInside)
        contractsBtn.titleLabel?.font = UIFont(name: Constants.helveticaCondensed, size: 16)
        contractsBtn.setTitleColor(UIColor.mainTextColor, for: .normal)
        contractsBtn.backgroundColor = UIColor.white
        contractsBtn.setTitle("MÜQAVİLƏLƏR", for: .normal)
        
        // init constraint
        contractsBtn.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        contractsBtn.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        contractsBtn.heightAnchor.constraint(equalTo: settingsBtn.heightAnchor).isActive = true
        contractsBtn.widthAnchor.constraint(equalTo: settingsBtn.widthAnchor).isActive = true
        
    }

    private func createButtonsSplitter() {
        
        let splitter = UIView()
        view.addSubview(splitter)
        splitter.translatesAutoresizingMaskIntoConstraints = false
        splitter.backgroundColor = UIColor.mainTextColor
        
        // init constraints
        splitter.leftAnchor.constraint(equalTo: contractsBtn.leftAnchor).isActive = true
        splitter.centerYAnchor.constraint(equalTo: contractsBtn.centerYAnchor).isActive = true
        splitter.widthAnchor.constraint(equalToConstant: 1).isActive = true
        splitter.heightAnchor.constraint(equalTo: contractsBtn.heightAnchor, multiplier: 0.6).isActive = true
        
    }

    private func createButtonsTopView() {
        
        let topView = UIView()
        view.addSubview(topView)
        topView.translatesAutoresizingMaskIntoConstraints = false
        topView.backgroundColor = UIColor.mainTextColor
        
        // init constraints
        topView.bottomAnchor.constraint(equalTo: contractsBtn.topAnchor, constant: 1).isActive = true
        topView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        topView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        topView.heightAnchor.constraint(equalToConstant: 1).isActive = true
    }

    private func createTableView() {
        
        view.addSubview(tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorInset.left = 72
        tableView.separatorInset.right = 0
        tableView.tableFooterView = UIView()
        tableView.register(MenuCell.self, forCellReuseIdentifier: Constants.menuCell)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        
        // init constraints
        tableView.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 20).isActive = true
        tableView.bottomAnchor.constraint(equalTo: settingsBtn.topAnchor, constant: 1).isActive = true
        tableView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        
    }
    
}
