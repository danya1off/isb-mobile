//
//  UIViewController+Extension.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/24/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView
import SVProgressHUD

extension UINavigationController {
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}

extension UIViewController {
    
    func alert(withTitle title: String, message: String, showButton: Bool) -> UIAlertController {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if showButton {
            let alertAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(alertAction)
        }
        return alertController
        
    }
    
    func fireErrorAlert(withMessage message: String) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "HelveticaNeue", size: 18)!,
            kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
            kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
            showCloseButton: true
        )
        
        let alert = SCLAlertView(appearance: appearance)
        alert.showError("Xəta", subTitle: message, closeButtonTitle: "BAĞLA")
    }
    
    func fireErrorAlert(withMessage message: String, completion: @escaping() -> Void) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "HelveticaNeue", size: 18)!,
            kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
            kButtonFont: UIFont(name: "HelveticaNeue-Bold", size: 14)!,
            showCloseButton: false
        )
        
        let alert = SCLAlertView(appearance: appearance)
        alert.addButton("BAĞLA") {
            completion()
        }
        alert.showError("Xəta", subTitle: message)
    }
    
    func fireInfoAlert(withMessage message: String, showCloseButton: Bool, returnAlert: Bool) -> SCLAlertView? {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "HelveticaNeue", size: 18)!,
            kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
            kButtonFont: UIFont(name: "HelveticaNeue-Medium", size: 14)!,
            showCloseButton: showCloseButton
        )
        
        let alert = SCLAlertView(appearance: appearance)
        alert.showInfo("Məlumat", subTitle: message, closeButtonTitle: "BAĞLA")
        if returnAlert {
            return alert
        } else {
            return nil
        }
    }
    
    func fireSuccessAlert(withMessage message: String, completion: @escaping () -> Void) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "HelveticaNeue", size: 18)!,
            kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
            kButtonFont: UIFont(name: "HelveticaNeue-Medium", size: 14)!,
            showCloseButton: false
        )
        
        let alert = SCLAlertView(appearance: appearance)
        alert.addButton("BAĞLA") {
            
            completion()
            
        }
        alert.showSuccess("Məlumat", subTitle: message)
    }
    
    func fireEditAlert(withTitle title: String, phoneNumber number: String, completion: @escaping (String, String) -> Void) {
        
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: Constants.helveticaMedium, size: 14)!,
            kTextFont: UIFont(name: Constants.helveticaRegular, size: 13)!,
            kButtonFont: UIFont(name: Constants.helveticaCondensed, size: 14)!,
            showCloseButton: true
        )
        
        let getInsuredDataAlert = SCLAlertView(appearance: appearance)
        let phoneNumberTxt = getInsuredDataAlert.addTextField("Telefon nömrəsi")
        phoneNumberTxt.keyboardType = .numberPad
        phoneNumberTxt.placeholder = "Ex.: 994505554433"
        if number != "" {
            phoneNumberTxt.text = number
        }
        let emailTxt = getInsuredDataAlert.addTextField("Email ünvan")
        emailTxt.keyboardType = .emailAddress
        emailTxt.placeholder = "Ex.: test@gmail.com"
        
        getInsuredDataAlert.addButton("GÖNDƏR") {
            completion(phoneNumberTxt.text!, emailTxt.text!)
        }
        _ = getInsuredDataAlert.showCustom("Sığortalının əlaqə məlumatları", subTitle: title, color: UIColor.mainAppColor, icon: #imageLiteral(resourceName: "warning"), closeButtonTitle: "BAĞLA")
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
}

extension String {
    func index(from: Int) -> Index {
        return self.index(startIndex, offsetBy: from)
    }
    
    func substring(from: Int) -> String {
        let fromIndex = index(from: from)
        return substring(from: fromIndex)
    }
    
    func substring(to: Int) -> String {
        let toIndex = index(from: to)
        return substring(to: toIndex)
    }
    
    func substring(with r: Range<Int>) -> String {
        let startIndex = index(from: r.lowerBound)
        let endIndex = index(from: r.upperBound)
        return substring(with: startIndex..<endIndex)
    }
    
    var latinCharactersOnly: Bool {
        return self.range(of: "\\P{Latin}", options: .regularExpression) == nil
    }
    
    var numbersOnly: Bool {
        return self.range(of: "\\P{Number}", options: .regularExpression) == nil
    }
    
}

extension Date {
    
    /// Returns the amount of days from another date
    func days(from date: Date) -> Int {
        return Calendar.current.dateComponents([.day], from: date, to: self).day ?? 0
    }
    /// Returns the amount of hours from another date
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    
    func localFormat(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = dateFormatter.date(from: dateString)
        if let d = date {
            dateFormatter.dateFormat = "dd.MM.yyyy"
            return dateFormatter.string(from: d)
        } else {
            return ""
        }
    }
    
    func requestDateFormat() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: self)
    }
    
}

extension UINavigationItem {
    
    func createCustomNavigationItem(title: String, subTitle: String) {
        
        let titleLabel = UILabel(frame: CGRect(x: 0, y: -2, width: 0, height: 0))
        
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.textColor = UIColor.white
        titleLabel.font = UIFont(name: Constants.helveticaCondensed, size: 14)
        titleLabel.text = title
        titleLabel.sizeToFit()
        
        let subtitleLabel = UILabel(frame: CGRect(x: 0, y: 18, width: 0, height: 0))
        subtitleLabel.backgroundColor = UIColor.clear
        subtitleLabel.textColor = UIColor.white
        subtitleLabel.font = UIFont(name: Constants.helveticaRegular, size: 10)
        subtitleLabel.text = subTitle
        subtitleLabel.sizeToFit()
        
        if (subtitleLabel.frame.size.width > titleLabel.frame.size.width) {
            var titleFrame = titleLabel.frame
            titleFrame.size.width = subtitleLabel.frame.size.width
            titleLabel.frame = titleFrame
            titleLabel.textAlignment = .center
        }
        
        let titleView = UIView(frame: CGRect(x: 0, y: 0, width: max(titleLabel.frame.size.width, subtitleLabel.frame.size.width), height: 30))
        titleView.addSubview(titleLabel)
        titleView.addSubview(subtitleLabel)
        
        self.titleView = titleView
        
        userDefaults.set(title, forKey: Constants.navigationTitle)
        userDefaults.set(subTitle, forKey: Constants.navigationSubtitle)
        
    }
    
}

extension UIButton {
    
    static func createCustomButton(text: String, textSize: CGFloat) -> UIButton {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(text, for: .normal)
        button.titleLabel?.font = UIFont(name: Constants.helveticaCondensed, size: textSize)
        button.layer.cornerRadius = 5
        button.backgroundColor = UIColor.mainAppColor
        return button
    }
    
}

extension CustomTextField {
    
    static func createCustomTxtField(placeholder: String, textSize: CGFloat) -> CustomTextField {
        let textField = CustomTextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "Ex.: \(placeholder)"
        textField.textAlignment = .left
        textField.contentVerticalAlignment = .bottom
        textField.font = UIFont(name: Constants.helveticaBold, size: textSize)
        textField.textColor = UIColor.mainTextColor
        textField.minimumFontSize = 14
        textField.clearButtonMode = .whileEditing
        return textField
    }
    
}

extension UILabel {
    
    static func createLabel(withText text: String, andSize size: CGFloat) -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = text
        label.textColor = UIColor.labelColor
        label.font = UIFont(name: Constants.helveticaBold, size: size)
        return label
    }
    
}
