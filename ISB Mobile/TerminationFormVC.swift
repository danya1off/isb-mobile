//
//  TerminationFormVC.swift
//  ISB Mobile
//
//  Created by Jeyhun Danyalov on 9/21/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD

class TerminationFormVC: UIViewController, UITextFieldDelegate {

    var contractNumberTxtField: CustomTextField!
    var searchBtn: UIButton!
    
    var contractNumber: String?
    
    private var terminationManager: TerminationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        terminationManager = TerminationManager(view: self)
        
        //create main UI
        createUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userDefaults.set(String(describing: self.classForCoder) , forKey: Constants.operationName)
    }
    
    @objc func searchContract() {
        if contractNumberTxtField.text == "" {
            self.fireErrorAlert(withMessage: "Müqavilə nömrə boş ola bilməz!")
        } else {
            guard let operationType = userDefaults.string(forKey: Constants.operationCode) else {
                fireErrorAlert(withMessage: "Əməliyyat növü tapılmadı!")
                return
            }
            terminationManager.getTerminationContract(byNumber: contractNumberTxtField.text!, andOperationType: operationType)
        }
    }
    
    @objc func backAction() {
        ApplicationManager.redirectToMainMenu()
    }
    
    @objc func saveContractNumber() {
        if contractNumberTxtField.text != "" {
            userDefaults.set(contractNumberTxtField.text!, forKey: Constants.contractNumber)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
