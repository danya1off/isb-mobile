//
//  SearchUserVC+Ext.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/20/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

extension SearchUserVC {
    
    func createUI() {
        
        view.backgroundColor = UIColor.white
        navigationItem.hidesBackButton = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "close"), style: .plain, target: self, action: #selector(SearchUserVC.cancelOperation))
//        hideKeyboardWhenTappedAround()
        
        CustomProgressHUD.customize()
        
        if let navTitle = userDefaults.string(forKey: Constants.navigationTitle) {
            if navTitle != "" {
                navigationTitle = navTitle
            }
        }
        if let navSubtitle = userDefaults.string(forKey: Constants.navigationSubtitle) {
            if navSubtitle != "" {
                navigationSubTitle = navSubtitle
            }
        }
        
        navigationItem.createCustomNavigationItem(title: navigationTitle, subTitle: navigationSubTitle)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        
        createMainView()
        createPersonTypeToggleBtn()
        createResidentTypeToggleBtn()
        createResidentView(mode: .naturalPerson)
        configureRestoredView()
        
    }

    private func createMainView() {
        
        formView.backgroundColor = UIColor.white
        formView.translatesAutoresizingMaskIntoConstraints = false
        formView.layer.cornerRadius = 5
        formView.layer.shadowRadius = 5
        formView.layer.shadowOffset = CGSize.zero
        formView.layer.shadowColor = UIColor.black.cgColor
        formView.layer.shadowOpacity = 0.2
        view.addSubview(formView)

        // init constraints
        NSLayoutConstraint.activate([
            
            formView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            formView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            formView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.85),
            formView.heightAnchor.constraint(greaterThanOrEqualToConstant: 100)
            
            ])
        
    }

    private func createPersonTypeToggleBtn() {
        
        personTypeToggleBtn.translatesAutoresizingMaskIntoConstraints = false
        personTypeToggleBtn.isOn = false
        personTypeToggleBtn.tintColor = UIColor.labelColor
        personTypeToggleBtn.onTintColor = UIColor.mainAppColor
        personTypeToggleBtn.addTarget(self, action: #selector(SearchUserVC.personTypeToggleChanged), for: .valueChanged)
        formView.addSubview(personTypeToggleBtn)
        
        naturalPersonLbl = UILabel.createLabel(withText: "Fiziki şəxs", andSize: 12)
        formView.addSubview(naturalPersonLbl)
        
        juridicalPersonLbl = UILabel.createLabel(withText: "Hüquqi şəxs", andSize: 12)
        formView.addSubview(juridicalPersonLbl)
        
        if personTypeToggleBtn.isOn {
            
            naturalPersonLbl.textColor = UIColor.labelColor
            juridicalPersonLbl.textColor = UIColor.mainTextColor
            
        } else {
            
            naturalPersonLbl.textColor = UIColor.mainTextColor
            juridicalPersonLbl.textColor = UIColor.labelColor
            
        }
        
        // init constraints
        personTypeToggleBtn.centerXAnchor.constraint(equalTo: formView.centerXAnchor).isActive = true
        personTypeToggleBtn.topAnchor.constraint(equalTo: formView.topAnchor, constant: 15).isActive = true
        
        naturalPersonLbl.centerYAnchor.constraint(equalTo: personTypeToggleBtn.centerYAnchor).isActive = true
        naturalPersonLbl.rightAnchor.constraint(equalTo: personTypeToggleBtn.leftAnchor, constant: -10).isActive = true
        
        juridicalPersonLbl.centerYAnchor.constraint(equalTo: personTypeToggleBtn.centerYAnchor).isActive = true
        juridicalPersonLbl.leftAnchor.constraint(equalTo: personTypeToggleBtn.rightAnchor, constant: 10).isActive = true
        
    }
    

    func createResidentTypeToggleBtn() {
        
        residentTypeToggleBtn.translatesAutoresizingMaskIntoConstraints = false
        residentTypeToggleBtn.isOn = false
        residentTypeToggleBtn.tintColor = UIColor.labelColor
        residentTypeToggleBtn.onTintColor = UIColor.mainAppColor
        residentTypeToggleBtn.addTarget(self, action: #selector(SearchUserVC.residentTypeToggleChanged), for: .valueChanged)
        formView.addSubview(residentTypeToggleBtn)
        
        residentPersonLbl = UILabel.createLabel(withText: "Rezident", andSize: 12)
        formView.addSubview(residentPersonLbl)
        
        nonResidentPersonLbl = UILabel.createLabel(withText: "Qeyri-rezident", andSize: 12)
        formView.addSubview(nonResidentPersonLbl)
        
        if residentTypeToggleBtn.isOn {
            
            residentPersonLbl.textColor = UIColor.labelColor
            nonResidentPersonLbl.textColor = UIColor.mainTextColor
            
        } else {
            
            residentPersonLbl.textColor = UIColor.mainTextColor
            nonResidentPersonLbl.textColor = UIColor.labelColor
            
        }
        
        // init constraints
        residentTypeToggleBtn.centerXAnchor.constraint(equalTo: personTypeToggleBtn.centerXAnchor).isActive = true
        residentTypeToggleBtn.topAnchor.constraint(equalTo: personTypeToggleBtn.bottomAnchor, constant: 15).isActive = true
        
        residentPersonLbl.centerYAnchor.constraint(equalTo: residentTypeToggleBtn.centerYAnchor).isActive = true
        residentPersonLbl.rightAnchor.constraint(equalTo: residentTypeToggleBtn.leftAnchor, constant: -10).isActive = true
        
        nonResidentPersonLbl.centerYAnchor.constraint(equalTo: residentTypeToggleBtn.centerYAnchor).isActive = true
        nonResidentPersonLbl.leftAnchor.constraint(equalTo: residentTypeToggleBtn.rightAnchor, constant: 10).isActive = true
        
    }

    func createToggleLbl() {
        
        toggleLbl.translatesAutoresizingMaskIntoConstraints = false
        toggleLbl.text = "Yaşama sənədinin növü"
        toggleLbl.textColor = UIColor.labelColor
        toggleLbl.font = UIFont(name: "HelveticaNeue-Bold", size: 10)
        formView.addSubview(toggleLbl)
        
        // init constraints
        toggleLbl.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 20).isActive = true
        toggleLbl.topAnchor.constraint(equalTo: residentTypeToggleBtn.bottomAnchor, constant: 10).isActive = true
        
    }

    func createToggleBtn() {
        
        toggleBtn.translatesAutoresizingMaskIntoConstraints = false
        toggleBtn.isOn = false
        toggleBtn.tintColor = UIColor.labelColor
        toggleBtn.onTintColor = UIColor.labelColor
        toggleBtn.addTarget(self, action: #selector(SearchUserVC.toggleValueChanged), for: .valueChanged)
        formView.addSubview(toggleBtn)
        
        toggleModeLbl.translatesAutoresizingMaskIntoConstraints = false
        toggleModeLbl.text = "DAİMİ"
        toggleModeLbl.textColor = UIColor.labelColor
        toggleModeLbl.font = UIFont(name: "HelveticaNeue-Bold", size: 12)
        formView.addSubview(toggleModeLbl)
        
        // init constraints
        toggleBtn.leadingAnchor.constraint(equalTo: toggleLbl.leadingAnchor).isActive = true
        toggleBtn.topAnchor.constraint(equalTo: toggleLbl.bottomAnchor, constant: 10).isActive = true

        toggleModeLbl.leftAnchor.constraint(equalTo: toggleBtn.rightAnchor, constant: 15).isActive = true
        toggleModeLbl.centerYAnchor.constraint(equalTo: toggleBtn.centerYAnchor).isActive = true
        
    }

    func createResidentView(mode: UserSearchMode) {
        
        residentView.backgroundColor = UIColor.white
        residentView.translatesAutoresizingMaskIntoConstraints = false
        formView.addSubview(residentView)
        
        idNumberTxtField = CustomTextField.createCustomTxtField(placeholder: "AZE82374289", textSize: 20)
        idNumberTxtField.delegate = self
        idNumberTxtField.autocapitalizationType = .allCharacters
        idNumberTxtField.delegate = self
        residentView.addSubview(idNumberTxtField)
        
        idNumberLbl = UILabel.createLabel(withText: "ŞƏXSİYYƏT VƏSİQƏ", andSize: 12)
        residentView.addSubview(idNumberLbl)
        
        searchBtn = UIButton.createCustomButton(text: "AXTAR", textSize: 20)
        searchBtn.addTarget(self, action: #selector(SearchUserVC.searchUser), for: .touchUpInside)
        residentView.addSubview(searchBtn)
        
        // init constraints
        residentView.centerXAnchor.constraint(equalTo: formView.centerXAnchor).isActive = true
        residentView.widthAnchor.constraint(equalTo: formView.widthAnchor).isActive = true
        residentView.heightAnchor.constraint(greaterThanOrEqualToConstant: 100).isActive = true
        residentView.bottomAnchor.constraint(equalTo: formView.bottomAnchor, constant: -15).isActive = true
        
        switch mode {
        case .naturalPerson:
            residentView.topAnchor.constraint(equalTo: residentTypeToggleBtn.bottomAnchor, constant: 15).isActive = true
        case .juridicalPerson:
            residentView.topAnchor.constraint(equalTo: personTypeToggleBtn.bottomAnchor, constant: 15).isActive = true
        }

        idNumberTxtField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        idNumberTxtField.widthAnchor.constraint(equalTo: residentView.widthAnchor, constant: -40).isActive = true
        idNumberTxtField.centerXAnchor.constraint(equalTo: residentView.centerXAnchor).isActive = true
        idNumberTxtField.topAnchor.constraint(equalTo: residentView.topAnchor).isActive = true

        idNumberLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
        idNumberLbl.leadingAnchor.constraint(equalTo: idNumberTxtField.leadingAnchor).isActive = true
        idNumberLbl.trailingAnchor.constraint(equalTo: idNumberTxtField.trailingAnchor).isActive = true
        idNumberLbl.topAnchor.constraint(equalTo: idNumberTxtField.topAnchor).isActive = true

        searchBtn.leadingAnchor.constraint(equalTo: idNumberTxtField.leadingAnchor).isActive = true
        searchBtn.trailingAnchor.constraint(equalTo: idNumberTxtField.trailingAnchor).isActive = true
        searchBtn.topAnchor.constraint(equalTo: idNumberTxtField.bottomAnchor, constant: 20).isActive = true
        searchBtn.heightAnchor.constraint(equalTo: idNumberTxtField.heightAnchor).isActive = true
        searchBtn.bottomAnchor.constraint(equalTo: residentView.bottomAnchor).isActive = true
        
    }

    func creteNonResidentView() {
        
        let textFieldHeight: CGFloat = 40.0
        let textFieldFontSize: CGFloat = 15.0
        let labelSize: CGFloat = 8.0
        
        nonResidentView.backgroundColor = UIColor.white
        nonResidentView.translatesAutoresizingMaskIntoConstraints = false
        formView.addSubview(nonResidentView)
        
        // init constraints
        nonResidentView.centerXAnchor.constraint(equalTo: formView.centerXAnchor).isActive = true
        nonResidentView.widthAnchor.constraint(equalTo: formView.widthAnchor).isActive = true
        nonResidentView.heightAnchor.constraint(greaterThanOrEqualToConstant: 100).isActive = true
        nonResidentView.bottomAnchor.constraint(equalTo: formView.bottomAnchor, constant: -15).isActive = true
        nonResidentView.topAnchor.constraint(equalTo: toggleBtn.bottomAnchor, constant: 15).isActive = true
        
        if operationCode == Constants.BORDER_CMTPL_CONTRACT_OPERATION {
            
            // last name
            lastNameTxtField = CustomTextField.createCustomTxtField(placeholder: "Danyalov", textSize: textFieldFontSize)
            lastNameTxtField.delegate = self
            nonResidentView.addSubview(lastNameTxtField)
            
            // last name label
            let lastNameLbl = UILabel.createLabel(withText: "SOYADI", andSize: labelSize)
            nonResidentView.addSubview(lastNameLbl)
            
            // first name
            firstNameTxtField = CustomTextField.createCustomTxtField(placeholder: "Ceyhun", textSize: textFieldFontSize)
            firstNameTxtField.delegate = self
            nonResidentView.addSubview(firstNameTxtField)
            
            // first name label
            let firstNameLbl = UILabel.createLabel(withText: "ADI", andSize: labelSize)
            nonResidentView.addSubview(firstNameLbl)
            
            // patronymic
            patronymicTxtField = CustomTextField.createCustomTxtField(placeholder: "Fizuli oğlu", textSize: textFieldFontSize)
            patronymicTxtField.delegate = self
            nonResidentView.addSubview(patronymicTxtField)
            
            // patronymic label
            let patronymicLbl = UILabel.createLabel(withText: "ATA ADI", andSize: labelSize)
            nonResidentView.addSubview(patronymicLbl)
            
            // pin
            pinTxtField = CustomTextField.createCustomTxtField(placeholder: "11042D", textSize: textFieldFontSize)
            pinTxtField.autocapitalizationType = .allCharacters
            pinTxtField.delegate = self
            nonResidentView.addSubview(pinTxtField)
            
            // pin label
            let pinLbl = UILabel.createLabel(withText: "VMMS fin kod", andSize: labelSize)
            nonResidentView.addSubview(pinLbl)
            
            // id document
            idDocumentTxtField = CustomTextField.createCustomTxtField(placeholder: "1111222", textSize: textFieldFontSize)
            idDocumentTxtField.delegate = self
            nonResidentView.addSubview(idDocumentTxtField)
            
            // id document label
            let idDocumentLbl = UILabel.createLabel(withText: "YAŞAMA İCAZƏ VƏSİQƏSİ", andSize: labelSize)
            nonResidentView.addSubview(idDocumentLbl)
            
            // button
            searchBtn = UIButton.createCustomButton(text: "GÖNDƏR", textSize: 20)
            searchBtn.addTarget(self, action: #selector(SearchUserVC.searchUser), for: .touchUpInside)
            nonResidentView.addSubview(searchBtn)
            
            
            // init constraints
            lastNameTxtField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true
            lastNameTxtField.widthAnchor.constraint(equalTo: nonResidentView.widthAnchor, constant: -40).isActive = true
            lastNameTxtField.centerXAnchor.constraint(equalTo: nonResidentView.centerXAnchor).isActive = true
            lastNameTxtField.topAnchor.constraint(equalTo: nonResidentView.topAnchor).isActive = true

            lastNameLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
            lastNameLbl.leadingAnchor.constraint(equalTo: lastNameTxtField.leadingAnchor).isActive = true
            lastNameLbl.trailingAnchor.constraint(equalTo: lastNameTxtField.trailingAnchor).isActive = true
            lastNameLbl.topAnchor.constraint(equalTo: lastNameTxtField.topAnchor).isActive = true

            firstNameTxtField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true
            firstNameTxtField.widthAnchor.constraint(equalTo: nonResidentView.widthAnchor, constant: -40).isActive = true
            firstNameTxtField.centerXAnchor.constraint(equalTo: nonResidentView.centerXAnchor).isActive = true
            firstNameTxtField.topAnchor.constraint(equalTo: lastNameTxtField.bottomAnchor, constant: 15).isActive = true

            firstNameLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
            firstNameLbl.leadingAnchor.constraint(equalTo: firstNameTxtField.leadingAnchor).isActive = true
            firstNameLbl.trailingAnchor.constraint(equalTo: firstNameTxtField.trailingAnchor).isActive = true
            firstNameLbl.topAnchor.constraint(equalTo: firstNameTxtField.topAnchor).isActive = true

            patronymicTxtField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true
            patronymicTxtField.widthAnchor.constraint(equalTo: nonResidentView.widthAnchor, constant: -40).isActive = true
            patronymicTxtField.centerXAnchor.constraint(equalTo: nonResidentView.centerXAnchor).isActive = true
            patronymicTxtField.topAnchor.constraint(equalTo: firstNameTxtField.bottomAnchor, constant: 15).isActive = true

            patronymicLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
            patronymicLbl.leadingAnchor.constraint(equalTo: patronymicTxtField.leadingAnchor).isActive = true
            patronymicLbl.trailingAnchor.constraint(equalTo: patronymicTxtField.trailingAnchor).isActive = true
            patronymicLbl.topAnchor.constraint(equalTo: patronymicTxtField.topAnchor).isActive = true

            pinTxtField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true
            pinTxtField.widthAnchor.constraint(equalTo: nonResidentView.widthAnchor, constant: -40).isActive = true
            pinTxtField.centerXAnchor.constraint(equalTo: nonResidentView.centerXAnchor).isActive = true
            pinTxtField.topAnchor.constraint(equalTo: patronymicTxtField.bottomAnchor, constant: 15).isActive = true

            pinLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
            pinLbl.leadingAnchor.constraint(equalTo: pinTxtField.leadingAnchor).isActive = true
            pinLbl.trailingAnchor.constraint(equalTo: pinTxtField.trailingAnchor).isActive = true
            pinLbl.topAnchor.constraint(equalTo: pinTxtField.topAnchor).isActive = true

            idDocumentTxtField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true
            idDocumentTxtField.widthAnchor.constraint(equalTo: nonResidentView.widthAnchor, constant: -40).isActive = true
            idDocumentTxtField.centerXAnchor.constraint(equalTo: nonResidentView.centerXAnchor).isActive = true
            idDocumentTxtField.topAnchor.constraint(equalTo: pinTxtField.bottomAnchor, constant: 15).isActive = true

            idDocumentLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
            idDocumentLbl.leadingAnchor.constraint(equalTo: idDocumentTxtField.leadingAnchor).isActive = true
            idDocumentLbl.trailingAnchor.constraint(equalTo: idDocumentTxtField.trailingAnchor).isActive = true
            idDocumentLbl.topAnchor.constraint(equalTo: idDocumentTxtField.topAnchor).isActive = true

            searchBtn.leadingAnchor.constraint(equalTo: idDocumentTxtField.leadingAnchor).isActive = true
            searchBtn.trailingAnchor.constraint(equalTo: idDocumentTxtField.trailingAnchor).isActive = true
            searchBtn.topAnchor.constraint(equalTo: idDocumentTxtField.bottomAnchor, constant: 15).isActive = true
            searchBtn.heightAnchor.constraint(equalTo: idDocumentTxtField.heightAnchor).isActive = true
            searchBtn.bottomAnchor.constraint(equalTo: nonResidentView.bottomAnchor).isActive = true
            
        } else {
            
            idNumberTxtField = CustomTextField.createCustomTxtField(placeholder: "...", textSize: 20)
            idNumberTxtField.delegate = self
            idNumberTxtField.autocapitalizationType = .allCharacters
            idNumberTxtField.delegate = self
            nonResidentView.addSubview(idNumberTxtField)
            
            idNumberLbl = UILabel.createLabel(withText: "Yaşama icazə sənədi", andSize: 12)
            nonResidentView.addSubview(idNumberLbl)
            
            searchBtn = UIButton.createCustomButton(text: "AXTAR", textSize: 20)
            searchBtn.addTarget(self, action: #selector(SearchUserVC.searchUser), for: .touchUpInside)
            nonResidentView.addSubview(searchBtn)
            
            
            // init constraints
            idNumberTxtField.heightAnchor.constraint(equalToConstant: 50).isActive = true
            idNumberTxtField.widthAnchor.constraint(equalTo: nonResidentView.widthAnchor, constant: -40).isActive = true
            idNumberTxtField.centerXAnchor.constraint(equalTo: nonResidentView.centerXAnchor).isActive = true
            idNumberTxtField.topAnchor.constraint(equalTo: nonResidentView.topAnchor).isActive = true
            
            idNumberLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
            idNumberLbl.leadingAnchor.constraint(equalTo: idNumberTxtField.leadingAnchor).isActive = true
            idNumberLbl.trailingAnchor.constraint(equalTo: idNumberTxtField.trailingAnchor).isActive = true
            idNumberLbl.topAnchor.constraint(equalTo: idNumberTxtField.topAnchor).isActive = true

            searchBtn.leadingAnchor.constraint(equalTo: idNumberTxtField.leadingAnchor).isActive = true
            searchBtn.trailingAnchor.constraint(equalTo: idNumberTxtField.trailingAnchor).isActive = true
            searchBtn.topAnchor.constraint(equalTo: idNumberTxtField.bottomAnchor, constant: 20).isActive = true
            searchBtn.heightAnchor.constraint(equalTo: idNumberTxtField.heightAnchor).isActive = true
            searchBtn.bottomAnchor.constraint(equalTo: nonResidentView.bottomAnchor).isActive = true
            
        }
        
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0 {
                view.frame.origin.y -= keyboardSize.height / 1.8
            }
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0{
                view.frame.origin.y += keyboardSize.height / 1.8
            }
        }
    }
    
    func removeFromView(_ view: UIView) {
        
        
        
    }
    
    private func configureRestoredView() {
        
        personTypeToggleBtn.isOn = userDefaults.bool(forKey: Constants.personType)
        residentTypeToggleBtn.isOn = userDefaults.bool(forKey: Constants.residencyType)
        
        if (!personTypeToggleBtn.isOn || personTypeToggleBtn.isOn) && !residentTypeToggleBtn.isOn {
            
            personTypeToggleChanged()
            
            idNumberTxtField.addTarget(self, action: #selector(saveIdNumber), for: .editingDidEnd)
            
            if let value = userDefaults.string(forKey: Constants.idNumber) {
                idNumberTxtField.text = value
            }
        }
        
        if residentTypeToggleBtn.isOn && operationCode == Constants.BORDER_CMTPL_CONTRACT_OPERATION {
            
            residentTypeToggleChanged()
            
            lastNameTxtField.addTarget(self, action: #selector(saveLastName), for: .editingDidEnd)
            firstNameTxtField.addTarget(self, action: #selector(saveFirstName), for: .editingDidEnd)
            patronymicTxtField.addTarget(self, action: #selector(savePatronymic), for: .editingDidEnd)
            pinTxtField.addTarget(self, action: #selector(savePin), for: .editingDidEnd)
            idDocumentTxtField.addTarget(self, action: #selector(saveIdDocument), for: .editingDidEnd)
            
            if let value = userDefaults.string(forKey: Constants.lastName) {
                lastNameTxtField.text = value
            }
            
            if let value = userDefaults.string(forKey: Constants.firstName) {
                firstNameTxtField.text = value
            }
            
            if let value = userDefaults.string(forKey: Constants.patronymic) {
                patronymicTxtField.text = value
            }
            
            if let value = userDefaults.string(forKey: Constants.pin) {
                pinTxtField.text = value
            }
            
            if let value = userDefaults.string(forKey: Constants.idDocument) {
                idDocumentTxtField.text = value
            }
            
        }
        
    }
    
    @objc func saveIdNumber() {
        if idNumberTxtField.text != "" {
            userDefaults.set(idNumberTxtField.text!, forKey: Constants.idNumber)
        }
    }
    
    @objc func saveLastName() {
        if lastNameTxtField.text != "" {
            userDefaults.set(lastNameTxtField.text!, forKey: Constants.lastName)
        }
    }
    
    @objc func saveFirstName() {
        if firstNameTxtField.text != "" {
            userDefaults.set(firstNameTxtField.text!, forKey: Constants.firstName)
        }
    }
    
    @objc func savePatronymic() {
        if patronymicTxtField.text != "" {
            userDefaults.set(patronymicTxtField.text!, forKey: Constants.patronymic)
        }
    }
    
    @objc func savePin() {
        if pinTxtField.text != "" {
            userDefaults.set(pinTxtField.text!, forKey: Constants.pin)
        }
    }
    
    @objc func saveIdDocument() {
        if idDocumentTxtField.text != "" {
            userDefaults.set(idDocumentTxtField.text!, forKey: Constants.idDocument)
        }
    }
    
    func clearUserViewFieldValues() {
        
        userDefaults.set(false, forKey: Constants.personType)
        userDefaults.set(false, forKey: Constants.residencyType)
        userDefaults.removeObject(forKey: Constants.idNumber)
        userDefaults.removeObject(forKey: Constants.lastName)
        userDefaults.removeObject(forKey: Constants.firstName)
        userDefaults.removeObject(forKey: Constants.patronymic)
        userDefaults.removeObject(forKey: Constants.pin)
        userDefaults.removeObject(forKey: Constants.idDocument)
        
    }
    
}

enum UserSearchMode {
    case naturalPerson
    case juridicalPerson
}
