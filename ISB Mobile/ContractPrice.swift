//
//  ContractPrice.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/21/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import SwiftyJSON

class ContractPrice: NSObject, NSCoding {
    
    private var _price: Double!
    private var _bmCoefficient: Double!
    private var _calculatedPrice: Double!
    private var _isPriceFromLastContract: Bool!
    
    init(withData data: JSON) {
        _price = data["price"].double ?? 0.0
        _bmCoefficient = data["bmCoefficient"].double ?? 0.0
        _calculatedPrice = data["calculatedPrice"].double ?? 0.0
        _isPriceFromLastContract = data["isPriceFromLastContract"].bool ?? false
    }
    
    init(price: Double?, bmCoefficient: Double?, calculatedPrice: Double?, isPriceFromLastContract: Bool) {
        _price = price
        _bmCoefficient = bmCoefficient
        _calculatedPrice = calculatedPrice
        _isPriceFromLastContract = isPriceFromLastContract
    }
    
    override init() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        _price = aDecoder.decodeDouble(forKey: Keys.price)
        _bmCoefficient = aDecoder.decodeDouble(forKey: Keys.bmCoefficient)
        _calculatedPrice = aDecoder.decodeDouble(forKey: Keys.calculatedPrice)
        _isPriceFromLastContract = aDecoder.decodeBool(forKey: Keys.isPriceFromLastContract)

    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(_price, forKey: Keys.price)
        aCoder.encode(_bmCoefficient, forKey: Keys.bmCoefficient)
        aCoder.encode(_calculatedPrice, forKey: Keys.calculatedPrice)
        aCoder.encode(_isPriceFromLastContract, forKey: Keys.isPriceFromLastContract)
    }
    
    struct Keys {
        static let price = "price"
        static let bmCoefficient = "bmCoefficient"
        static let calculatedPrice = "calculatedPrice"
        static let isPriceFromLastContract = "isPriceFromLastContract"
    }
    
    
    var contractPriseArray: [CellDataCustomizer] {
        
        var arr = [CellDataCustomizer]()
        
        var customizer: CellDataCustomizer
        if let data = _price, _price != 0.0 {
            customizer = CellDataCustomizer(label: "SIĞORTA HAQQI", data: data)
            arr.append(customizer)
        }
        if let data = _bmCoefficient, _bmCoefficient != 0.0 {
            customizer = CellDataCustomizer(label: "BONUS-MALUS ƏMSALI", data: data)
            arr.append(customizer)
        }
        if let data = _calculatedPrice, _calculatedPrice != 0.0 {
            customizer = CellDataCustomizer(label: "YEKUN SIĞORTA HAQQI", data: data)
            arr.append(customizer)
        }
        
        return arr
        
    }
    
    var price: Double {
        get {
            return _price
        } set {
            _price = newValue
        }
    }
    
    var bmCoefficient: Double {
        get {
            return _bmCoefficient
        }
        set {
            _bmCoefficient = newValue
        }
    }
    
    var calculatedPrice: Double {
        get {
            return _calculatedPrice
        }
        set {
            _calculatedPrice = newValue
        }
    }
    
    var isPriceFromLastContract: Bool {
        get {
            return _isPriceFromLastContract
        }
        set {
            _isPriceFromLastContract = newValue
        }
    }
    
    
}
