//
//  SearchVehicleVC+Ext.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/20/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

extension SearchVehicleVC {
    
    func createUI() {
        view.backgroundColor = UIColor.white
        CustomProgressHUD.customize()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "close"), style: .plain, target: self, action: #selector(SearchVehicleVC.cancelOperation))
        
        if let navTitle = userDefaults.string(forKey: Constants.navigationTitle) {
            if navTitle != "" {
                navigationTitle = navTitle
            }
        }
        if let navSubtitle = userDefaults.string(forKey: Constants.navigationSubtitle) {
            if navSubtitle != "" {
                navigationSubTitle = navSubtitle
            }
        }
        navigationItem.createCustomNavigationItem(title: navigationTitle, subTitle: navigationSubTitle)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        
        vehicleTypeSelector.delegate = self
        vehicleTypeSelector.dataSource = self
        
        createForm()
        if operationCode == Constants.BORDER_CMTPL_CONTRACT_OPERATION {
            createMiscView()
        } else {
            createMiaView()
        }
        
        configureRestoredView()
        
    }
    
    func createMiaView() {
        
        miaForm.translatesAutoresizingMaskIntoConstraints = false
        miaForm.backgroundColor = UIColor.clear
        formView.addSubview(miaForm)
        
        certificateNumberTxtField = CustomTextField.createCustomTxtField(placeholder: "AC111111", textSize: 20)
        certificateNumberTxtField.autocapitalizationType = .allCharacters
        certificateNumberTxtField.delegate = self
        miaForm.addSubview(certificateNumberTxtField)
        
        carNumberTxtField = CustomTextField.createCustomTxtField(placeholder: "99OT123", textSize: 20)
        carNumberTxtField.autocapitalizationType = .allCharacters
        carNumberTxtField.delegate = self
        miaForm.addSubview(carNumberTxtField)
        
        let certificateLbl = UILabel.createLabel(withText: Constants.certificateNumberLbl, andSize: 12)
        miaForm.addSubview(certificateLbl)
        
        let carNumbeLbl = UILabel.createLabel(withText: Constants.carNumberLbl, andSize: 12)
        miaForm.addSubview(carNumbeLbl)
        
        searchBtn = UIButton.createCustomButton(text: "AXTAR", textSize: 20)
        searchBtn.addTarget(self, action: #selector(SearchVehicleVC.searchVehicle), for: .touchUpInside)
        miaForm.addSubview(searchBtn)
        
        // init constraints
        miaForm.centerXAnchor.constraint(equalTo: formView.centerXAnchor).isActive = true
        miaForm.widthAnchor.constraint(equalTo: formView.widthAnchor).isActive = true
        miaForm.heightAnchor.constraint(greaterThanOrEqualToConstant: 100).isActive = true
        miaForm.bottomAnchor.constraint(equalTo: formView.bottomAnchor, constant: -20).isActive = true
        if operationCode == Constants.STANDARD_CMTPL_CONTRACT_OPERATION {
            miaForm.topAnchor.constraint(equalTo: toggleBtn.bottomAnchor, constant: 20).isActive = true
        } else {
            miaForm.topAnchor.constraint(equalTo: formView.topAnchor, constant: 20).isActive = true
        }
        
        certificateNumberTxtField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        certificateNumberTxtField.widthAnchor.constraint(equalTo: miaForm.widthAnchor, constant: -40).isActive = true
        certificateNumberTxtField.centerXAnchor.constraint(equalTo: miaForm.centerXAnchor).isActive = true
        certificateNumberTxtField.topAnchor.constraint(equalTo: miaForm.topAnchor).isActive = true

        certificateLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
        certificateLbl.leadingAnchor.constraint(equalTo: certificateNumberTxtField.leadingAnchor).isActive = true
        certificateLbl.trailingAnchor.constraint(equalTo: certificateNumberTxtField.trailingAnchor).isActive = true
        certificateLbl.topAnchor.constraint(equalTo: certificateNumberTxtField.topAnchor).isActive = true

        carNumberTxtField.heightAnchor.constraint(equalTo: certificateNumberTxtField.heightAnchor).isActive = true
        carNumberTxtField.leadingAnchor.constraint(equalTo: certificateNumberTxtField.leadingAnchor).isActive = true
        carNumberTxtField.trailingAnchor.constraint(equalTo: certificateNumberTxtField.trailingAnchor).isActive = true
        carNumberTxtField.topAnchor.constraint(equalTo: certificateNumberTxtField.bottomAnchor, constant: 20).isActive = true

        carNumbeLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
        carNumbeLbl.leadingAnchor.constraint(equalTo: carNumberTxtField.leadingAnchor).isActive = true
        carNumbeLbl.trailingAnchor.constraint(equalTo: carNumberTxtField.trailingAnchor).isActive = true
        carNumbeLbl.topAnchor.constraint(equalTo: carNumberTxtField.topAnchor).isActive = true

        searchBtn.leadingAnchor.constraint(equalTo: carNumberTxtField.leadingAnchor).isActive = true
        searchBtn.trailingAnchor.constraint(equalTo: carNumberTxtField.trailingAnchor).isActive = true
        searchBtn.topAnchor.constraint(equalTo: carNumberTxtField.bottomAnchor, constant: 20).isActive = true
        searchBtn.heightAnchor.constraint(equalTo: carNumberTxtField.heightAnchor).isActive = true
        searchBtn.bottomAnchor.constraint(equalTo: miaForm.bottomAnchor).isActive = true
        
    }
    
    func createMiscView() {
        
        let textFieldHeight: CGFloat = 45.0
        let textFieldFontSize: CGFloat = 15.0
        let labelSize: CGFloat = 8.0
        
        miscForm.translatesAutoresizingMaskIntoConstraints = false
        miscForm.backgroundColor = UIColor.clear
        formView.addSubview(miscForm)
        
        miscForm.centerXAnchor.constraint(equalTo: formView.centerXAnchor).isActive = true
        miscForm.widthAnchor.constraint(equalTo: formView.widthAnchor).isActive = true
        miscForm.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.55).isActive = true
        if operationCode == Constants.STANDARD_CMTPL_CONTRACT_OPERATION {
            miscForm.topAnchor.constraint(equalTo: toggleBtn.bottomAnchor, constant: 20).isActive = true
        } else {
            miscForm.topAnchor.constraint(equalTo: formView.topAnchor, constant: 20).isActive = true
        }
        miscForm.bottomAnchor.constraint(equalTo: formView.bottomAnchor, constant: -20).isActive = true
        
        scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.bounces = true
        scrollView.alwaysBounceVertical = true
        scrollView.backgroundColor = UIColor.clear
        scrollView.contentSize = CGSize(width: miscForm.frame.size.width, height: 700)
        miscForm.addSubview(scrollView)
        
        scrollView.topAnchor.constraint(equalTo: miscForm.topAnchor).isActive = true
        scrollView.widthAnchor.constraint(equalTo: miscForm.widthAnchor).isActive = true
        scrollView.centerXAnchor.constraint(equalTo: miscForm.centerXAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: miscForm.bottomAnchor).isActive = true
        
        // body number
        bodyNumberTxtField = CustomTextField.createCustomTxtField(placeholder: "KFRIE20344JFORIJ", textSize: textFieldFontSize)
        bodyNumberTxtField.autocapitalizationType = .allCharacters
        bodyNumberTxtField.delegate = self
        scrollView.addSubview(bodyNumberTxtField)
        
        bodyNumberTxtField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true
        bodyNumberTxtField.widthAnchor.constraint(equalTo: scrollView.widthAnchor, constant: -40).isActive = true
        bodyNumberTxtField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        bodyNumberTxtField.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        
        // body number label
        let bodyNumberLbl = UILabel.createLabel(withText: Constants.bodyNumberLbl, andSize: labelSize)
        scrollView.addSubview(bodyNumberLbl)
        
        bodyNumberLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
        bodyNumberLbl.leadingAnchor.constraint(equalTo: bodyNumberTxtField.leadingAnchor).isActive = true
        bodyNumberLbl.trailingAnchor.constraint(equalTo: bodyNumberTxtField.trailingAnchor).isActive = true
        bodyNumberLbl.topAnchor.constraint(equalTo: bodyNumberTxtField.topAnchor).isActive = true
        
        // engine number
        engineNumberTxtField = CustomTextField.createCustomTxtField(placeholder: "Y54Y55", textSize: textFieldFontSize)
        engineNumberTxtField.autocapitalizationType = .allCharacters
        engineNumberTxtField.delegate = self
        scrollView.addSubview(engineNumberTxtField)
        
        engineNumberTxtField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true
        engineNumberTxtField.widthAnchor.constraint(equalTo: bodyNumberTxtField.widthAnchor).isActive = true
        engineNumberTxtField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        engineNumberTxtField.topAnchor.constraint(equalTo: bodyNumberTxtField.bottomAnchor, constant: 15).isActive = true
        
        // engine number label
        let engineNumberLbl = UILabel.createLabel(withText: Constants.engineNumberLbl, andSize: labelSize)
        scrollView.addSubview(engineNumberLbl)
        
        engineNumberLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
        engineNumberLbl.leadingAnchor.constraint(equalTo: engineNumberTxtField.leadingAnchor).isActive = true
        engineNumberLbl.trailingAnchor.constraint(equalTo: engineNumberTxtField.trailingAnchor).isActive = true
        engineNumberLbl.topAnchor.constraint(equalTo: engineNumberTxtField.topAnchor).isActive = true
        
        // chassis number
        chassisTxtField = CustomTextField.createCustomTxtField(placeholder: "", textSize: textFieldFontSize)
        chassisTxtField.autocapitalizationType = .allCharacters
        chassisTxtField.delegate = self
        scrollView.addSubview(chassisTxtField)
        
        chassisTxtField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true
        chassisTxtField.widthAnchor.constraint(equalTo: bodyNumberTxtField.widthAnchor).isActive = true
        chassisTxtField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        chassisTxtField.topAnchor.constraint(equalTo: engineNumberTxtField.bottomAnchor, constant: 15).isActive = true
        
        // certificate number label
        let chassisNumberLbl = UILabel.createLabel(withText: Constants.chassisNumberLbl, andSize: labelSize)
        scrollView.addSubview(chassisNumberLbl)
        
        chassisNumberLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
        chassisNumberLbl.leadingAnchor.constraint(equalTo: chassisTxtField.leadingAnchor).isActive = true
        chassisNumberLbl.trailingAnchor.constraint(equalTo: chassisTxtField.trailingAnchor).isActive = true
        chassisNumberLbl.topAnchor.constraint(equalTo: chassisTxtField.topAnchor).isActive = true
        
        // car number
        carNumberTxtField = CustomTextField.createCustomTxtField(placeholder: "99YY422", textSize: textFieldFontSize)
        carNumberTxtField.autocapitalizationType = .allCharacters
        carNumberTxtField.delegate = self
        scrollView.addSubview(carNumberTxtField)
        
        carNumberTxtField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true
        carNumberTxtField.widthAnchor.constraint(equalTo: bodyNumberTxtField.widthAnchor).isActive = true
        carNumberTxtField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        carNumberTxtField.topAnchor.constraint(equalTo: chassisTxtField.bottomAnchor, constant: 15).isActive = true
        
        // car number label
        let carNumberLbl = UILabel.createLabel(withText: Constants.carNumberLbl, andSize: labelSize)
        scrollView.addSubview(carNumberLbl)
        
        carNumberLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
        carNumberLbl.leadingAnchor.constraint(equalTo: carNumberTxtField.leadingAnchor).isActive = true
        carNumberLbl.trailingAnchor.constraint(equalTo: carNumberTxtField.trailingAnchor).isActive = true
        carNumberLbl.topAnchor.constraint(equalTo: carNumberTxtField.topAnchor).isActive = true
        
        // certificate number
        certificateNumberTxtField = CustomTextField.createCustomTxtField(placeholder: "AC445998", textSize: textFieldFontSize)
        certificateNumberTxtField.autocapitalizationType = .allCharacters
        certificateNumberTxtField.delegate = self
        scrollView.addSubview(certificateNumberTxtField)
        
        certificateNumberTxtField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true
        certificateNumberTxtField.widthAnchor.constraint(equalTo: bodyNumberTxtField.widthAnchor).isActive = true
        certificateNumberTxtField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        certificateNumberTxtField.topAnchor.constraint(equalTo: carNumberTxtField.bottomAnchor, constant: 15).isActive = true
        
        // certificate number label
        let certificateNumberLbl = UILabel.createLabel(withText: Constants.certificateNumberLbl, andSize: labelSize)
        scrollView.addSubview(certificateNumberLbl)
        
        certificateNumberLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
        certificateNumberLbl.leadingAnchor.constraint(equalTo: certificateNumberTxtField.leadingAnchor).isActive = true
        certificateNumberLbl.trailingAnchor.constraint(equalTo: certificateNumberTxtField.trailingAnchor).isActive = true
        certificateNumberLbl.topAnchor.constraint(equalTo: certificateNumberTxtField.topAnchor).isActive = true
        
        
        
        // year of manufacture
        yearOfManufactureTxtField = CustomTextField.createCustomTxtField(placeholder: "2017", textSize: textFieldFontSize)
        yearOfManufactureTxtField.keyboardType = .numberPad
        yearOfManufactureTxtField.delegate = self
        scrollView.addSubview(yearOfManufactureTxtField)
        
        yearOfManufactureTxtField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true
        yearOfManufactureTxtField.widthAnchor.constraint(equalTo: bodyNumberTxtField.widthAnchor).isActive = true
        yearOfManufactureTxtField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        yearOfManufactureTxtField.topAnchor.constraint(equalTo: certificateNumberTxtField.bottomAnchor, constant: 15).isActive = true
        
        // certificate number label
        let yearOfManLbl = UILabel.createLabel(withText: Constants.yearOfManufacturerLbl, andSize: labelSize)
        scrollView.addSubview(yearOfManLbl)
        
        yearOfManLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
        yearOfManLbl.leadingAnchor.constraint(equalTo: yearOfManufactureTxtField.leadingAnchor).isActive = true
        yearOfManLbl.trailingAnchor.constraint(equalTo: yearOfManufactureTxtField.trailingAnchor).isActive = true
        yearOfManLbl.topAnchor.constraint(equalTo: yearOfManufactureTxtField.topAnchor).isActive = true
        
        // vehicle color
        vehicleColorTxtField = CustomTextField.createCustomTxtField(placeholder: "Qırmızı", textSize: textFieldFontSize)
        vehicleColorTxtField.autocapitalizationType = .words
        vehicleColorTxtField.delegate = self
        scrollView.addSubview(vehicleColorTxtField)
        
        vehicleColorTxtField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true
        vehicleColorTxtField.widthAnchor.constraint(equalTo: bodyNumberTxtField.widthAnchor).isActive = true
        vehicleColorTxtField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        vehicleColorTxtField.topAnchor.constraint(equalTo: yearOfManufactureTxtField.bottomAnchor, constant: 15).isActive = true
        
        // vehicle color label
        let vehicleColorLbl = UILabel.createLabel(withText: Constants.vehicleColorLbl, andSize: labelSize)
        scrollView.addSubview(vehicleColorLbl)
        
        vehicleColorLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
        vehicleColorLbl.leadingAnchor.constraint(equalTo: vehicleColorTxtField.leadingAnchor).isActive = true
        vehicleColorLbl.trailingAnchor.constraint(equalTo: vehicleColorTxtField.trailingAnchor).isActive = true
        vehicleColorLbl.topAnchor.constraint(equalTo: vehicleColorTxtField.topAnchor).isActive = true
        
        // car make
        makeTxtField = CustomTextField.createCustomTxtField(placeholder: "Kia", textSize: textFieldFontSize)
        makeTxtField.autocapitalizationType = .words
        makeTxtField.delegate = self
        scrollView.addSubview(makeTxtField)
        
        makeTxtField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true
        makeTxtField.widthAnchor.constraint(equalTo: bodyNumberTxtField.widthAnchor).isActive = true
        makeTxtField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        makeTxtField.topAnchor.constraint(equalTo: vehicleColorTxtField.bottomAnchor, constant: 15).isActive = true
        
        // car make
        let makeLbl = UILabel.createLabel(withText: Constants.makeLbl, andSize: labelSize)
        scrollView.addSubview(makeLbl)
        
        makeLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
        makeLbl.leadingAnchor.constraint(equalTo: makeTxtField.leadingAnchor).isActive = true
        makeLbl.trailingAnchor.constraint(equalTo: makeTxtField.trailingAnchor).isActive = true
        makeLbl.topAnchor.constraint(equalTo: makeTxtField.topAnchor).isActive = true
        
        // car model
        modelTxtField = CustomTextField.createCustomTxtField(placeholder: "Sportage", textSize: textFieldFontSize)
        modelTxtField.autocapitalizationType = .words
        modelTxtField.delegate = self
        scrollView.addSubview(modelTxtField)
        
        modelTxtField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true
        modelTxtField.widthAnchor.constraint(equalTo: bodyNumberTxtField.widthAnchor).isActive = true
        modelTxtField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        modelTxtField.topAnchor.constraint(equalTo: makeTxtField.bottomAnchor, constant: 15).isActive = true
        
        // car make
        let modelLbl = UILabel.createLabel(withText: Constants.modelLbl, andSize: labelSize)
        scrollView.addSubview(modelLbl)
        
        modelLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
        modelLbl.leadingAnchor.constraint(equalTo: modelTxtField.leadingAnchor).isActive = true
        modelLbl.trailingAnchor.constraint(equalTo: modelTxtField.trailingAnchor).isActive = true
        modelLbl.topAnchor.constraint(equalTo: modelTxtField.topAnchor).isActive = true
        
        if operationCode != Constants.STANDARD_CMTPL_CONTRACT_OPERATION {
            // vehicle type selector
            vehicleTypeSelector.translatesAutoresizingMaskIntoConstraints = false
            vehicleTypeSelector.backgroundColor = UIColor(red:0.94, green:0.95, blue:0.97, alpha:1.0)
            vehicleTypeSelector.layer.cornerRadius = 5
            scrollView.addSubview(vehicleTypeSelector)
            
            vehicleTypeSelector.heightAnchor.constraint(equalToConstant: 80).isActive = true
            vehicleTypeSelector.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
            vehicleTypeSelector.topAnchor.constraint(equalTo: modelTxtField.bottomAnchor, constant: 15).isActive = true
            vehicleTypeSelector.widthAnchor.constraint(equalTo: bodyNumberTxtField.widthAnchor).isActive = true
            
            generalTxtField = CustomTextField.createCustomTxtField(placeholder: "...", textSize: textFieldFontSize)
            generalTxtField.keyboardType = .numberPad
            generalTxtField.isHidden = true
            generalTxtField.delegate = self
            scrollView.addSubview(generalTxtField)
            
            generalTxtField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true
            generalTxtField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
            generalTxtField.widthAnchor.constraint(equalTo: bodyNumberTxtField.widthAnchor).isActive = true
            generalTxtField.topAnchor.constraint(equalTo: vehicleTypeSelector.bottomAnchor, constant: 15).isActive = true
            
            generalLbl = UILabel.createLabel(withText: "...", andSize: labelSize)
            scrollView.addSubview(generalLbl)
            
            generalLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
            generalLbl.leadingAnchor.constraint(equalTo: generalTxtField.leadingAnchor).isActive = true
            generalLbl.trailingAnchor.constraint(equalTo: generalTxtField.trailingAnchor).isActive = true
            generalLbl.topAnchor.constraint(equalTo: generalTxtField.topAnchor).isActive = true
        }
        searchBtn = UIButton.createCustomButton(text: "YADDA SAXLA", textSize: 20)
        searchBtn.addTarget(self, action: #selector(SearchVehicleVC.saveVehicle), for: .touchUpInside)
        scrollView.addSubview(searchBtn)        
        
        searchBtn.widthAnchor.constraint(equalTo: bodyNumberTxtField.widthAnchor).isActive = true
        searchBtn.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        if operationCode == Constants.BORDER_CMTPL_CONTRACT_OPERATION {
            searchBtn.topAnchor.constraint(equalTo: generalTxtField.bottomAnchor, constant: 15).isActive = true
        } else {
            searchBtn.topAnchor.constraint(equalTo: modelTxtField.bottomAnchor, constant: 15).isActive = true
        }
        searchBtn.heightAnchor.constraint(equalTo: bodyNumberTxtField.heightAnchor).isActive = true
        searchBtn.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        
    }

    private func createForm() {
        
        formView.backgroundColor = UIColor.white
        formView.translatesAutoresizingMaskIntoConstraints = false
        formView.layer.cornerRadius = 5
        formView.layer.shadowRadius = 5
        formView.layer.shadowOffset = CGSize.zero
        formView.layer.shadowColor = UIColor.black.cgColor
        formView.layer.shadowOpacity = 0.2
        view.addSubview(formView)
        
        formView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        formView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        formView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.85).isActive = true
        formView.heightAnchor.constraint(greaterThanOrEqualToConstant: 100).isActive = true
        
        if operationCode == Constants.STANDARD_CMTPL_CONTRACT_OPERATION {
            
            let toggleMainLbl: UILabel = {
                let label = UILabel()
                label.translatesAutoresizingMaskIntoConstraints = false
                label.text = "Nəqliyyat vasitəsinin qeydiyyata alan orqan"
                label.font = UIFont(name: Constants.helveticaMedium, size: 15)
                label.textColor = UIColor.mainTextColor
                label.textAlignment = .center
                label.numberOfLines = 0
                return label
            }()
            formView.addSubview(toggleMainLbl)
            
            toggleBtn.translatesAutoresizingMaskIntoConstraints = false
            toggleBtn.isOn = false
            toggleBtn.tintColor = UIColor.labelColor
            toggleBtn.onTintColor = UIColor.mainAppColor
            toggleBtn.addTarget(self, action: #selector(SearchVehicleVC.toggleValueChanged), for: .valueChanged)
            formView.addSubview(toggleBtn)
            
            toggleMiaLabel.translatesAutoresizingMaskIntoConstraints = false
            toggleMiaLabel.text = "DİN"
            toggleMiaLabel.textColor = UIColor.labelColor
            toggleMiaLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 12)
            formView.addSubview(toggleMiaLabel)
            
            toggleMiscLabel.translatesAutoresizingMaskIntoConstraints = false
            toggleMiscLabel.text = "DİGƏR"
            toggleMiscLabel.textColor = UIColor.labelColor
            toggleMiscLabel.font = UIFont(name: "HelveticaNeue-Bold", size: 12)
            formView.addSubview(toggleMiscLabel)
            
            if toggleBtn.isOn {
                
                toggleMiaLabel.textColor = UIColor.labelColor
                toggleMiscLabel.textColor = UIColor.mainTextColor
                
            } else {
                
                toggleMiaLabel.textColor = UIColor.mainTextColor
                toggleMiscLabel.textColor = UIColor.labelColor
                
            }
            
            toggleMainLbl.leftAnchor.constraint(equalTo: formView.leftAnchor, constant: 25).isActive = true
            toggleMainLbl.rightAnchor.constraint(equalTo: formView.rightAnchor, constant: -25).isActive = true
            toggleMainLbl.topAnchor.constraint(equalTo: formView.topAnchor, constant: 25).isActive = true
            
            toggleBtn.topAnchor.constraint(equalTo: toggleMainLbl.bottomAnchor, constant: 15).isActive = true
            toggleBtn.centerXAnchor.constraint(equalTo: formView.centerXAnchor).isActive = true
            
            toggleMiaLabel.centerYAnchor.constraint(equalTo: toggleBtn.centerYAnchor).isActive = true
            toggleMiaLabel.rightAnchor.constraint(equalTo: toggleBtn.leftAnchor, constant: -10).isActive = true
            
            toggleMiscLabel.centerYAnchor.constraint(equalTo: toggleBtn.centerYAnchor).isActive = true
            toggleMiscLabel.leftAnchor.constraint(equalTo: toggleBtn.rightAnchor, constant: 10).isActive = true
        }
        
    }
    
    func removeFromView(_ view: UIView) {
        
        if view == miscForm {
            bodyNumberTxtField.removeFromSuperview()
            engineNumberTxtField.removeFromSuperview()
            carNumberTxtField.removeFromSuperview()
            certificateNumberTxtField.removeFromSuperview()
            yearOfManufactureTxtField.removeFromSuperview()
            vehicleColorTxtField.removeFromSuperview()
            makeTxtField.removeFromSuperview()
            modelTxtField.removeFromSuperview()
            vehicleTypeSelector.removeFromSuperview()
            if generalTxtField != nil && !generalTxtField.isHidden {
                generalTxtField.removeFromSuperview()
            }
            scrollView.removeFromSuperview()
        } else {
            carNumberTxtField.removeFromSuperview()
            certificateNumberTxtField.removeFromSuperview()
        }
        searchBtn.removeFromSuperview()
        view.removeFromSuperview()
        
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0 {
                view.frame.origin.y -= keyboardSize.height / 1.8
            }
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0{
                view.frame.origin.y += keyboardSize.height / 1.8
            }
        }
    }
    
    private func configureRestoredView() {
        
        toggleBtn.isOn = userDefaults.bool(forKey: Constants.miaNonMiaType)
        
        if toggleBtn.isOn || operationCode == Constants.BORDER_CMTPL_CONTRACT_OPERATION {
            
            if operationCode != Constants.BORDER_CMTPL_CONTRACT_OPERATION {
                miaForm.removeFromSuperview()
                createMiscView()
                toggleBtn.isEnabled = false
                toggleMiaLabel.textColor = UIColor.labelColor
                toggleMiscLabel.textColor = UIColor.mainTextColor
            }
            
            certificateNumberTxtField.addTarget(self, action: #selector(saveCertNumber), for: .editingDidEnd)
            carNumberTxtField.addTarget(self, action: #selector(saveCarNumber), for: .editingDidEnd)
            bodyNumberTxtField.addTarget(self, action: #selector(saveBodyNumber), for: .editingDidEnd)
            engineNumberTxtField.addTarget(self, action: #selector(saveEngineNumber), for: .editingDidEnd)
            chassisTxtField.addTarget(self, action: #selector(saveChassisNumber), for: .editingDidEnd)
            yearOfManufactureTxtField.addTarget(self, action: #selector(saveYearOfManufacturer), for: .editingDidEnd)
            vehicleColorTxtField.addTarget(self, action: #selector(saveVehicleColor), for: .editingDidEnd)
            engineNumberTxtField.addTarget(self, action: #selector(saveEngineNumber), for: .editingDidEnd)
            makeTxtField.addTarget(self, action: #selector(saveMake), for: .editingDidEnd)
            modelTxtField.addTarget(self, action: #selector(saveModel), for: .editingDidEnd)
            if operationCode == Constants.BORDER_CMTPL_CONTRACT_OPERATION {
                generalTxtField.addTarget(self, action: #selector(saveGeneral), for: .editingDidEnd)
            }
            
            if let value = userDefaults.string(forKey: Constants.certNumber) {
                certificateNumberTxtField.text = value
            }
            
            if let value = userDefaults.string(forKey: Constants.carNumber) {
                carNumberTxtField.text = value
            }
            
            if let value = userDefaults.string(forKey: Constants.bodyNumber) {
                bodyNumberTxtField.text = value
            }
            
            if let value = userDefaults.string(forKey: Constants.engineNumber) {
                engineNumberTxtField.text = value
            }
            
            if let value = userDefaults.string(forKey: Constants.chassisNumber) {
                chassisTxtField.text = value
            }
            
            if let value = userDefaults.string(forKey: Constants.yearOfManufacturer) {
                yearOfManufactureTxtField.text = value
            }
            
            if let value = userDefaults.string(forKey: Constants.vehicleColor) {
                vehicleColorTxtField.text = value
            }
            
            if let value = userDefaults.string(forKey: Constants.engineNumber) {
                engineNumberTxtField.text = value
            }
            
            if let value = userDefaults.string(forKey: Constants.make) {
                makeTxtField.text = value
            }
            
            if let value = userDefaults.string(forKey: Constants.model) {
                modelTxtField.text = value
            }
            
            if let value = userDefaults.string(forKey: Constants.engineCapacity) {
                engineCapacityTxtField.text = value
            }
            
            if let value = userDefaults.string(forKey: Constants.horsePower) {
                horsePowerTxtField.text = value
            }
            
        } else {
            
            certificateNumberTxtField.addTarget(self, action: #selector(saveCertNumber), for: .editingDidEnd)
            carNumberTxtField.addTarget(self, action: #selector(saveCarNumber), for: .editingDidEnd)
            
            if let value = userDefaults.string(forKey: Constants.certNumber) {
                certificateNumberTxtField.text = value
            }
            
            if let value = userDefaults.string(forKey: Constants.carNumber) {
                carNumberTxtField.text = value
            }
            
        }
        
    }
    
    @objc func saveCertNumber() {
        if certificateNumberTxtField.text != "" {
            userDefaults.set(certificateNumberTxtField.text!, forKey: Constants.certNumber)
        }
    }
    
    @objc func saveCarNumber() {
        if carNumberTxtField.text != "" {
            userDefaults.set(carNumberTxtField.text!, forKey: Constants.carNumber)
        }
    }
    
    @objc func saveBodyNumber() {
        if bodyNumberTxtField.text != "" {
            userDefaults.set(bodyNumberTxtField.text!, forKey: Constants.bodyNumber)
        }
    }
    
    @objc func saveEngineNumber() {
        if engineNumberTxtField.text != "" {
            userDefaults.set(engineNumberTxtField.text!, forKey: Constants.engineNumber)
        }
    }
    
    @objc func saveChassisNumber() {
        if chassisTxtField.text != "" {
            userDefaults.set(chassisTxtField.text!, forKey: Constants.chassisNumber)
        }
    }
    
    @objc func saveYearOfManufacturer() {
        if yearOfManufactureTxtField.text != "" {
            userDefaults.set(yearOfManufactureTxtField.text!, forKey: Constants.yearOfManufacturer)
        }
    }
    
    @objc func saveVehicleColor() {
        if vehicleColorTxtField.text != "" {
            userDefaults.set(vehicleColorTxtField.text!, forKey: Constants.vehicleColor)
        }
    }
    
    @objc func saveMake() {
        if makeTxtField.text != "" {
            userDefaults.set(makeTxtField.text!, forKey: Constants.make)
        }
    }
    
    @objc func saveModel() {
        if modelTxtField.text != "" {
            userDefaults.set(modelTxtField.text!, forKey: Constants.model)
        }
    }
    
    func saveEngineCapacity() {
        if engineCapacityTxtField.text != "" {
            userDefaults.set(engineCapacityTxtField.text!, forKey: Constants.engineCapacity)
        }
    }
    
    @objc func saveGeneral() {
        
        if generalTxtField.text != "" {
            switch selectedVehicleType {
            case 0:
                break
                
            case 1:
                userDefaults.set(generalTxtField.text, forKey: Constants.passengerSeatCount)
                break
                
            case 2:
                userDefaults.set(generalTxtField.text, forKey: Constants.maxPermissibleMass)
                break
                
            case 3:
                userDefaults.set(generalTxtField.text, forKey: Constants.engineCapacity)
                break
                
            case 7:
                userDefaults.set(generalTxtField.text, forKey: Constants.horsePower)
                break
                
            default:
                break
                
            }
        }
    }
    
    func clearVehicleViewFieldValues() {
        
        userDefaults.set(false, forKey: Constants.miaNonMiaType)
        userDefaults.removeObject(forKey: Constants.certNumber)
        userDefaults.removeObject(forKey: Constants.carNumber)
        userDefaults.removeObject(forKey: Constants.bodyNumber)
        userDefaults.removeObject(forKey: Constants.engineNumber)
        userDefaults.removeObject(forKey: Constants.chassisNumber)
        userDefaults.removeObject(forKey: Constants.yearOfManufacturer)
        userDefaults.removeObject(forKey: Constants.vehicleColor)
        userDefaults.removeObject(forKey: Constants.make)
        userDefaults.removeObject(forKey: Constants.model)
        userDefaults.removeObject(forKey: Constants.engineCapacity)
        userDefaults.removeObject(forKey: Constants.horsePower)
        
    }
    
}
