//
//  Validation.swift
//  ISB Mobile
//
//  Created by  Simberg on 11/7/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import Foundation

enum Validation {
    
    case valid
    case failure(String)
    
}
