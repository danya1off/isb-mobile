//
//  GreenCardNewBlankVC.swift
//  ISB Mobile
//
//  Created by  Simberg on 10/10/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD

class GreenCardNewBlankVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    var navigationTitle = ""
    var navigationSubTitle = ""
    
    let blankSeriesPicker = UIPickerView()
    let datePicker = UIDatePicker()
    var blankNumberTxtField: CustomTextField!
    
    let blankTypes = ["TA", "RA", "GA"]
    
    private var gcManager: GreenCardManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gcManager = GreenCardManager(view: self)

        //create main UI
        createUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userDefaults.set(String(describing: self.classForCoder) , forKey: Constants.operationName)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return blankTypes.count + 1
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = (view as? UILabel) ?? UILabel()
        label.font = UIFont(name: Constants.helveticaCondensed, size: 15)
        label.textColor = .mainAppColor
        label.textAlignment = .center
        label.text = row == 0 ? "Blank Seriyasını seçin" : blankTypes[row-1]
        return label
        
    }
    
    @objc func sendBlankInfo() {        
        var blankSeries = ""
        if blankSeriesPicker.selectedRow(inComponent: 0) == 0 {
            fireErrorAlert(withMessage: "Blank Seriya seçilməyib!")
        } else {
            blankSeries = blankTypes[blankSeriesPicker.selectedRow(inComponent: 0)-1]
        }
        let params = ["blankSeries": blankSeries,
                      "blankNumber": blankNumberTxtField.text!,
                      "effectiveDate": datePicker.date.requestDateFormat()]
        gcManager.sendBlank(withParams: params)
    }
    
    @objc func cancelOperation() {
        let contractManager = ContractManager(view: self)
        contractManager.cancelContractOperation()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
