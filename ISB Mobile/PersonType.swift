//
//  PersonType.swift
//  ISB Mobile
//
//  Created by  Simberg on 11/8/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import Foundation

enum PersonType {
    case natural
    case juridical
    case nonResident(String)
}
