//
//  PeriodVC.swift
//  ISB Mobile
//
//  Created by  Simberg on 11/1/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD

class PeriodVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var navigationTitle = ""
    var navigationSubTitle = ""
    
    let formView = UIView()
    let periodPicker = UIPickerView()
    var searchBtn: UIButton!
    
    var periods = ["1 ay", "3 ay", "6 ay", "12 ay"]
    private var contractManager: ContractManager!

    override func viewDidLoad() {
        super.viewDidLoad()
        contractManager = ContractManager(view: self)

        //create main UI
        createUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userDefaults.set(String(describing: self.classForCoder) , forKey: Constants.operationName)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return periods.count + 1
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = (view as? UILabel) ?? UILabel()
        label.font = UIFont(name: Constants.helveticaCondensed, size: 15)
        label.textColor = .mainAppColor
        label.textAlignment = .center
        label.text = row == 0 ? "Sığorta müddəti səçin" : periods[row - 1]
        return label
    }
    
    @objc func sendPeriod() {
        if periodPicker.selectedRow(inComponent: 0) == 0 {
            fireErrorAlert(withMessage: "Sığorta müddəti seçilməyib!")
            return
        }
        let period = periods[periodPicker.selectedRow(inComponent: 0)-1]
        contractManager.contractPrice(withPeriod: period)
    }
    
    @objc func cancelOperation() {
        contractManager.cancelContractOperation()
    }

}
