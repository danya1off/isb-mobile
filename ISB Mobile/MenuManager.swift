//
//  MenuManager.swift
//  ISB Mobile
//
//  Created by  Simberg on 11/7/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD

class MenuManager {
    
    private var participant: ParticipantInfo!
    private var vc: UIViewController!
    
    init(participantInfo: ParticipantInfo, view: UIViewController) {
        self.participant = participantInfo
        self.vc = view
    }
    
    func select(operation: Operation) {
        SVProgressHUD.show(withStatus: "Gözləyin...")
        userDefaults.set(operation.name, forKey: Constants.operationFullName)
        guard !operation.participants.isEmpty else {
            vc.fireErrorAlert(withMessage: "Participants can't be empty!")
            return
        }
        if operation.code == Constants.STANDARD_CMTPL_TERMINATE_CONTRACT_OPERATION
            || operation.code == Constants.BORDER_CMTPL_TERMINATE_CONTRACT_OPERATION
            || operation.code == Constants.GREENCARD_CMTPL_TERMINATE_CONTRACT_OPERATION {
            
            SVProgressHUD.dismiss()
            userDefaults.set(operation.code, forKey: Constants.operationCode)
            ApplicationManager.saveParticipants(participants: operation.participants)
            let viewController = TerminationFormVC()
            vc.present(UINavigationController(rootViewController: viewController), animated: true)
        } else {
            if operation.participants.count == 1 {
                let partis = operation.participants.first!
                guard !partis.insurers.isEmpty else {
                    return
                }
                if partis.insurers.count == 1 {
                    ApplicationManager.saveParticipantInfo(pinCode: participant.pinCode,
                                                           operations: participant.operations,
                                                           operation: operation,
                                                           participant: partis,
                                                           insurer: partis.insurers.first!,
                                                           isParticipantSaved: true)
                    openOperation(withCode: operation.code, participantName: partis.name, insurerName: partis.insurers.first!.name)
                } else {
                    let participantVC = ParticipantVC()
                    participantVC.navigationTitle = "Sığortaçılar"
                    participantVC.isParticipants = false
                    participantVC.insurers = partis.insurers
                    
                    let participantInfo = ParticipantInfo()
                    participantInfo.pinCode = participant.pinCode
                    participantInfo.participantName = partis.name
                    participantInfo.participantTin = partis.tin
                    participantInfo.hasAsanImza = partis.hasAsanImza
                    participantInfo.operation = operation
                    participantInfo.operations = participant.operations
                    participantVC.participantInfo = participantInfo
                    vc.present(UINavigationController(rootViewController: participantVC), animated: true)
                }
            } else {
                SVProgressHUD.dismiss()
                let participantVC = ParticipantVC()
                participantVC.navigationTitle = "İştirakçılar"
                participantVC.isParticipants = true
                participantVC.participants = operation.participants
                
                let participantInfo = ParticipantInfo()
                participantInfo.pinCode = participant.pinCode
                participantInfo.operation = operation
                participantInfo.operations = participant.operations
                participantVC.participantInfo = participantInfo
                vc.present(UINavigationController(rootViewController: participantVC), animated: true)
            }
        }
        
    }
    
    func openOperation(withCode code: String, participantName: String, insurerName: String) {
        userDefaults.set(code, forKey: Constants.operationCode)
        
        switch code {
        case Constants.STANDARD_CMTPL_CONTRACT_OPERATION,
             Constants.BORDER_CMTPL_CONTRACT_OPERATION:
            let viewController = SearchVehicleVC()
            viewController.navigationTitle = participantName
            viewController.navigationSubTitle = insurerName
            startOperation(viewController: viewController, operationCode: code)
            break
            
        case Constants.GREENCARD_CMTPL_CONTRACT_OPERATION:
            let viewController = GreenCardNewBlankVC()
            viewController.navigationTitle = participantName
            viewController.navigationSubTitle = insurerName
            startOperation(viewController: viewController, operationCode: code)
            break
            
        case Constants.STANDARD_CMTPL_VIEW_PARTICIPANT_CONTRACTS,
             Constants.BORDER_CMTPL_VIEW_PARTICIPANT_CONTRACTS,
             Constants.GREENCARD_CMTPL_VIEW_PARTICIPANT_CONTRACTS:
            openAuthorizedContracts(forOperationType: code)
            
        default:
            break
        }
        
    }
    
    private func startOperation(viewController: UIViewController, operationCode: String) {
        SVProgressHUD.show(withStatus: "Gözləyin...")
        let destination = UINavigationController(rootViewController: viewController)
        guard let partis = ApplicationManager.getParticipantInfo() else {
            vc.fireErrorAlert(withMessage: "İştirakçı tapılmadı!")
            return
        }
        Services.startContractOperation(byType: operationCode, forParticipant: partis.participantTin, andInsurer: partis.insurerTin, completion: { (result) in
            SVProgressHUD.dismiss()
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            userDefaults.set(result.data!, forKey: Constants.operationID)
            self.vc.present(destination, animated: true, completion: nil)
        })
    }
    
    func openAuthorizedContracts(forOperationType type: String) {
        SVProgressHUD.show(withStatus: "Gözləyin...")
        
        userDefaults.set(Constants.searchAuthorizedContracts, forKey: Constants.contractsSearchType)
        
        guard let participant = ApplicationManager.getParticipantInfo() else {
            SVProgressHUD.dismiss()
            vc.fireErrorAlert(withMessage: "İştirakçı tapılmadı")
            return
        }
        
        Services.searchAuthorizedContracts(byParticipantTin: participant.participantTin, andInsurerTin: participant.insurerTin, forOperationType: type) { (result) in
            SVProgressHUD.dismiss()
            
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            
            guard let contracts = result.data else {
                self.vc.fireErrorAlert(withMessage: Constants.dataError)
                return
            }
            let contractVC = ContractsTVC()
            contractVC.contracts = contracts
            self.vc.present(UINavigationController(rootViewController: contractVC), animated: true, completion: nil)
        }
    }
    
    func openMyContracts() {
        SVProgressHUD.show(withStatus: "Gözləyin...")
        userDefaults.set(Constants.searchMyContracts, forKey: Constants.contractsSearchType)
        Services.searchMyContracts { (result) in
            SVProgressHUD.dismiss()
            
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            
            guard let contracts = result.data else {
                self.vc.fireErrorAlert(withMessage: Constants.dataError)
                return
            }
            let contractVC = ContractsTVC()
            contractVC.contracts = contracts
            self.vc.present(UINavigationController(rootViewController: contractVC), animated: true, completion: nil)
            
        }
    }
    
}
