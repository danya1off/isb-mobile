//
//  EmployeeInfoCell.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/20/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

class EmployeeInfoCell: UITableViewCell {

    let infoLbl: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "LABEL"
        label.font = UIFont(name: Constants.helveticaBold, size: 12)
        label.textColor = UIColor.labelColor
        return label
    }()
    
    let dataLbl: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Label"
        label.font = UIFont(name: Constants.helveticaBold, size: 20)
        label.textColor = UIColor.mainTextColor
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCustomCell(infoLabel: infoLbl, dataLabel: dataLbl)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func config(withCellCustomizer data: CellDataCustomizer) {
        infoLbl.text = data.label
        dataLbl.text = data.data as? String
    }

}
