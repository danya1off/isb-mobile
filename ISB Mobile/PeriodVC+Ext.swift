//
//  SearchUserVC+Ext.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/20/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

extension PeriodVC {
    
    func createUI() {
        
        view.backgroundColor = UIColor.white
        navigationItem.hidesBackButton = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "close"), style: .plain, target: self, action: #selector(PeriodVC.cancelOperation))
        hideKeyboardWhenTappedAround()
        
        periodPicker.delegate = self
        periodPicker.dataSource = self
        
        CustomProgressHUD.customize()
        
        if let navTitle = userDefaults.string(forKey: Constants.navigationTitle) {
            if navTitle != "" {
                navigationTitle = navTitle
            }
        }
        if let navSubtitle = userDefaults.string(forKey: Constants.navigationSubtitle) {
            if navSubtitle != "" {
                navigationSubTitle = navSubtitle
            }
        }
        
        navigationItem.createCustomNavigationItem(title: navigationTitle, subTitle: navigationSubTitle)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
        
        createMainView()
        
        configureRestoredView()
        
    }
    
    private func createMainView() {
        
        formView.backgroundColor = UIColor.white
        formView.translatesAutoresizingMaskIntoConstraints = false
        formView.layer.cornerRadius = 5
        formView.layer.shadowRadius = 5
        formView.layer.shadowOffset = CGSize.zero
        formView.layer.shadowColor = UIColor.black.cgColor
        formView.layer.shadowOpacity = 0.2
        view.addSubview(formView)
        
        // vehicle type selector
        periodPicker.translatesAutoresizingMaskIntoConstraints = false
        periodPicker.backgroundColor = UIColor(red:0.94, green:0.95, blue:0.97, alpha:1.0)
        periodPicker.layer.cornerRadius = 5
        formView.addSubview(periodPicker)
        
        searchBtn = UIButton.createCustomButton(text: "GÖNDƏR", textSize: 20)
        searchBtn.addTarget(self, action: #selector(PeriodVC.sendPeriod), for: .touchUpInside)
        formView.addSubview(searchBtn)
        
        // init constraints
        
        formView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        formView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        formView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.85).isActive = true
        formView.heightAnchor.constraint(greaterThanOrEqualToConstant: 100).isActive = true
        
        periodPicker.heightAnchor.constraint(equalToConstant: 80).isActive = true
        periodPicker.centerXAnchor.constraint(equalTo: formView.centerXAnchor).isActive = true
        periodPicker.widthAnchor.constraint(equalTo: formView.widthAnchor, constant: -40).isActive = true
        periodPicker.topAnchor.constraint(equalTo: formView.topAnchor, constant: 20).isActive = true
        
        searchBtn.leadingAnchor.constraint(equalTo: periodPicker.leadingAnchor).isActive = true
        searchBtn.trailingAnchor.constraint(equalTo: periodPicker.trailingAnchor).isActive = true
        searchBtn.topAnchor.constraint(equalTo: periodPicker.bottomAnchor, constant: 20).isActive = true
        searchBtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        searchBtn.bottomAnchor.constraint(equalTo: formView.bottomAnchor, constant: -20).isActive = true
        
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y == 0 {
                view.frame.origin.y -= keyboardSize.height / 1.8
            }
        }
    }
    
    @objc func keyboardWillHide(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if view.frame.origin.y != 0{
                view.frame.origin.y += keyboardSize.height / 1.8
            }
        }
    }
    
    private func configureRestoredView() {
        
    }
    
}

