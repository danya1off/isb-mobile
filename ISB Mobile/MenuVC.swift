//
//  MenuVC.swift
//  ISB Mobile
//
//  Created by Jeyhun Danyalov on 9/19/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD

class MenuVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    let headerView = UIView()
    let iconImage = UIImageView()
    let participantLbl = UILabel()
    let tableView = UITableView()
    let settingsBtn = UIButton(type: .custom)
    let contractsBtn = UIButton(type: .custom)
    var participant = ParticipantInfo()
    private var sections = [Section]()
    
    private var menuManager: MenuManager!
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        menuManager = MenuManager(participantInfo: participant, view: self)
        
        //create main UI
        createUI()
        createSections()
    }
    
    // MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userDefaults.set(String(describing: self.classForCoder) , forKey: Constants.operationName)
        userDefaults.set("", forKey: Constants.operationCode)
    }
    
    // MARK: - TableView Functions
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].operations.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 100
        } else {
            return 65
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView: UIView = {
            let view = UIView()
            view.backgroundColor = UIColor.clear
            return view
        }()
        let headerTitle: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.text = sections[section].title
            label.font = UIFont(name: Constants.helveticaCondensed, size: 15)
            label.textColor = UIColor.labelColor
            return label
        }()
        headerView.addSubview(headerTitle)
        headerTitle.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        headerTitle.leftAnchor.constraint(equalTo: headerView.leftAnchor, constant: 72).isActive = true
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.menuCell) as! MenuCell
        let operation = sections[indexPath.section].operations[indexPath.row]
        if Constants.operations.contains(operation.code) {
            cell.config(menuName: operation.name, menuCode: operation.code)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! MenuCell
        let operation = sections[indexPath.section].operations.filter{$0.code == cell.menuCode!}.first
        
        if let operation = operation {
            menuManager.select(operation: operation)
        }
    }
    
    // MARK: - Settings Action
    @objc func settingsAction() {
        // TODO:
    }
    
    // MARK: - OpenMyContracts
    @objc func openMyContracts() {
        menuManager.openMyContracts()
    }
    
    //MARK: - Create Section
    // design main menu table's sections
    private func createSections() {
        var standardOperations = [Operation]()
        var borderOperations = [Operation]()
        var greenCardOperations = [Operation]()
        
        for operation in participant.operations {
            if operation.code.hasPrefix("STANDARD") {
                standardOperations.append(operation)
            } else if operation.code.hasPrefix("BORDER") {
                borderOperations.append(operation)
            } else if operation.code.hasPrefix("GREENCARD") {
                greenCardOperations.append(operation)
            }
        }
        
        let standardSection = Section(title: "Standart", operations: standardOperations)
        sections.append(standardSection)
        
        let borderSection = Section(title: "Sərhəd", operations: borderOperations)
        sections.append(borderSection)
        
        let greenCardSection = Section(title: "Yaşıl kart", operations: greenCardOperations)
        sections.append(greenCardSection)
    }
}
