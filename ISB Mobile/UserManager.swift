//
//  UserManager.swift
//  ISB Mobile
//
//  Created by  Simberg on 11/8/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD

class UserManager {
    
    private var vc: UIViewController!
    private var contractManager: ContractManager!
    
    init(view: UIViewController) {
        self.vc = view
        contractManager = ContractManager(view: vc)
    }
    
    func getPersonInfo(byPersonType type: PersonType, andIdentificator identificator: String) {
        switch type {
        case .natural:
            getNaturalPersonInfo(byIdNumber: identificator)
        case .juridical:
            getJuridicalPersonInfo(byTin: identificator)
        case .nonResident(let docType):
            getNonResidentPersonInfo(byRcNumber: identificator, andDocType: docType)
        }
        
    }
    
    func getNaturalPersonInfo(byIdNumber number: String) {
        SVProgressHUD.show(withStatus: "Gözləyin...")
        Services.getNaturalPersonInfo(byIDDocumentNumber: number, completion: { (result) in
            SVProgressHUD.dismiss()
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            guard let data = result.data else {
                self.vc.fireErrorAlert(withMessage: Constants.dataError)
                return
            }
            (self.vc as! SearchUserVC).clearUserViewFieldValues()
            ApplicationManager.saveNaturalPerson(naturalPerson: data)
            let userDetailsVC = UserDetailsVC()
            userDetailsVC.userInfoArray = data.naturalPersonArray
            self.vc.navigationController?.pushViewController(userDetailsVC, animated: true)
        })
    }
    
    func getJuridicalPersonInfo(byTin tin: String) {
        SVProgressHUD.show(withStatus: "Gözləyin...")
        Services.getJuridicalPersonInfo(byTin: tin, completion: { (result) in
            SVProgressHUD.dismiss()
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            guard let data = result.data else {
                self.vc.fireErrorAlert(withMessage: Constants.dataError)
                return
            }
            (self.vc as! SearchUserVC).clearUserViewFieldValues()
            ApplicationManager.saveJuridicalPerson(juridicalPerson: data)
            let userDetailsVC = UserDetailsVC()
            userDetailsVC.userInfoArray = data.juridicalPersonArray
            self.vc.navigationController?.pushViewController(userDetailsVC, animated: true)
        })
    }
    
    func getNonResidentPersonInfo(byRcNumber number: String, andDocType type: String) {
        SVProgressHUD.show(withStatus: "Gözləyin...")
        Services.getNonResidentPersonInfo(byRcNumber: number, andDocType: type, completion: { (result) in
            SVProgressHUD.dismiss()
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            guard let data = result.data else {
                self.vc.fireErrorAlert(withMessage: Constants.dataError)
                return
            }
            (self.vc as! SearchUserVC).clearUserViewFieldValues()
            ApplicationManager.saveNaturalPerson(naturalPerson: data)
            let userDetailsVC = UserDetailsVC()
            userDetailsVC.userInfoArray = data.naturalPersonArray
            self.vc.navigationController?.pushViewController(userDetailsVC, animated: true)
        })
    }
    
    func saveNonResidentPersonInfo(withData data: NaturalPerson) {
        SVProgressHUD.show(withStatus: "Gözləyin...")
        Services.saveNonResidentPersonInfo(withData: data, completion: { (result) in
            
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            guard let data = result.data else {
                self.vc.fireErrorAlert(withMessage: Constants.dataError)
                return
            }
            ApplicationManager.saveNaturalPerson(naturalPerson: data)
            (self.vc as! SearchUserVC).clearUserViewFieldValues()
            self.contractManager.contractPrice()
        })
    }
    
}
