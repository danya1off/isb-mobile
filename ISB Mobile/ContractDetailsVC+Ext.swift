//
//  ContractDetailsVC+Ext.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/25/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

extension ContractDetailsVC {
    
    func createUI() {
        
        view.backgroundColor = UIColor.white
        tableView.delegate = self
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.separatorInset.left = 20
        tableView.separatorInset.right = 20
        tableView.tableFooterView = UIView()
        tableView.register(ContractDetailsCell.self, forCellReuseIdentifier: Constants.contractDetailsCell)
        
        CustomProgressHUD.customize()
        
        ApplicationManager.saveContractInfo(contractInfo: contract)
        
        let date = Date()
        
        if contract.contractNumber != ""  {
            contractNumberLbl.text = contract.contractNumber
        }
        
        if contract.effectiveDate != ""  {
            effectiveDateLbl.text = date.localFormat(dateString: contract.effectiveDate)
        }
        
        if contract.expiryDate != ""  {
            expiryDateLbl.text = date.localFormat(dateString: contract.expiryDate)
        }
        
        statusLbl.text = contract.status.name
        detailsBgView.backgroundColor = Constants.statusColors[contract.status.code]
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "close"), style: .plain, target: self, action: #selector(close))
        
        createView()
        
    }
    
    private func createView() {
    
        let bgView: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.detailsBgColor
            return view
        }()
        
        view.addSubview(bgView)
        
        detailsBgView.translatesAutoresizingMaskIntoConstraints = false
        detailsBgView.layer.cornerRadius = 5
        detailsBgView.layer.shadowRadius = 5
        detailsBgView.layer.shadowColor = UIColor.black.cgColor
        detailsBgView.layer.shadowOffset = CGSize.zero
        detailsBgView.layer.shadowOpacity = 0.2
        bgView.addSubview(detailsBgView)
        
        let contractLbl = createLbl(withText: "MÜQAVİLƏ NÖMRƏSİ")
        detailsBgView.addSubview(contractLbl)
        
        contractNumberLbl.translatesAutoresizingMaskIntoConstraints = false
        contractNumberLbl.textColor = UIColor.white
        contractNumberLbl.text = "CP3314342"
        contractNumberLbl.font = UIFont(name: Constants.helveticaBold, size: 30)
        detailsBgView.addSubview(contractNumberLbl)
        
        let pdfButton: UIButton = {
            let button = UIButton(type: .custom)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.setImage(#imageLiteral(resourceName: "pdf-icon"), for: .normal)
            button.addTarget(self, action: #selector(ContractDetailsVC.openPDF), for: .touchUpInside)
            return button
        }()
        detailsBgView.addSubview(pdfButton)
        
        let startLbl = createLbl(withText: "QÜVVƏYƏ MİNMƏ TARİXİ")
        detailsBgView.addSubview(startLbl)
        
        effectiveDateLbl.translatesAutoresizingMaskIntoConstraints = false
        effectiveDateLbl.textColor = UIColor.white
        effectiveDateLbl.text = "31.01.2017"
        effectiveDateLbl.font = UIFont(name: Constants.helveticaBold, size: 22)
        detailsBgView.addSubview(effectiveDateLbl)
        
        let expiryLbl = createLbl(withText: "QÜVVƏDƏN DÜŞMƏ TARİXİ")
        detailsBgView.addSubview(expiryLbl)
        
        expiryDateLbl.translatesAutoresizingMaskIntoConstraints = false
        expiryDateLbl.textColor = UIColor.white
        expiryDateLbl.text = "31.01.2017"
        expiryDateLbl.font = UIFont(name: Constants.helveticaBold, size: 22)
        detailsBgView.addSubview(expiryDateLbl)
        
        statusLbl.translatesAutoresizingMaskIntoConstraints = false
        statusLbl.text = "Status"
        statusLbl.textAlignment = .center
        statusLbl.textColor = UIColor.mainTextColor
        statusLbl.font = UIFont(name: Constants.helveticaBold, size: 12)
        bgView.addSubview(statusLbl)
        
        view.addSubview(tableView)
        
        
        // init constraints
        bgView.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor).isActive = true
        bgView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        bgView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.34).isActive = true
        
        detailsBgView.topAnchor.constraint(equalTo: bgView.topAnchor, constant: 17).isActive = true
        detailsBgView.heightAnchor.constraint(equalTo: bgView.heightAnchor, multiplier: 0.76).isActive = true
        detailsBgView.widthAnchor.constraint(equalTo: bgView.widthAnchor, multiplier: 0.86).isActive = true
        detailsBgView.centerXAnchor.constraint(equalTo: bgView.centerXAnchor).isActive = true
    
        contractLbl.topAnchor.constraint(equalTo: detailsBgView.topAnchor, constant: 20).isActive = true
        contractLbl.leftAnchor.constraint(equalTo: detailsBgView.leftAnchor, constant: 20).isActive = true
        
        contractNumberLbl.leadingAnchor.constraint(equalTo: contractLbl.leadingAnchor).isActive = true
        contractNumberLbl.topAnchor.constraint(equalTo: contractLbl.bottomAnchor, constant: 6).isActive = true
        
        pdfButton.centerYAnchor.constraint(equalTo: contractNumberLbl.centerYAnchor).isActive = true
        pdfButton.rightAnchor.constraint(equalTo: detailsBgView.rightAnchor, constant: -20).isActive = true
        pdfButton.heightAnchor.constraint(equalTo: detailsBgView.heightAnchor, multiplier: 0.15).isActive = true
        pdfButton.widthAnchor.constraint(equalTo: pdfButton.heightAnchor).isActive = true
        
        startLbl.leadingAnchor.constraint(equalTo: contractLbl.leadingAnchor).isActive = true
        startLbl.topAnchor.constraint(equalTo: contractNumberLbl.bottomAnchor, constant: 23).isActive = true
        
        effectiveDateLbl.leadingAnchor.constraint(equalTo: startLbl.leadingAnchor).isActive = true
        effectiveDateLbl.topAnchor.constraint(equalTo: startLbl.bottomAnchor, constant: 6).isActive = true
        
        expiryLbl.trailingAnchor.constraint(equalTo: pdfButton.trailingAnchor).isActive = true
        expiryLbl.topAnchor.constraint(equalTo: startLbl.topAnchor).isActive = true
        
        expiryDateLbl.leadingAnchor.constraint(equalTo: expiryLbl.leadingAnchor).isActive = true
        expiryDateLbl.topAnchor.constraint(equalTo: effectiveDateLbl.topAnchor).isActive = true
        
        statusLbl.leadingAnchor.constraint(equalTo: detailsBgView.leadingAnchor).isActive = true
        statusLbl.trailingAnchor.constraint(equalTo: detailsBgView.trailingAnchor).isActive = true
        statusLbl.topAnchor.constraint(equalTo: detailsBgView.bottomAnchor, constant: 11).isActive = true
        
        tableView.topAnchor.constraint(equalTo: bgView.bottomAnchor, constant: 20).isActive = true
        tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        tableView.widthAnchor.constraint(equalTo: bgView.widthAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: bottomLayoutGuide.bottomAnchor).isActive = true
        
    }
    
    private func createLbl(withText text: String) -> UILabel {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = text
        label.font = UIFont(name: Constants.helveticaBold, size: 9)
        label.textColor = UIColor.white
        return label
    }
    
}
