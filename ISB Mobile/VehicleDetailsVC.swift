//
//  VehicleDetailsVC.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/20/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SCLAlertView
import SVProgressHUD

class VehicleDetailsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var navigationTitle = ""
    var navigationSubTitle = ""
    
    let tableView = UITableView()
    var confirmBtn: UIButton!
    var wrongDataBtn: UIButton!
    var vehicleInfoArray = [CellDataCustomizer]()
    
    var vehicleManager: VehicleManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vehicleManager = VehicleManager(view: self)

        //create main UI
        creatUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userDefaults.set(String(describing: self.classForCoder) , forKey: Constants.operationName)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return vehicleInfoArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 100
        } else {
            return 70
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.vehicleInfoCell) as! VehicleDetailsCell
        cell.config(withCellCustomizer: vehicleInfoArray[indexPath.row])
        return cell
    }
    
    @objc func confirmAction() {
        guard let vehicle = ApplicationManager.getVehicleInfo() else {
            fireErrorAlert(withMessage: "Maşın məlumatları tapılmadı!")
            return
        }
        let operationCode = userDefaults.string(forKey: Constants.operationCode)!
        if operationCode != Constants.GREENCARD_CMTPL_CONTRACT_OPERATION
            && vehicle.vehicleTypeID == 7 {
            showHorsePowerView(withVehicle: vehicle, andSubtitle: "At gücünü qeyd edin!")
        } else {
            navigationController?.pushViewController(SearchUserVC(), animated: true)
        }
    }
    
    fileprivate func showHorsePowerView(withVehicle vehicle: VehicleInfo, andSubtitle subTitle: String) {
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "HelveticaNeue", size: 18)!,
            kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
            kButtonFont: UIFont(name: "HelveticaNeue-Medium", size: 14)!,
            showCloseButton: true
        )
        let vehicleHorsePowerAlert = SCLAlertView(appearance: appearance)
        let horsePower = vehicleHorsePowerAlert.addTextField("At gücünü qeyd edin!")
        horsePower.keyboardType = .numberPad
        
        vehicleHorsePowerAlert.addButton("GÖNDƏR") {
            if horsePower.text == "" {
                self.showHorsePowerView(withVehicle: vehicle, andSubtitle: "At gücü boş ola bilməz!")
            } else if !horsePower.text!.numbersOnly {
                self.showHorsePowerView(withVehicle: vehicle, andSubtitle: "At gücü yalnız rəqəm ola bilər!")
            } else {
                self.vehicleManager.send(vehicleHorsePower: Int(horsePower.text!)!)
            }
            
        }
        vehicleHorsePowerAlert.showEdit("\(vehicle.make) \(vehicle.model)", subTitle: subTitle, closeButtonTitle: "BAĞLA")
    }
    
    @objc func cancelOperation() {
        let contractManager = ContractManager(view: self)
        contractManager.cancelContractOperation()
    }
    
    @objc func mismatchVehicleData() {
        SVProgressHUD.show(withStatus: "Gözləyin...")
        Services.mismatchVehicleInfo { (result) in
            SVProgressHUD.dismiss()
            guard result.isSuccess else {
                self.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            self.dismiss(animated: true, completion: {
                ApplicationManager.redirectToMainMenu()
            })
            
        }
    }

}
