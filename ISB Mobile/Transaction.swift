//
//  Transaction.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/20/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import SwiftyJSON

struct Transaction {
    
    var transactionID: String?
    var verificationCode: String?
    
    init(transactionID: String, verificationCode: String) {
        self.transactionID = transactionID
        self.verificationCode = verificationCode
    }
    
    init(withData data: JSON) {
        transactionID = data["transactionId"].stringValue
        verificationCode = data["verificationCode"].stringValue
    }
    
}
