//
//  Constants.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/10/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    
    private init() {}
    
    // MARK: - URL
    static let URL = ""
    static let moduleCodeUUID = ""
    
    // MARK: - REST Methods
    static let startRegistration = "start-registration"
    static let authParams = "auth-params"
    static let authResult = "auth-result"
    static let mismatchPersonInfo = "mismatch-person-info"
    static let registerApp = "register-app"
    static let endRegistration = "end-registration"
    static let startAuthentication = "start-authentication"
    static let authorizations = "authorizations"
    static let endAuthentication = "end-authentication"
    static let startContractOperation = "start-contract-operation"
    static let operationAllowance = "operation-allowance"
    static let vehicleInfo = "vehicle-info"
    static let mismatchVehicleInfo = "mismatch-vehicle-info"
    static let naturalPersonInfo = "natural-person-info"
    static let juridicalPersonInfo = "juridical-person-info"
    static let nonResidentPersonInfo = "non-resident-person-info"
    static let contractPrice = "contract-price"
    static let signContract = "sign-contract"
    static let signingResult = "signing-result"
    static let cancelContractOperation = "cancel-contract-operation"
    static let searchMyContracts = "search-my-contracts"
    static let searchAuthorizedContracts = "search-authorized-contracts"
    static let getContract = "contract"
    static let getContractDoc = "contract-document"
    static let getTerminationContract = "termination-contract"
    static let terminateContract = "terminate-contract"
    static let saveNonResidentPersonInfo = "non-resident-person-info"
    static let nonMiaVehicleInfo = "non-mia-vehicle-info"
    static let blank = "blank"
    static let greenCardContract = "green-card-contract"
    static let vehicleHorsePower = "vehicle-horse-power"
    
    // MARK: - UserDefault parameters
    static let isPhoneRegistered = "isPhoneRegistered"
    static let operationID = "operationId"
    static let operationName = "operationName"
    static let operationFullName = "operationFullName"
    static let operationCode = "operationCode"
    static let appCode = "appCode"
    static let moduleCode = "moduleCode"
    static let timeStamp = "timeStamp"
    static let phoneNumber = "phoneNumber"
    static let asanID = "asanID"
    static let isLoggedInOnceADay = "isLoggedInOnceADay"
    static let savedDate = "savedDate"
    static let token = "token"
    static let navigationTitle = "navigationTitle"
    static let navigationSubtitle = "navigationSubtitle"
    static let greenCard = "greenCard"
    static let contractsSearchType = "contractsSearchType"
    
    // TextField codes
    static let certNumber = "certificationNumber"
    static let carNumber = "carNumber"
    static let idNumber = "idNumber"
    static let personType = "personType"
    static let residencyType = "residencyType"
    static let contractNumber = "contractNumber"
    static let bodyNumber = "bodyNumber"
    static let engineNumber = "engineNumber"
    static let chassisNumber = "chassisNumber"
    static let yearOfManufacturer = "yearOfManufacturer"
    static let vehicleColor = "vehicleColor"
    static let make = "make"
    static let model = "model"
    static let maxPermissibleMass = "maxPermissibleMass"
    static let passengerSeatCount = "passengerSeatCount"
    static let vehicleType = "vehicleType"
    static let engineCapacity = "engineCapacity"
    static let horsePower = "horsePower"
    static let miaNonMiaType = "miaNonMiaType"
    static let lastName = "lastName"
    static let firstName = "firstName"
    static let patronymic = "patronymic"
    static let pin = "pin"
    static let idDocument = "idDocument"
    static let juridicalPersonType = "JURIDICAL_PERSON_TYPE"
    static let naturalPersonType = "NATURAL_PERSON_TYPE"
    
    // MARK: - TextField Labels
    static let carNumberLbl = "DÖVLƏT QEYDİYYAT NİŞANI"
    static let certificateNumberLbl = "NV-NİN QEYDİYYAT ŞƏHADƏTNAMƏSİ"
    static let bodyNumberLbl = "BAN NÖMRƏSİ"
    static let engineNumberLbl = "MÜHƏRRİK NÖMRƏSİ"
    static let chassisNumberLbl = "ŞASSİ NÖMRƏSİ"
    static let vehicleTypeLbl = "NƏQLİYYAT VASİTƏSİNİN TİPİ"
    static let engineCapacityLbl = "MÜHƏRRİKİN HƏCMİ"
    static let passengerSeatCountLbl = "OTURACAĞIN SAYI"
    static let maxPermissibleMassLbl = "MAKSİMAL KÜTLƏ"
    static let makeLbl = "MARKA"
    static let modelLbl = "MODEL"
    static let vehicleColorLbl = "RƏNG"
    static let yearOfManufacturerLbl = "BURAXILIŞ İLİ"
    static let horsePowerLbl = "AT GÜCÜ"
    
    // MARK: - Error messages
    static let dataError = "Məlumatlar düzgün deyil!"
    static let error = "Daxili xəta baş verib. İnzibatçıynan əlaqə saxlayın!"
    static let tokenError = "Əməliyyatı davam etməkçün, daxil olun!"
    
    // MARK: - Cell identifiers
    static let employeeInfoCell = "employeeInfoCell"
    static let menuCell = "menuCell"
    static let participantCell = "participantCell"
    static let vehicleInfoCell = "vehicleInfoCell"
    static let userInfoCell = "userInfoCell"
    static let finalDetailCell = "finalDetailCell"
    static let contractCell = "contractCell"
    static let contractDetailsCell = "contractDetailsCell"
    
    // MARK: - Local storage path's keys
    static let participantInfoPath = "PARTICIPANT-INFO"
    static let participantsPath = "PARTICIPANTS"
    static let naturalPersonPath = "NATURAL"
    static let juridicalPersonPath = "JURIDICAL"
    static let carInfoPath = "CAR"
    static let contractPath = "CONTRACT"
    static let contractPricePath = "CONTRACT-PRICE"
    
    // MARK: - Fonts
    static let helveticaMedium = "HelveticaNeue-Medium"
    static let helveticaCondensed = "HelveticaNeue-CondensedBold"
    static let helveticaBold = "HelveticaNeue-Bold"
    static let helveticaLight = "HelveticaNeue-Light"
    static let helveticaRegular = "HelveticaNeue"
    
    // MARK: - Status colors
    static let statusColors: [String: UIColor] = ["TERMINATED_CONTRACT_STATUS": UIColor(red:0.57, green:0.54, blue:0.59, alpha:1.0),
                                                  "CANCELLED_CONTRACT_STATUS": UIColor(red:0.91, green:0.30, blue:0.24, alpha:1.0),
                                                  "EXPIRED_CONTRACT_STATUS": UIColor(red:0.61, green:0.35, blue:0.71, alpha:1.0),
                                                  "ONGOING_CONTRACT_STATUS": UIColor(red:0.10, green:0.74, blue:0.61, alpha:1.0),
                                                  "PENDING_CONTRACT_STATUS": UIColor(red:0.95, green:0.61, blue:0.07, alpha:1.0),
                                                  "DRAFTED_CONTRACT_STATUS": UIColor(red:0.20, green:0.60, blue:0.86, alpha:1.0)]
    
    // MARK: - Operation Codes
    static let STANDARD_CMTPL_CONTRACT_OPERATION = "STANDARD_CMTPL_CONTRACT_OPERATION"
    static let STANDARD_CMTPL_TERMINATE_CONTRACT_OPERATION = "STANDARD_CMTPL_TERMINATE_CONTRACT_OPERATION"
    static let STANDARD_CMTPL_VIEW_PARTICIPANT_CONTRACTS = "STANDARD_CMTPL_VIEW_PARTICIPANT_CONTRACTS"
    
    static let BORDER_CMTPL_CONTRACT_OPERATION = "BORDER_CMTPL_CONTRACT_OPERATION"
    static let BORDER_CMTPL_TERMINATE_CONTRACT_OPERATION = "BORDER_CMTPL_TERMINATE_CONTRACT_OPERATION"
    static let BORDER_CMTPL_VIEW_PARTICIPANT_CONTRACTS = "BORDER_CMTPL_VIEW_PARTICIPANT_CONTRACTS"
    
    static let GREENCARD_CMTPL_CONTRACT_OPERATION = "GREENCARD_CMTPL_CONTRACT_OPERATION"
    static let GREENCARD_CMTPL_TERMINATE_CONTRACT_OPERATION = "GREENCARD_CMTPL_TERMINATE_CONTRACT_OPERATION"
    static let GREENCARD_CMTPL_VIEW_PARTICIPANT_CONTRACTS = "GREENCARD_CMTPL_VIEW_PARTICIPANT_CONTRACTS"
    
    static let operations = [Constants.STANDARD_CMTPL_CONTRACT_OPERATION,
                             Constants.STANDARD_CMTPL_TERMINATE_CONTRACT_OPERATION,
                             Constants.STANDARD_CMTPL_VIEW_PARTICIPANT_CONTRACTS,
                             
                             Constants.BORDER_CMTPL_CONTRACT_OPERATION,
                             Constants.BORDER_CMTPL_TERMINATE_CONTRACT_OPERATION,
                             Constants.BORDER_CMTPL_VIEW_PARTICIPANT_CONTRACTS,
                             
                             Constants.GREENCARD_CMTPL_CONTRACT_OPERATION,
                             Constants.GREENCARD_CMTPL_TERMINATE_CONTRACT_OPERATION,
                             Constants.GREENCARD_CMTPL_VIEW_PARTICIPANT_CONTRACTS]
    
}
