//
//  Insurer.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/21/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation

class Insurer: NSObject, NSCoding {
    
    var name = ""
    var tin = ""
    
    init(name: String, tin: String, operations: [Operation]) {
        self.name = name
        self.tin = tin
    }
    
    init(name: String, tin: String) {
        self.name = name
        self.tin = tin
    }
    
    override init() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        if let data = aDecoder.decodeObject(forKey: Keys.name) as? String {
            name = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.tin) as? String {
            tin = data
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: Keys.name)
        aCoder.encode(tin, forKey: Keys.tin)
    }
    
    struct Keys {
        static let name = "name"
        static let tin = "tin"
    }
    
}
