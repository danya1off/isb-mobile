//
//  CustomProtocol.swift
//  ISB Mobile
//
//  Created by  Simberg on 10/30/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import Foundation
import SVProgressHUD

class CustomProgressHUD {
    
    class func customize() {
        
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setFont(UIFont(name: Constants.helveticaMedium, size: 15)!)
        SVProgressHUD.setDefaultStyle(.light)
        SVProgressHUD.setForegroundColor(UIColor.mainTextColor)
        SVProgressHUD.setMaximumDismissTimeInterval(TimeInterval(exactly: 30)!)
        SVProgressHUD.setStatus("Gözləyin...")
        
    }
    
}
