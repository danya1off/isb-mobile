//
//  InsuredPerson.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/31/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation

class InsuredPerson: NSObject, NSCoding {
    
    var personType = ""
    var fullName = ""
    var lastName = ""
    var firstName = ""
    var patronymic = ""
    var pin = ""
    var address = ""
    var idDocument = ""
    
    override init() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        if let data = aDecoder.decodeObject(forKey: Keys.personType) as? String {
            personType = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.fullName) as? String {
            fullName = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.lastName) as? String {
            lastName = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.firstName) as? String {
            firstName = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.patronymic) as? String {
            patronymic = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.pin) as? String {
            pin = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.address) as? String {
            address = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.idDocument) as? String {
            idDocument = data
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(personType, forKey: Keys.personType)
        aCoder.encode(fullName, forKey: Keys.fullName)
        aCoder.encode(lastName, forKey: Keys.lastName)
        aCoder.encode(firstName, forKey: Keys.firstName)
        aCoder.encode(patronymic, forKey: Keys.patronymic)
        aCoder.encode(pin, forKey: Keys.pin)
        aCoder.encode(address, forKey: Keys.address)
        aCoder.encode(idDocument, forKey: Keys.idDocument)
    }
    
    struct Keys {
        static var personType = "personType"
        static var fullName = "fullName"
        static var lastName = "lastName"
        static var firstName = "firstName"
        static var patronymic = "patronymic"
        static var pin = "pin"
        static var address = "address"
        static var idDocument = "idDocument"
    }
    
}
