//
//  MenuModel.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/22/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation

struct MenuModel {
    
    var userFullName: String
    var operations: [Operation]
    var participantTin: String
    var insurerTin: String
    
}
