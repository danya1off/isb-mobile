//
//  UserDetailsVC.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/21/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD

class UserDetailsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var navigationTitle = ""
    var navigationSubTitle = ""
    var fromBorder = false
    
    let tableView = UITableView()
    var confirmBtn: UIButton!
    var wrongDataBtn: UIButton!
    var userInfoArray = [CellDataCustomizer]()
    
    private var userManager: UserManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userManager = UserManager(view: self)
        
        //create main UI
        createUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userDefaults.set(String(describing: self.classForCoder) , forKey: Constants.operationName)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userInfoArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 100
        } else {
            return 70
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.userInfoCell) as! UserDetailsCell
        cell.config(withCellCustomizer: userInfoArray[indexPath.row])
        return cell
    }
    
    @objc func confirmAction() {
        guard let operationCode = userDefaults.string(forKey: Constants.operationCode) else {
            fireErrorAlert(withMessage: "Əməliyyat kodu tapılmadı!")
            return
        }
        if operationCode == Constants.BORDER_CMTPL_CONTRACT_OPERATION
            || operationCode == Constants.GREENCARD_CMTPL_CONTRACT_OPERATION {
            let periodVC = PeriodVC()
            self.navigationController?.pushViewController(periodVC, animated: true)
            
        } else {
            let contractManager = ContractManager(view: self)
            contractManager.contractPrice()
        }
    }
    
    @objc func cancelOperation() {
        let contractManager = ContractManager(view: self)
        contractManager.cancelContractOperation()
    }
    
    @objc func mismatchUserData() {
        let employeeManager = EmployeeManager(view: self)
        employeeManager.mismatchPersonInfo()
    }

}
