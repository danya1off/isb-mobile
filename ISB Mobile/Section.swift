//
//  File.swift
//  ISB Mobile
//
//  Created by  Simberg on 10/3/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import Foundation

struct Section {
    
    var title: String!
    var operations: [Operation]!
    
    init(title: String, operations: [Operation]) {
        self.title = title
        self.operations = operations
    }
    
}
