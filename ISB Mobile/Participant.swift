//
//  Participant.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/21/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation

class Participant: NSObject, NSCoding {
    
    var name = ""
    var tin = ""
    var hasAsanImza = false
    var insurers = [Insurer]()
    
    init(name: String, tin: String, hasAsanImza: Bool, insurers: [Insurer]) {
        self.name = name
        self.tin = tin
        self.hasAsanImza = hasAsanImza
        self.insurers = insurers
    }
    
    init(name: String, tin: String, hasAsanImza: Bool) {
        self.name = name
        self.tin = tin
        self.hasAsanImza = hasAsanImza
        self.insurers = [Insurer]()
    }
    
    override init() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        if let data = aDecoder.decodeObject(forKey: Keys.name) as? String {
            self.name = data
        }
        
        if let data = aDecoder.decodeObject(forKey: Keys.tin) as? String {
            tin = data
        }
        
        if let data = aDecoder.decodeObject(forKey: Keys.insurers) as? [Insurer] {
            insurers = data
        }
        
        hasAsanImza = aDecoder.decodeBool(forKey: Keys.hasAsanImza)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: Keys.name)
        aCoder.encode(tin, forKey: Keys.tin)
        aCoder.encode(hasAsanImza, forKey: Keys.hasAsanImza)
        aCoder.encode(insurers, forKey: Keys.insurers)
    }
    
    struct Keys {
        static let name = "name"
        static let tin = "tin"
        static let hasAsanImza = "hasAsanImza"
        static let insurers = "insurers"
    }
    
}
