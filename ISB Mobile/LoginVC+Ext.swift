//
//  LoginVC+Ext.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/19/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

extension LoginVC {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func createUI() {
        view.backgroundColor = UIColor.white
//        hideKeyboardWhenTappedAround()
        CustomProgressHUD.customize()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        createView()
        
    }
    
    private func createView() {
        
        let iconTopAnchor: CGFloat!
        let textFieldHeight: CGFloat!
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            
            iconTopAnchor = 70
            textFieldHeight = 70
            
        } else {
            
            iconTopAnchor = 50
            textFieldHeight = 50
            
        }
        
        // header view
        view.addSubview(headerView)
        headerView.backgroundColor = UIColor.mainAppColor
        headerView.translatesAutoresizingMaskIntoConstraints = false
        
        // icon and label
        view.addSubview(iconImage)
        iconImage.translatesAutoresizingMaskIntoConstraints = false
        iconImage.image = UIImage(named: "Logo")
        
        let label: UILabel = {
            let l = UILabel()
            l.translatesAutoresizingMaskIntoConstraints = false
            l.font = UIFont(name: Constants.helveticaLight, size: 17)
            l.textColor = UIColor.white
            l.text = "QEYDİYYAT"
            return l
        }()
        view.addSubview(label)
        
        // login form
        view.addSubview(loginFormView)
        loginFormView.translatesAutoresizingMaskIntoConstraints = false
        loginFormView.backgroundColor = UIColor.white
        loginFormView.layer.cornerRadius = 5
        loginFormView.layer.shadowColor = UIColor.black.cgColor
        loginFormView.layer.shadowOpacity = 0.2
        loginFormView.layer.shadowOffset = CGSize.zero
        
        // phone number text field
        phoneNumberTxtField = CustomTextField.createCustomTxtField(placeholder: "994513039809", textSize: 20)
        phoneNumberTxtField.keyboardType = .numbersAndPunctuation
        phoneNumberTxtField.delegate = self
        view.addSubview(phoneNumberTxtField)
        
        let phoneNumberLbl = UILabel.createLabel(withText: "TELEFON NÖMRƏSİ", andSize: 12)
        view.addSubview(phoneNumberLbl)
        
        // userID text field
        userIDTxtField = CustomTextField.createCustomTxtField(placeholder: "agent", textSize: 20)
        userIDTxtField.delegate = self
        view.addSubview(userIDTxtField)
        
        let userIDLbl = UILabel.createLabel(withText: "İSTİFADƏÇİ İD-Sİ", andSize: 12)
        view.addSubview(userIDLbl)
        
        // create login button
        loginBtn = UIButton.createCustomButton(text: "DAXİL OL", textSize: 20)
        loginBtn.addTarget(self, action: #selector(LoginVC.login), for: .touchUpInside)
        view.addSubview(loginBtn)
        
        // init contraints
        headerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        headerView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        headerView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        headerView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5).isActive = true
        
        iconImage.topAnchor.constraint(equalTo: view.topAnchor, constant: iconTopAnchor).isActive = true
        iconImage.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.27).isActive = true
        iconImage.heightAnchor.constraint(equalTo: iconImage.widthAnchor, multiplier: 1).isActive = true
        iconImage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        label.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        label.topAnchor.constraint(equalTo: iconImage.bottomAnchor, constant: 10).isActive = true
        
        loginFormView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        loginFormView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        loginFormView.heightAnchor.constraint(greaterThanOrEqualToConstant: 100).isActive = true
        loginFormView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.85).isActive = true
        
        phoneNumberTxtField.heightAnchor.constraint(equalToConstant: textFieldHeight).isActive = true
        phoneNumberTxtField.centerXAnchor.constraint(equalTo: loginFormView.centerXAnchor).isActive = true
        phoneNumberTxtField.topAnchor.constraint(equalTo: loginFormView.topAnchor, constant: 35).isActive = true
        phoneNumberTxtField.widthAnchor.constraint(equalTo: loginFormView.widthAnchor, constant: -40).isActive = true
        
        phoneNumberLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
        phoneNumberLbl.leadingAnchor.constraint(equalTo: phoneNumberTxtField.leadingAnchor).isActive = true
        phoneNumberLbl.trailingAnchor.constraint(equalTo: phoneNumberTxtField.trailingAnchor).isActive = true
        phoneNumberLbl.topAnchor.constraint(equalTo: phoneNumberTxtField.topAnchor).isActive = true
        
        userIDTxtField.heightAnchor.constraint(equalTo: phoneNumberTxtField.heightAnchor).isActive = true
        userIDTxtField.leadingAnchor.constraint(equalTo: phoneNumberTxtField.leadingAnchor).isActive = true
        userIDTxtField.trailingAnchor.constraint(equalTo: phoneNumberTxtField.trailingAnchor).isActive = true
        userIDTxtField.topAnchor.constraint(equalTo: phoneNumberTxtField.bottomAnchor, constant: 25).isActive = true
        
        userIDLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
        userIDLbl.leadingAnchor.constraint(equalTo: userIDTxtField.leadingAnchor).isActive = true
        userIDLbl.trailingAnchor.constraint(equalTo: userIDTxtField.trailingAnchor).isActive = true
        userIDLbl.topAnchor.constraint(equalTo: userIDTxtField.topAnchor).isActive = true
        
        loginBtn.heightAnchor.constraint(equalTo: phoneNumberTxtField.heightAnchor).isActive = true
        loginBtn.centerXAnchor.constraint(equalTo: loginFormView.centerXAnchor).isActive = true
        loginBtn.widthAnchor.constraint(equalTo: loginFormView.widthAnchor, constant: -50).isActive = true
        loginBtn.topAnchor.constraint(equalTo: userIDTxtField.bottomAnchor, constant: 25).isActive = true
        loginBtn.bottomAnchor.constraint(equalTo: loginFormView.bottomAnchor, constant: 25).isActive = true
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height/3
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height/3
            }
        }
    }
    
}
