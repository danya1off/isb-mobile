//
//  TerminationManager.swift
//  ISB Mobile
//
//  Created by  Simberg on 11/8/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD
import SCLAlertView

class TerminationManager {
    
    private var vc: UIViewController!
    
    init(view: UIViewController) {
        self.vc = view
    }
    
    func getTerminationContract(byNumber number: String, andOperationType type: String) {
        SVProgressHUD.show(withStatus: "Gözləyin...")
        Services.getTerminationContract(byContractNumber: number, andOperationType: type) { (result) in
            SVProgressHUD.dismiss()
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            
            guard let contract = result.data else {
                self.vc.fireErrorAlert(withMessage: Constants.dataError)
                return
            }
            
            let terminationCommitVC = TerminationCommitVC()
            terminationCommitVC.modalTransitionStyle = .crossDissolve
            terminationCommitVC.insuredPersonName = contract.insuredPerson.fullName
            terminationCommitVC.carNumber = contract.vehicle.carNumber
            terminationCommitVC.contractNumber = contract.contractNumber
            terminationCommitVC.terminationReturningAmount = contract.terminationReturningAmount
            terminationCommitVC.insuredTin = contract.insuranceCompanyTIN
            self.vc.navigationController?.pushViewController(terminationCommitVC, animated: true)
            
        }
    }
    
    func terminateContract(byNumber number: String, forTin tin: String, withReason reason: String, fromDate date: String) {
        SVProgressHUD.show(withStatus: "Gözləyin...")
        Services.terminateContract(byContractNumber: number, forParticipant: tin, withReason: reason, fromDate: date, completion: { (result) in
            SVProgressHUD.dismiss()
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            let appearance = SCLAlertView.SCLAppearance(
                kTitleFont: UIFont(name: "HelveticaNeue", size: 18)!,
                kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                kButtonFont: UIFont(name: "HelveticaNeue-Medium", size: 14)!,
                showCloseButton: false
            )
            let alert = SCLAlertView(appearance: appearance)
            alert.addButton("BAĞLA", action: {
                // remove saved participants from memory
                userDefaults.removeObject(forKey: Constants.participantsPath)
                ApplicationManager.redirectToMainMenu()
                
            })
            alert.showSuccess("Məlumat", subTitle: "Müqavilə üğurla xitam olundu!")
        })
    }
    
}
