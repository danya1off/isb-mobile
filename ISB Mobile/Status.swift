//
//  Status.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/31/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation

class Status: NSObject, NSCoding {
    
    private var _code: String = ""
    private var _name: String = ""
    
    override init() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        if let data = aDecoder.decodeObject(forKey: Keys.code) as? String {
            _code = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.name) as? String {
            _name = data
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(_code, forKey: Keys.code)
        aCoder.encode(_name, forKey: Keys.name)
    }
    
    struct Keys {
        static var code = "code"
        static var name = "name"
    }
    
    var code: String {
        get {
            return _code
        }
        set {
            _code = newValue
        }
    }
    
    var name: String {
        get {
            return _name
        }
        set {
            _name = newValue
        }
    }
    
}
