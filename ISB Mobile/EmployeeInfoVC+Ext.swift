//
//  EmployeeInfoVC+Ext.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/20/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

extension EmployeeInfoVC {
    
    func createUI() {
        view.backgroundColor = UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.5)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(EmployeeInfoCell.self, forCellReuseIdentifier: Constants.employeeInfoCell)
        tableView.tableFooterView = UIView()
        tableView.separatorInset.left = 20
        tableView.separatorInset.right = 20
        
        CustomProgressHUD.customize()
        
        // create custom UI
        createMainView()
        
    }
    
    private func createMainView() {
        
        let shadowView: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.layer.cornerRadius = 5
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOffset = CGSize.zero
            view.layer.shadowOpacity = 0.2
            view.layer.shadowRadius = 5
            return view
        }()
        view.addSubview(shadowView)
        
        mainView.translatesAutoresizingMaskIntoConstraints = false
        mainView.backgroundColor = UIColor.white
        mainView.layer.masksToBounds = true
        mainView.layer.cornerRadius = 5
        view.addSubview(mainView)
        
        let headerView: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.mainAppColor
            return view
        }()
        mainView.addSubview(headerView)
        
        let headerLabel: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.textColor = UIColor.white
            label.text = "İŞÇİ MƏLUMATLARI"
            label.font = UIFont(name: Constants.helveticaCondensed, size: 15)
            return label
        }()
        headerView.addSubview(headerLabel)
        
        cancelBtn = UIButton.createCustomButton(text: "İMTİNA", textSize: 15)
        cancelBtn.addTarget(self, action: #selector(EmployeeInfoVC.mismatchAction), for: .touchUpInside)
        cancelBtn.layer.cornerRadius = 0
        mainView.addSubview(cancelBtn)
        
        confirmBtn = UIButton.createCustomButton(text: "TƏSDİQLƏ", textSize: 15)
        confirmBtn.addTarget(self, action: #selector(EmployeeInfoVC.confirmAction), for: .touchUpInside)
        confirmBtn.layer.cornerRadius = 0
        mainView.addSubview(confirmBtn)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        mainView.addSubview(tableView)
        
        let separator: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.white
            return view
        }()
        cancelBtn.addSubview(separator)
        
        // init constraints
        shadowView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        shadowView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        shadowView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.85).isActive = true
        shadowView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.85).isActive = true
        
        mainView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        mainView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        mainView.heightAnchor.constraint(equalTo: shadowView.heightAnchor).isActive = true
        mainView.widthAnchor.constraint(equalTo: shadowView.widthAnchor).isActive = true
        
        headerView.centerXAnchor.constraint(equalTo: mainView.centerXAnchor).isActive = true
        headerView.topAnchor.constraint(equalTo: mainView.topAnchor).isActive = true
        headerView.widthAnchor.constraint(equalTo: mainView.widthAnchor).isActive = true
        headerView.heightAnchor.constraint(equalTo: mainView.heightAnchor, multiplier: 0.08).isActive = true
        
        headerLabel.centerXAnchor.constraint(equalTo: headerView.centerXAnchor).isActive = true
        headerLabel.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        
        cancelBtn.heightAnchor.constraint(equalTo: headerView.heightAnchor).isActive = true
        cancelBtn.widthAnchor.constraint(equalTo: mainView.widthAnchor, multiplier: 0.5).isActive = true
        cancelBtn.leftAnchor.constraint(equalTo: mainView.leftAnchor).isActive = true
        cancelBtn.bottomAnchor.constraint(equalTo: mainView.bottomAnchor).isActive = true
        
        separator.centerYAnchor.constraint(equalTo: cancelBtn.centerYAnchor).isActive = true
        separator.widthAnchor.constraint(equalToConstant: 1).isActive = true
        separator.rightAnchor.constraint(equalTo: cancelBtn.rightAnchor).isActive = true
        separator.heightAnchor.constraint(equalTo: cancelBtn.heightAnchor, multiplier: 0.6).isActive = true
        
        confirmBtn.heightAnchor.constraint(equalTo: cancelBtn.heightAnchor).isActive = true
        confirmBtn.widthAnchor.constraint(equalTo: cancelBtn.widthAnchor).isActive = true
        confirmBtn.rightAnchor.constraint(equalTo: mainView.rightAnchor).isActive = true
        confirmBtn.bottomAnchor.constraint(equalTo: mainView.bottomAnchor).isActive = true
        
        tableView.topAnchor.constraint(equalTo: headerView.bottomAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: cancelBtn.topAnchor).isActive = true
        tableView.centerXAnchor.constraint(equalTo: mainView.centerXAnchor).isActive = true
        tableView.widthAnchor.constraint(equalTo: mainView.widthAnchor).isActive = true
        
    }
    
}
