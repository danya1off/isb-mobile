//
//  TerminationCommitVC.swift
//  ISB Mobile
//
//  Created by Jeyhun Danyalov on 9/21/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SCLAlertView
import SVProgressHUD

class TerminationCommitVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {

    let datePicker = UIDatePicker()
    var insuredPersonLbl: UILabel!
    var carNumberLbl: UILabel!
    var terminationReasonTxtField: CustomTextField!
    var participantInfoLbl = UILabel()
    var participantsPicker = UIPickerView()
    
    var insuredPersonName = ""
    var carNumber = ""
    var contractNumber = ""
    var terminationReturningAmount: Double!
    var insuredTin: String?
    var participants: [Participant]?
    
    private var terminationManager: TerminationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        terminationManager = TerminationManager(view: self)
        
        if let data = ApplicationManager.getParticipants() {
            guard let tin = insuredTin else {
                self.fireErrorAlert(withMessage: "Insured TIN not found!")
                return
            }
            participants = data.filter{$0.insurers.contains{ $0.tin == tin }}
            if let parts = participants {
                if parts.count == 1 {
                    participantInfoLbl.text = parts.first!.name
                }
            }
        }
        
        //create main UI
        createUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userDefaults.set(String(describing: self.classForCoder) , forKey: Constants.operationName)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if let data = participants {
            return data.count + 1
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = (view as? UILabel) ?? UILabel()
        if let data = participants {
            label.font = UIFont(name: Constants.helveticaCondensed, size: 15)
            label.textColor = .mainAppColor
            label.textAlignment = .center
            label.text = row == 0 ? "İştirakçını seçin" : data[row - 1].name
        }
        return label
    }
    
    @objc func terminateAction() {
        switch checkInputs() {
        case .valid:
            var participantTin = ""
            if let data = participants {
                if data.count == 1 {
                    participantTin = data.first!.tin
                } else if data.count > 1 {
                    participantTin = data[participantsPicker.selectedRow(inComponent: 0)].tin
                }
            } else {
                guard let data = ApplicationManager.getParticipantInfo() else {
                    self.fireErrorAlert(withMessage: "İştirakçı tapılmadı!")
                    return
                }
                participantTin = data.participantTin
            }
            terminationManager.terminateContract(byNumber: contractNumber, forTin: participantTin, withReason: terminationReasonTxtField.text!, fromDate: datePicker.date.requestDateFormat())
        case .failure(let error):
            fireErrorAlert(withMessage: error)
        }
    }
    
    fileprivate func checkInputs() -> Validation {
        if contractNumber == "" {
            return .failure("Müqavilə nömrə boş ola bilməz!")
        }
        if terminationReasonTxtField.text == "" {
            return .failure("Səbəb boş ola bilməz!")
        }
        if let data = participants, data.count > 1 && participantsPicker.selectedRow(inComponent: 0) == 0 {
            return .failure("İştirakçı seçilməyib!")
        }
        return .valid
    }
    
    @objc func cancelOperation() {
        ApplicationManager.redirectToMainMenu()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
