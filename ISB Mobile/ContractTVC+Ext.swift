//
//  ContractTVC+Ext.swift
//  ISB Mobile
//
//  Created by Jeyhun Danyalov on 9/20/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

extension ContractsTVC {
    
    func createUI() {
        
        CustomProgressHUD.customize()
        
        tableView.register(ContractCell.self, forCellReuseIdentifier: Constants.contractCell)
        tableView.tableFooterView = UIView()
        tableView.separatorStyle = .none
        navigationItem.title = "MÜQAVİLƏLƏR"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "close"), style: .plain, target: self, action: #selector(ContractsTVC.closeContracts))
        
        createView()
        
    }
    
    private func createView() {
        
        
        
    }
    
}
