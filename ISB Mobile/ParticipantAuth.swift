//
//  ParticipantAuth.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/21/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ParticipantAuth {
    
    var pinCode: String
    var operations: [Operation]
    
    init(withData data: JSON) {
        
        pinCode = data["pinCode"].stringValue
        operations = [Operation]()
        
        for operationItem in data["operations"].arrayValue {
            
            let name = operationItem["name"].stringValue
            let code = operationItem["code"].stringValue
            let operation = Operation(code: code, name: name)
            
            for participantItem in operationItem["participants"].arrayValue {
                
                let participantName = participantItem["name"].stringValue
                let participantTin = participantItem["tin"].stringValue
                let participantHasAsanImza = participantItem["hasAsanImza"].boolValue
                let participant = Participant(name: participantName, tin: participantTin, hasAsanImza: participantHasAsanImza)
                
                for insurerItem in participantItem["insurers"].arrayValue {
                    
                    let insurerName = insurerItem["name"].stringValue
                    let insurerTin = insurerItem["tin"].stringValue
                    let insurer = Insurer(name: insurerName, tin: insurerTin)
                    participant.insurers.append(insurer)
                    
                }
                operation.participants.append(participant)
                
            }
            operations.append(operation)
            
        }
        
    }
    
}
