//
//  ContractsTVC.swift
//  ISB Mobile
//
//  Created by Jeyhun Danyalov on 9/20/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD

class ContractsTVC: UITableViewController {
    
    var contracts = [Contract]()
    var fromSessionRestore = false
    
    private var menuManager: MenuManager!
    private var contractManager: ContractManager!

    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        menuManager = MenuManager(participantInfo: ParticipantInfo(), view: self)
        contractManager = ContractManager(view: self)
        
        //create main UI
        createUI()
        
        // if session restored in this controller, show all contracts
        if fromSessionRestore {
            callContractsService()
        }

    }
    
    //MARK: - viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userDefaults.set(String(describing: self.classForCoder) , forKey: Constants.operationName)
    }

    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contracts.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 120
        } else {
            return 95
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.contractCell) as! ContractCell
        cell.configure(contract: contracts[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        SVProgressHUD.show(withStatus: "Gözləyin...")
        let contract = contracts[indexPath.row]
        
        if contract.contractNumber != ""  {
            contractManager.getContract(withContractNumber: contract.contractNumber)
        } else {
            SVProgressHUD.dismiss()
            self.fireErrorAlert(withMessage: "Müqavilə nömrə tapılmadı!")
        }
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.alpha = 0.0
        let transform = CATransform3DTranslate(CATransform3DIdentity, -250, 0, 0)
        cell.layer.transform = transform
        
        UIView.animate(withDuration: 0.5) {
            cell.alpha = 1.0
            cell.layer.transform = CATransform3DIdentity
        }
    }
    
    //MARK: - Close Contracts
    @ objc func closeContracts() {
        ApplicationManager.redirectToMainMenu()
    }
    
    //MARK: - Call Contracts
    func callContractsService() {
        guard let searchType = userDefaults.string(forKey: Constants.contractsSearchType) else {
            return
        }
        if searchType == Constants.searchAuthorizedContracts {
            guard let operationCode = userDefaults.string(forKey: Constants.operationCode) else {
                fireErrorAlert(withMessage: "Əməliyyat kodu tapılmadı!")
                return
            }
            menuManager.openAuthorizedContracts(forOperationType: operationCode)
        } else if searchType == Constants.searchMyContracts {
            menuManager.openMyContracts()
        }
    }

}
