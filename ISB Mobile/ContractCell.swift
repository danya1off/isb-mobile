//
//  ContractCell.swift
//  ISB Mobile
//
//  Created by Jeyhun Danyalov on 9/20/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

class ContractCell: UITableViewCell {
    
    let insuredNameLbl: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: Constants.helveticaBold, size: 14)
        label.textColor = UIColor.mainTextColor
        label.text = "Label"
        return label
    }()
    
    let contractNumberLbl: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: Constants.helveticaMedium, size: 9)
        label.textColor = UIColor(red:0.82, green:0.82, blue:0.82, alpha:1.0)
        label.text = "PIB1775060071"
        return label
    }()
    
    let priceLbl: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: Constants.helveticaMedium, size: 9)
        label.textColor = UIColor(red:0.82, green:0.82, blue:0.82, alpha:1.0)
        label.text = "Label"
        label.textAlignment = .left
        return label
    }()
    
    let carNumberLbl: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: Constants.helveticaMedium, size: 9)
        label.textColor = UIColor(red:0.82, green:0.82, blue:0.82, alpha:1.0)
        label.text = "Label"
        label.textAlignment = .left
        return label
    }()
    
    let statusLbl: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: Constants.helveticaMedium, size: 11)
        label.textColor = UIColor.detailsColor
        label.text = "Label"
        return label
    }()
    
    let operationTypeLbl: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: Constants.helveticaMedium, size: 11)
        label.textColor = UIColor.detailsColor
        label.text = "Label"
        return label
    }()
    
    let dateNumberLbl: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: Constants.helveticaCondensed, size: 23)
        label.textColor = UIColor.mainAppColor
        label.text = "10"
        return label
    }()
    
    let monthLbl: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont(name: Constants.helveticaCondensed, size: 12)
        label.textColor = UIColor.mainAppColor
        label.text = "YAN"
        return label
    }()
    
    let statusLine: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.mainAppColor
        return view
    }()
    
    let certIconView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "certNumber"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let priceIconView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "price"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let carIconView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "carNumber"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let statusIconView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "status"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    let operTypeIconView: UIImageView = {
        let imageView = UIImageView(image: #imageLiteral(resourceName: "operation_type"))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupUI()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(contract: Contract) {
        
        insuredNameLbl.text = contract.insuredPerson.fullName
        
        contractNumberLbl.text = contract.contractNumber
        
        priceLbl.text = String(describing: "\(contract.contractPrice.price) AZN")
        
        carNumberLbl.text = contract.vehicle.carNumber
        
        statusLbl.text = contract.status.name
        
        operationTypeLbl.text = contract.operationType
        
        dateNumberLbl.textColor = Constants.statusColors[contract.status.code]
        monthLbl.textColor = Constants.statusColors[contract.status.code]
        statusLine.backgroundColor = Constants.statusColors[contract.status.code]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        if let date = dateFormatter.date(from: contract.creationDate) {
            let calendar = Calendar.current
            dateNumberLbl.text = String(describing: calendar.component(.day, from: date))
            
            switch calendar.component(.month, from: date) {
            case 1:
                monthLbl.text = "YAN"
            case 2:
                monthLbl.text = "FEV"
            case 3:
                monthLbl.text = "MAR"
            case 4:
                monthLbl.text = "APR"
            case 5:
                monthLbl.text = "MAY"
            case 6:
                monthLbl.text = "IYN"
            case 7:
                monthLbl.text = "IYL"
            case 8:
                monthLbl.text = "AVG"
            case 9:
                monthLbl.text = "SEN"
            case 10:
                monthLbl.text = "OKT"
            case 11:
                monthLbl.text = "NOY"
            case 12:
                monthLbl.text = "DEK"
            default:
                monthLbl.text = ""
            }
            
        }
        
    }
    
    func setupUI() {
        
        let shadowView: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.white
            view.layer.cornerRadius = 5
            view.layer.shadowRadius = 5
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOpacity = 0.2
            view.layer.shadowOffset = CGSize.zero
            return view
        }()
        addSubview(shadowView)
        
        let topView: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.white
            view.layer.cornerRadius = 5
            view.layer.masksToBounds = true
            return view
        }()
        addSubview(topView)
        
        let dateBgView: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.white
            return view
        }()
        topView.addSubview(dateBgView)
        
        let separator: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.dimmedGray
            return view
        }()
        dateBgView.addSubview(separator)
        
        topView.addSubview(statusLine)
        
        dateBgView.addSubview(dateNumberLbl)
        dateBgView.addSubview(monthLbl)
        topView.addSubview(insuredNameLbl)
        
        topView.addSubview(contractNumberLbl)
        topView.addSubview(priceLbl)
        topView.addSubview(carNumberLbl)
        topView.addSubview(statusLbl)
        topView.addSubview(operationTypeLbl)
        topView.addSubview(certIconView)
        topView.addSubview(priceIconView)
        topView.addSubview(carIconView)
        topView.addSubview(statusIconView)
        topView.addSubview(operTypeIconView)
        
        
        
        shadowView.topAnchor.constraint(equalTo: topAnchor, constant: 7).isActive = true
        shadowView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -7).isActive = true
        shadowView.leftAnchor.constraint(equalTo: leftAnchor, constant: 16).isActive = true
        shadowView.rightAnchor.constraint(equalTo: rightAnchor, constant: -16).isActive = true
        
        topView.topAnchor.constraint(equalTo: shadowView.topAnchor).isActive = true
        topView.bottomAnchor.constraint(equalTo: shadowView.bottomAnchor).isActive = true
        topView.leftAnchor.constraint(equalTo: shadowView.leftAnchor).isActive = true
        topView.rightAnchor.constraint(equalTo: shadowView.rightAnchor).isActive = true
        
        dateBgView.heightAnchor.constraint(equalTo: topView.heightAnchor).isActive = true
        dateBgView.centerYAnchor.constraint(equalTo: topView.centerYAnchor).isActive = true
        dateBgView.leftAnchor.constraint(equalTo: statusLine.rightAnchor).isActive = true
        dateBgView.widthAnchor.constraint(equalToConstant: 75).isActive = true
        
        separator.widthAnchor.constraint(equalToConstant: 1).isActive = true
        separator.rightAnchor.constraint(equalTo: dateBgView.rightAnchor).isActive = true
        separator.centerYAnchor.constraint(equalTo: topView.centerYAnchor).isActive = true
        separator.heightAnchor.constraint(equalTo: dateBgView.heightAnchor, multiplier: 0.7).isActive = true
        
        statusLine.widthAnchor.constraint(equalToConstant: 5).isActive = true
        statusLine.heightAnchor.constraint(equalTo: topView.heightAnchor).isActive = true
        statusLine.leftAnchor.constraint(equalTo: topView.leftAnchor).isActive = true
        statusLine.centerYAnchor.constraint(equalTo: topView.centerYAnchor).isActive = true
        
        dateNumberLbl.centerXAnchor.constraint(equalTo: dateBgView.centerXAnchor).isActive = true
        dateNumberLbl.centerYAnchor.constraint(equalTo: dateBgView.centerYAnchor, constant: -7).isActive = true
        
        monthLbl.centerXAnchor.constraint(equalTo: dateNumberLbl.centerXAnchor).isActive = true
        monthLbl.topAnchor.constraint(equalTo: dateNumberLbl.bottomAnchor, constant: -2).isActive = true
        
        insuredNameLbl.topAnchor.constraint(equalTo: topView.topAnchor, constant: 12).isActive = true
        insuredNameLbl.leftAnchor.constraint(equalTo: dateBgView.rightAnchor, constant: 15).isActive = true
        
        certIconView.leadingAnchor.constraint(equalTo: insuredNameLbl.leadingAnchor).isActive = true
        certIconView.topAnchor.constraint(equalTo: insuredNameLbl.bottomAnchor, constant: 7).isActive = true
        certIconView.heightAnchor.constraint(equalTo: topView.heightAnchor, multiplier: 0.15).isActive = true
        certIconView.widthAnchor.constraint(equalTo: certIconView.heightAnchor, multiplier: 1).isActive = true
        
        contractNumberLbl.centerYAnchor.constraint(equalTo: certIconView.centerYAnchor).isActive = true
        contractNumberLbl.leftAnchor.constraint(equalTo: certIconView.rightAnchor, constant: 5).isActive = true
        
        priceIconView.centerYAnchor.constraint(equalTo: certIconView.centerYAnchor).isActive = true
        priceIconView.leftAnchor.constraint(equalTo: contractNumberLbl.rightAnchor, constant: 15).isActive = true
        priceIconView.heightAnchor.constraint(equalTo: certIconView.heightAnchor).isActive = true
        priceIconView.widthAnchor.constraint(equalTo: certIconView.widthAnchor).isActive = true
        
        priceLbl.centerYAnchor.constraint(equalTo: certIconView.centerYAnchor).isActive = true
        priceLbl.leftAnchor.constraint(equalTo: priceIconView.rightAnchor, constant: 5).isActive = true
        
        carIconView.centerYAnchor.constraint(equalTo: certIconView.centerYAnchor).isActive = true
        carIconView.leftAnchor.constraint(equalTo: priceLbl.rightAnchor, constant: 15).isActive = true
        carIconView.heightAnchor.constraint(equalTo: certIconView.heightAnchor).isActive = true
        carIconView.widthAnchor.constraint(equalTo: certIconView.widthAnchor).isActive = true
        
        carNumberLbl.centerYAnchor.constraint(equalTo: certIconView.centerYAnchor).isActive = true
        carNumberLbl.leftAnchor.constraint(equalTo: carIconView.rightAnchor, constant: 5).isActive = true
        
        statusIconView.leadingAnchor.constraint(equalTo: insuredNameLbl.leadingAnchor).isActive = true
        statusIconView.topAnchor.constraint(equalTo: certIconView.bottomAnchor, constant: 7).isActive = true
        statusIconView.heightAnchor.constraint(equalTo: topView.heightAnchor, multiplier: 0.18).isActive = true
        statusIconView.widthAnchor.constraint(equalTo: statusIconView.heightAnchor, multiplier: 1).isActive = true
        
        statusLbl.centerYAnchor.constraint(equalTo: statusIconView.centerYAnchor).isActive = true
        statusLbl.leftAnchor.constraint(equalTo: statusIconView.rightAnchor, constant: 5).isActive = true
        
        operTypeIconView.centerYAnchor.constraint(equalTo: statusIconView.centerYAnchor).isActive = true
        operTypeIconView.leftAnchor.constraint(equalTo: statusLbl.rightAnchor, constant: 20).isActive = true
        operTypeIconView.heightAnchor.constraint(equalTo: statusIconView.heightAnchor).isActive = true
        operTypeIconView.widthAnchor.constraint(equalTo: statusIconView.widthAnchor).isActive = true
        
        operationTypeLbl.centerYAnchor.constraint(equalTo: statusIconView.centerYAnchor).isActive = true
        operationTypeLbl.leftAnchor.constraint(equalTo: operTypeIconView.rightAnchor, constant: 5).isActive = true
        
    }

}
