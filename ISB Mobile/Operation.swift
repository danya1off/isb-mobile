//
//  Operation.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/21/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation

class Operation: NSObject, NSCoding {
    
    var code: String = ""
    var name: String = ""
    var participants = [Participant]()
    
    init(code: String, name: String, participants: [Participant]) {
        self.code = code
        self.name = name
        self.participants = participants
    }
    
    init(code: String, name: String) {
        self.code = code
        self.name = name
        self.participants = [Participant]()
    }
    
    override init() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        if let data = aDecoder.decodeObject(forKey: Keys.code) as? String {
            code = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.name) as? String {
            name = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.participants) as? [Participant] {
            participants = data
        }
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(code, forKey: Keys.code)
        aCoder.encode(name, forKey: Keys.name)
        aCoder.encode(participants, forKey: Keys.participants)
    }
    
    struct Keys {
        static let code = "code"
        static let name = "name"
        static let participants = "participants"
    }
    
}
