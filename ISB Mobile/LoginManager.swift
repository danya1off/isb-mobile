//
//  LoginManager.swift
//  ISB Mobile
//
//  Created by  Simberg on 11/7/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD

class LoginManager {
    
    private var isPhoneRegistered = false
    private var vc: LoginVC!
    
    init(view: LoginVC) {
        self.vc = view
    }
    
    // MARK: - User authorization
    func authorization() {
        
        isPhoneRegistered = userDefaults.bool(forKey: Constants.isPhoneRegistered)
        
        vc.loginBtn.isEnabled = false
        vc.loginBtn.backgroundColor = UIColor(red:0.83, green:0.83, blue:0.83, alpha:1.0)
        vc.phoneNumberTxtField.text = "+994702041574"
        vc.userIDTxtField.text = "test13"
        
        SVProgressHUD.show(withStatus: "Gözləyin...")
        if !isPhoneRegistered { // if phone is not registered yet
            Services.startRegistration { (result) in
                SVProgressHUD.dismiss()
                
                guard result.isSuccess else {
                    self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                    return
                }
                
                if let data = result.data {
                    print("OperationID: \(data)")
                    userDefaults.set(data, forKey: Constants.operationID)
                }
                
                self.vc.loginBtn.isEnabled = true
                self.vc.loginBtn.backgroundColor = UIColor(red:0.00, green:0.75, blue:0.81, alpha:1.0)
            }
        } else { // if already registered
            Services.startAuthentication(completion: { result in
                SVProgressHUD.dismiss()
                if result.isSuccess {
                    userDefaults.set(result.data!, forKey: Constants.operationID)
                    self.vc.loginBtn.isEnabled = true
                    self.vc.loginBtn.backgroundColor = UIColor(red:0.00, green:0.75, blue:0.81, alpha:1.0)
                } else {
                    self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                }
            })
        }
    }
    
    func login() {
        
        SVProgressHUD.show(withStatus: "Gözləyin...")
        userDefaults.set(vc.phoneNumberTxtField.text!, forKey: Constants.phoneNumber)
        userDefaults.set(vc.userIDTxtField.text!, forKey: Constants.asanID)
        
        // Authentificates users inputs
        Services.authParams(byPhoneNumber: vc.phoneNumberTxtField.text!, andID: vc.userIDTxtField.text!) { (result) in
            SVProgressHUD.dismiss()
            
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            
            guard let transaction = result.data else {
                self.vc.fireErrorAlert(withMessage: Constants.dataError)
                return
            }
            
            // Show verification code in alert view (dismiss automatically)
            let verificationAlert = self.vc.alert(withTitle: "Verifikasiya", message: "Sizin verifikasiya kodunuz: \(transaction.verificationCode!)", showButton: false)
            self.vc.present(verificationAlert, animated: true, completion: nil)
            
            // Results authentification
            Services.authResult(byTransactionID: transaction.transactionID!, completion: { (result) in
                
                guard result.isSuccess else {
                    self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                    return
                }
                
                self.isPhoneRegistered = userDefaults.bool(forKey: Constants.isPhoneRegistered)
                let operationName = userDefaults.string(forKey: Constants.operationName)!
                
                // If phone is not registered yet or operation name is empty, then we must register phone
                if !self.isPhoneRegistered || operationName == "" {
                    guard let data = result.data else {
                        self.vc.fireErrorAlert(withMessage: Constants.dataError)
                        return
                    }
                    let employeeInfoVC = EmployeeInfoVC()
                    employeeInfoVC.modalTransitionStyle = .crossDissolve
                    employeeInfoVC.modalPresentationStyle = .overCurrentContext
                    employeeInfoVC.employeeInfoArray = data.employeeInfoArray
                    verificationAlert.dismiss(animated: true, completion: {
                        self.vc.present(employeeInfoVC, animated: true, completion: nil)
                    })
                    
                } else { // if phone is registered already
                    
                    if operationName != "" { // if there is saved state for some operation, then we just recover that view
                        
                        ApplicationManager.restoreView()
                        
                    } else { // else we render the main menu
                        
                        // User authorization and getting the operations list
                        Services.authorizations(byPhoneNumber: self.vc.phoneNumberTxtField.text!, andID: self.vc.userIDTxtField.text!, completion: { result in
                            
                            guard result.isSuccess else {
                                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                                return
                            }
                            
                            guard let participantAuth = result.data else {
                                self.vc.fireErrorAlert(withMessage: Constants.dataError)
                                return
                            }
                            
                            if participantAuth.operations.count > 0 {
                                ApplicationManager.saveParticipantInfo(pinCode: participantAuth.pinCode, operations: participantAuth.operations)
                                
                                self.vc.dismiss(animated: true, completion: {
                                    ApplicationManager.redirectToMainMenu()
                                })
                            }
                        })
                        
                    }
                }
            })
        }
        
    }
    
}


