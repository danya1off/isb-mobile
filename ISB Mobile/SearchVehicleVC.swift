//
//  SearchVehicleVC.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/20/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD

class SearchVehicleVC: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    
    var navigationTitle = ""
    var navigationSubTitle = ""

    var scrollView: UIScrollView!
    let formView = UIView()
    let miaForm = UIView()
    let miscForm = UIView()
    let toggleMiaLabel = UILabel()
    let toggleMiscLabel = UILabel()
    let toggleBtn = UISwitch()
    var certificateNumberTxtField: CustomTextField!
    var carNumberTxtField: CustomTextField!
    var bodyNumberTxtField: CustomTextField!
    var engineNumberTxtField: CustomTextField!
    var yearOfManufactureTxtField: CustomTextField!
    var vehicleColorTxtField: CustomTextField!
    var makeTxtField: CustomTextField!
    var modelTxtField: CustomTextField!
    var vehicleTypeSelector = UIPickerView()
    var engineCapacityTxtField: CustomTextField!
    var horsePowerTxtField: CustomTextField!
    var chassisTxtField: CustomTextField!
    var generalTxtField: CustomTextField!
    var generalLbl: UILabel!
    
    var searchBtn: UIButton!
    
    var certificateNumberValue: String?
    var carNumberValue: String?
    var bodyNumberValue: String?
    var engineNumberValue: String?
    var yearOfManufactureValue: String?
    var vehicleColorValue: String?
    var makeValue: String?
    var modelValue: String?
    var vehicleTypeValue: String?
    var engineCapacityValue: String?
    var horsePowerValue: String?
    
    var vehicleTypes = VehicleType.getVehicleTypes()
    var operationCode: String?
    var selectedVehicleType = 0
    
    var vehicleManager: VehicleManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vehicleManager = VehicleManager(view: self)
        
        if let code = userDefaults.string(forKey: Constants.operationCode) {
            operationCode = code
        }
        
        //create main UI
        createUI()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userDefaults.set(String(describing: self.classForCoder) , forKey: Constants.operationName)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return vehicleTypes.count + 1
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = (view as? UILabel) ?? UILabel()
        label.font = UIFont(name: Constants.helveticaCondensed, size: 15)
        label.textColor = .mainAppColor
        label.textAlignment = .center
        label.text = row == 0 ? "Nəqliyyat vasitənin tipi seçin" : vehicleTypes[row - 1].name
        return label
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedVehicleType = row
        switch row {
        case 1:
            if generalTxtField.isHidden {
                generalTxtField.isHidden = false
            }
            generalLbl.text = Constants.passengerSeatCountLbl
            generalTxtField.placeholder = "25"
            generalTxtField.keyboardType = .numberPad
            break
        case 2:
            if generalTxtField.isHidden {
                generalTxtField.isHidden = false
            }
            generalLbl.text = Constants.maxPermissibleMassLbl
            generalTxtField.placeholder = "10000"
            break
        case 3:
            if generalTxtField.isHidden {
                generalTxtField.isHidden = false
            }
            generalLbl.text = Constants.engineCapacityLbl
            generalTxtField.placeholder = "2500"
            generalTxtField.keyboardType = .numberPad
            break
        case 7:
            if generalTxtField.isHidden {
                generalTxtField.isHidden = false
            }
            generalLbl.text = Constants.horsePowerLbl
            generalTxtField.placeholder = "250"
            generalTxtField.keyboardType = .numberPad
            break
        default:
            generalLbl.text = "..."
            generalTxtField.placeholder = "..."
            generalTxtField.isHidden = true
        }
    }
    
    @objc func searchVehicle() {
        switch checkInputs() {
        case .valid:
            vehicleManager.getVehicleInfo(byCertificationNumber: certificateNumberTxtField.text!, andCarNumber: carNumberTxtField.text!)
        case .failure(let error):
            fireErrorAlert(withMessage: error)
        }
    }
    
    @objc func saveVehicle() {
        switch checkNonMiaVehicleData() {
        case .valid:
            let vehicleInfo = VehicleInfo()
            vehicleInfo.bodyNumber = bodyNumberTxtField.text!
            vehicleInfo.engineNumber = engineNumberTxtField.text!
            vehicleInfo.carNumber = carNumberTxtField.text!
            vehicleInfo.certificateNumber = certificateNumberTxtField.text!
            if let yearOfManufacturer = Int(yearOfManufactureTxtField.text!) {
                vehicleInfo.yearOfManufacture = yearOfManufacturer
            }
            vehicleInfo.vehicleColor = vehicleColorTxtField.text!
            vehicleInfo.make = makeTxtField.text!
            vehicleInfo.model = modelTxtField.text!
            vehicleInfo.vehicleType = vehicleTypes[vehicleTypeSelector.selectedRow(inComponent: 0)].code
            vehicleInfo.vehicleTypeID = vehicleTypes[vehicleTypeSelector.selectedRow(inComponent: 0)].codeNumber + 1
            
            if operationCode == Constants.BORDER_CMTPL_CONTRACT_OPERATION {
                switch selectedVehicleType {
                case 0:
                    fireErrorAlert(withMessage: "Nəqliyyat vasitənin tipi seçilməyib!")
                    break
                    
                case 1:
                    vehicleInfo.passengerSeatCount = Int(generalTxtField.text!)!
                    break
                    
                case 2:
                    vehicleInfo.maxPermissibleMass = Double(generalTxtField.text!)!
                    break
                    
                case 3:
                    vehicleInfo.engineCapacity = Int(generalTxtField.text!)!
                    break
                    
                case 7:
                    vehicleInfo.horsePower = Int(generalTxtField.text!)!
                    break
                    
                default:
                    break
                    
                }
            }
            vehicleManager.saveNonMiaVehicleInfo(vehicleInfo: vehicleInfo)
        case .failure(let error):
            fireErrorAlert(withMessage: error)
        }
    }
    
    @objc func cancelOperation() {
        let contractManager = ContractManager(view: self)
        contractManager.cancelContractOperation()
    }
    
    @objc func toggleValueChanged() {
        userDefaults.set(toggleBtn.isOn, forKey: Constants.miaNonMiaType)
        if toggleBtn.isOn {
            toggleMiaLabel.textColor = UIColor.labelColor
            toggleMiscLabel.textColor = UIColor.mainTextColor
            removeFromView(miaForm)
            createMiscView()
        } else {
            toggleMiaLabel.textColor = UIColor.mainTextColor
            toggleMiscLabel.textColor = UIColor.labelColor
            removeFromView(miscForm)
            createMiaView()
        }
    }
    
    func checkNonMiaVehicleData() -> Validation {
        if bodyNumberTxtField.text == "" && engineNumberTxtField.text == "" && chassisTxtField.text == "" {
            return .failure("Zəhmət olmasa, NV-nin ban nömrəsi, mühərrikin nömrəsi " +
                "və ya şassi nömrələrindən hər hansı birini daxil edəsiniz!")
        }

        if carNumberTxtField.text != "" {
            let number = carNumberTxtField.text!
            if number.count < 7 || number.count > 7 {
                return .failure("Maşın nömrəsi 7 simvoldan ibarət olmalıdır")
            }
            let carFirstNumbers = number.substring(to: 2)
            let carNumberLetters = number.substring(with: 2..<4)
            let carLastNumbers = number.substring(from: 4)
            if !carFirstNumbers.numbersOnly || !carNumberLetters.latinCharactersOnly || !carLastNumbers.numbersOnly {
                return .failure("Maşın nömrəsinin formatı düzgün dəyil!")
            }
        } else {
            return .failure("Maşın nömrəsi boş ola bilməz!")
        }
        
        if operationCode == Constants.BORDER_CMTPL_CONTRACT_OPERATION {
            if vehicleTypeSelector.selectedRow(inComponent: 0) == 0 {
                return .failure("Nəqliyyat tipi seçilməyib!")
            }
            if !generalTxtField.isHidden && generalTxtField.text == "" {
                switch selectedVehicleType {
                case 0:
                    return .failure("Nəqliyyat vasitənin tipi seçilməyib!")
                    
                case 1:
                    return .failure("Oturacağların sayı boş ola bilməz!")
                    
                case 2:
                    return .failure("Maksimal kütlə boş ola bilməz!")
                    
                case 3:
                    return .failure("Mühərrikin həcmi boş ola bilməz!")
                    
                case 7:
                    return .failure("At gücü boş ola bilməz!")
                    
                default:
                    return .valid
                    
                }
            }
        }
        return .valid
    }
    
    func checkInputs() -> Validation {
        if certificateNumberTxtField.text == "" {
            return .failure("NV-nin qeydiyyat şəhadətnaməsinin nömrəsi boş ola bilməz!")
        }
        if carNumberTxtField.text == "" {
            return .failure("Maşın nömrəsi boş ola bilməz!")
        } else {
            let number = carNumberTxtField.text!
            
            if number.count < 7 || number.count > 7 {
                return .failure("Maşın nömrəsi 7 simvoldan ibarət olmalıdır")
            }

            let carFirstNumbers = number.substring(to: 2)
            let carNumberLetters = number.substring(with: 2..<4)
            let carLastNumbers = number.substring(from: 4)
            
            if !carFirstNumbers.numbersOnly {
                return .failure("Maşın nömrəsinin ilk 2 simvolu rəqəm olmalıdır!")
            }
            
            if !carNumberLetters.latinCharactersOnly {
                return .failure("Maşın nömrəsinin 3-cü və 4-cü simvollar hərf olmalıdır!")
            }
            
            if !carLastNumbers.numbersOnly {
                return .failure("Maşın nömrəsinin son 3 simvol rəqəm olmalıdır!")
            }
        }
        return .valid
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == carNumberTxtField.tag {
            return ApplicationManager.setMaxLength(forTextField: textField, withRange: range, withString: string, maxLength: 7)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
