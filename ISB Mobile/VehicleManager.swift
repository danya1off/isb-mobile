//
//  VehicleManager.swift
//  ISB Mobile
//
//  Created by  Simberg on 11/8/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD

class VehicleManager {
    
    private var vc: UIViewController!
    
    init(view: UIViewController) {
        self.vc = view
    }
    
    func getVehicleInfo(byCertificationNumber certNumber: String, andCarNumber carNumber: String) {
        SVProgressHUD.show(withStatus: "Gözləyin...")
        Services.getVehicleInfo(byCertificationNumber: certNumber, andCarNumber: carNumber) { (result) in
            SVProgressHUD.dismiss()
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            guard let data = result.data else {
                self.vc.fireErrorAlert(withMessage: Constants.dataError)
                return
            }
            ApplicationManager.saveVehicleInfo(vehicleInfo: data)
            (self.vc as! SearchVehicleVC).clearVehicleViewFieldValues()
            let vehicleDetailsVC = VehicleDetailsVC()
            vehicleDetailsVC.vehicleInfoArray = data.vehicleInfoArray
            self.vc.navigationController?.pushViewController(vehicleDetailsVC, animated: true)
        }
    }
    
    func saveNonMiaVehicleInfo(vehicleInfo: VehicleInfo) {
        let viewController = vc as! SearchVehicleVC
        
        SVProgressHUD.show(withStatus: "Gözləyin...")
        Services.saveNonMiaVehicleInfo(vehicleInfo: vehicleInfo, completion: { (result) in
            
            SVProgressHUD.dismiss()
            
            guard result.isSuccess else {
                viewController.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            
            ApplicationManager.saveVehicleInfo(vehicleInfo: vehicleInfo)
            viewController.clearVehicleViewFieldValues()
            let searchUserVC = SearchUserVC()
            searchUserVC.navigationTitle = viewController.navigationTitle
            searchUserVC.navigationSubTitle = viewController.navigationSubTitle
            viewController.navigationController?.pushViewController(searchUserVC, animated: true)
        })
    }
    
    func send(vehicleHorsePower power: Int) {
        let viewController = vc as! VehicleDetailsVC
        SVProgressHUD.show(withStatus: "Gözləyin...")
        Services.vehicleHorsePower(power: power, completion: { (result) in
            SVProgressHUD.dismiss()
            
            guard result.isSuccess else {
                viewController.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            let destination = SearchUserVC()
            destination.navigationTitle = viewController.navigationTitle
            destination.navigationSubTitle = viewController.navigationSubTitle
            viewController.navigationController?.pushViewController(destination, animated: true)
            
        })
    }
    
}
