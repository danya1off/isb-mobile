//
//  FinalDetailsVC+Ext.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/21/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

extension FinalDetailsVC {
    
    func createUI() {
        
        self.navigationItem.title = "TƏSDİQLƏMƏ"
        navigationItem.hidesBackButton = true
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "close"), style: .plain, target: self, action: #selector(FinalDetailsVC.cancelOperation))
        
        CustomProgressHUD.customize()
        
        view.backgroundColor = UIColor.white
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(FinalDetailsCell.self, forCellReuseIdentifier: Constants.finalDetailCell)
        tableView.tableFooterView = UIView()
        tableView.separatorInset.left = 20
        tableView.separatorInset.right = 20
        
        createMainView()
        
    }
    
    private func createMainView() {
        
        confirmBtn = UIButton.createCustomButton(text: "MÜQAVİLƏNİN HAZIRLANMASI", textSize: 20)
        confirmBtn.addTarget(self, action: #selector(FinalDetailsVC.createContract), for: .touchUpInside)
        confirmBtn.layer.cornerRadius = 0
        view.addSubview(confirmBtn)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        
        confirmBtn.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.07).isActive = true
        confirmBtn.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        confirmBtn.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        confirmBtn.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        tableView.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: confirmBtn.topAnchor).isActive = true
        tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        tableView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        
    }
    
}
