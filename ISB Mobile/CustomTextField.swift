//
//  CustomTextField.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/19/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.height - 1, width: self.frame.width, height: 1.0)
        bottomLine.backgroundColor = UIColor(red:0.91, green:0.91, blue:0.91, alpha:1.0).cgColor
        self.borderStyle = .none
        self.layer.addSublayer(bottomLine)
        
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 0, dy: 7)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }
    
}
