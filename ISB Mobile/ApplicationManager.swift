//
//  ApplicationLogic.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/13/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import UIKit
import SCLAlertView
import SVProgressHUD

class ApplicationManager {
    
    private init() {}
    
    static let appDelegate = UIApplication.shared.delegate as! AppDelegate
    static let window = appDelegate.window
    static let storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    // MARK: - Restore View
    static func restoreView() {
        
        let isRegistered = userDefaults.bool(forKey: Constants.isPhoneRegistered)
        if isRegistered {
            let operationName = userDefaults.string(forKey: Constants.operationName)!
            if operationName != "" {
                
                if operationName == String(describing: type(of: MenuVC())) {
                    redirectToMainMenu()
                } else {
                    
                    guard let operationFullName = userDefaults.string(forKey: Constants.operationFullName) else {
                        redirectToMainMenu()
                        return
                    }
                    
                    let dummyViewController = UIViewController()
                    dummyViewController.view.backgroundColor = UIColor.white
                    window?.rootViewController = dummyViewController
                    
                    let appearance = SCLAlertView.SCLAppearance(
                        kTitleFont: UIFont(name: "HelveticaNeue", size: 18)!,
                        kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
                        kButtonFont: UIFont(name: "HelveticaNeue-Medium", size: 14)!,
                        showCloseButton: false
                    )
                    
                    let alert = SCLAlertView(appearance: appearance)
                    alert.addButton("Davam etmək") {
                        self.restore(withOperationName: operationName, andWindow: window)
                    }
                    alert.addButton("Menüya qayıt", action: {
                        let contractManager = ContractManager(view: dummyViewController)
                        contractManager.cancelContractOperation()
                    })
                    _ = alert.showCustom("Məlumat", subTitle: "Sizin natamam \"\(operationFullName)\" əməliyyatınız qalıb. Davam etmək istəyirsinizmi?",
                        color: UIColor.mainAppColor, icon: #imageLiteral(resourceName: "warning"))
                    
                }
                
            } else {
                redirectToLogin()
            }
        } else {
            redirectToLogin()
        }
        
    }
    
    fileprivate class func restore(withOperationName name: String, andWindow window: UIWindow?) {
        
        switch name {
        case String(describing: type(of: SearchVehicleVC())):
            let viewController = SearchVehicleVC()
            window?.rootViewController = UINavigationController(rootViewController: viewController)
            
        case String(describing: type(of: VehicleDetailsVC())):
            let viewController = VehicleDetailsVC()
            if let vehicle = getVehicleInfo() {
                viewController.vehicleInfoArray = vehicle.vehicleInfoArray
                window?.rootViewController = UINavigationController(rootViewController: viewController)
            } else {
                redirectToMainMenu()
            }
            
        case String(describing: type(of: SearchUserVC())):
            let viewController = SearchUserVC()
            window?.rootViewController = UINavigationController(rootViewController: viewController)
            
        case String(describing: type(of: UserDetailsVC())):
            let viewController = UserDetailsVC()
            if let naturalPerson = getNaturalPerson() {
                viewController.userInfoArray = naturalPerson.naturalPersonArray
                window?.rootViewController = UINavigationController(rootViewController: viewController)
            } else if let juridicalPerson = getJuridicalPerson() {
                viewController.userInfoArray = juridicalPerson.juridicalPersonArray
                window?.rootViewController = UINavigationController(rootViewController: viewController)
            } else {
                redirectToMainMenu()
            }
            
        case String(describing: type(of: ContractsTVC())):
            let viewController = ContractsTVC()
            viewController.fromSessionRestore = true
            window?.rootViewController = UINavigationController(rootViewController: viewController)
            
        case String(describing: type(of: ContractDetailsVC())):
            let viewController = ContractDetailsVC()
            if let contract = getContract() {
                viewController.fromCrashState = true
                viewController.contract = contract
                viewController.contractDetailsArray = contract.contractDetailsArray
                window?.rootViewController = UINavigationController(rootViewController: viewController)
            } else {
                redirectToMainMenu()
            }
            
        case String(describing: type(of: FinalDetailsVC())):
            let viewController = FinalDetailsVC()
            if let contractPrice = ApplicationManager.getContractPrice() {
                viewController.finalDetailsArray = contractPrice.contractPriseArray
                window?.rootViewController = UINavigationController(rootViewController: viewController)
            } else {
                redirectToMainMenu()
            }
            break
            
        case String(describing: type(of: TerminationFormVC())):
            let viewController = TerminationFormVC()
            if let contractNumber = userDefaults.string(forKey: Constants.contractNumber) {
                viewController.contractNumber = contractNumber
            }
            window?.rootViewController = UINavigationController(rootViewController: viewController)
            
        case String(describing: type(of: TerminationCommitVC())):
            let viewController = TerminationCommitVC()
            window?.rootViewController = UINavigationController(rootViewController: viewController)
            
        case String(describing: type(of: PeriodVC())):
            let viewController = PeriodVC()
            window?.rootViewController = UINavigationController(rootViewController: viewController)
            
        case String(describing: type(of: GreenCardNewBlankVC())):
            let viewController = GreenCardNewBlankVC()
            window?.rootViewController = UINavigationController(rootViewController: viewController)
            
        default:
            redirectToMainMenu()
        }
        
    }
    
    // MARK: - Redirect to Main Menu
    static func redirectToMainMenu() {
        if let participant = getParticipantInfo() {
            let vc = MenuVC()
            vc.participant = participant
            userDefaults.set(String(describing: type(of: MenuVC())) , forKey: Constants.operationName)
            
            UIView.transition(with: window!, duration: 0.5, options: .transitionCrossDissolve, animations: {
                window?.rootViewController = vc
            }, completion: nil)
        } else {
            redirectToLogin()
        }
    }
    
    // MARK: - Redirect to Login Screen
    static func redirectToLogin() {
        
        let vc = LoginVC()
        
        UIView.transition(with: window!, duration: 0.5, options: .transitionCrossDissolve, animations: {
            window?.rootViewController = vc
        }, completion: nil)
    }
    
    // MARK: - Save Participant Info
    static func saveParticipantInfo(participantInfo: ParticipantInfo) {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: participantInfo)
        userDefaults.set(encodedData, forKey: Constants.participantInfoPath)
    }
    
    static func saveParticipantInfo(pinCode: String, operations: [Operation], operation: Operation, participant: Participant, insurer: Insurer, isParticipantSaved: Bool) {
        let participantInfo = ParticipantInfo()
        participantInfo.pinCode = pinCode
        participantInfo.participantName = participant.name
        participantInfo.participantTin = participant.tin
        participantInfo.insurerName = insurer.name
        participantInfo.insurerTin = insurer.tin
        participantInfo.hasAsanImza = participant.hasAsanImza
        participantInfo.operation = operation
        participantInfo.operations = operations
        participantInfo.isParticipantSaved = isParticipantSaved
        saveParticipantInfo(participantInfo: participantInfo)
    }
    
    static func saveParticipantInfo(pinCode: String, operations: [Operation]) {
        let participantInfo = ParticipantInfo()
        participantInfo.pinCode = pinCode
        participantInfo.operations = operations
        saveParticipantInfo(participantInfo: participantInfo)
    }
    
    static func saveParticipantInfo(withInfo info: ParticipantInfo) {
        saveParticipantInfo(participantInfo: info)
    }
    
    // MARK: - Save Participants
    static func saveParticipants(participants: [Participant]) {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: participants)
        userDefaults.set(encodedData, forKey: Constants.participantsPath)
    }
    
    // MARK: - Save Natural Person
    static func saveNaturalPerson(naturalPerson: NaturalPerson) {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: naturalPerson)
        userDefaults.set(encodedData, forKey: Constants.naturalPersonPath)
    }
    
    // MARK: - Save Juridical Person
    static func saveJuridicalPerson(juridicalPerson: JuridicalPerson) {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: juridicalPerson)
        userDefaults.set(encodedData, forKey: Constants.juridicalPersonPath)
    }
    
    // MARK: - Save VehicleInfo
    static func saveVehicleInfo(vehicleInfo: VehicleInfo) {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: vehicleInfo)
        userDefaults.set(encodedData, forKey: Constants.carInfoPath)
    }
    
    // MARK: - Save ConstractInfo
    static func saveContractInfo(contractInfo: Contract) {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: contractInfo)
        userDefaults.set(encodedData, forKey: Constants.contractPath)
    }
    
    // MARK: - Save Contract PriceInfo
    static func saveContractPriceInfo(contractPriceInfo: ContractPrice) {
        let encodedData = NSKeyedArchiver.archivedData(withRootObject: contractPriceInfo)
        userDefaults.set(encodedData, forKey: Constants.contractPricePath)
    }
    
    // MARK: - Get ParticipantInfo
    static func getParticipantInfo() -> ParticipantInfo? {
        if let decoded = userDefaults.object(forKey: Constants.participantInfoPath) as? NSData {
            let participantInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded as Data) as? ParticipantInfo
            return participantInfo
        }
        return nil
    }
    
    // MARK: - Get Participants
    static func getParticipants() -> [Participant]? {
        if let decoded = userDefaults.object(forKey: Constants.participantsPath) as? NSData {
            let participants = NSKeyedUnarchiver.unarchiveObject(with: decoded as Data) as? [Participant]
            return participants
        }
        return nil
    }
    
    // MARK: - Get VehicleInfo
    static func getVehicleInfo() -> VehicleInfo? {
        if let decoded = userDefaults.object(forKey: Constants.carInfoPath) as? NSData {
            let vehicleInfo = NSKeyedUnarchiver.unarchiveObject(with: decoded as Data) as? VehicleInfo
            return vehicleInfo
        }
        return nil
    }
    
    // MARK: - Get Natural Person
    static func getNaturalPerson() -> NaturalPerson? {
        if let decoded = userDefaults.object(forKey: Constants.naturalPersonPath) as? NSData {
            let naturalPerson = NSKeyedUnarchiver.unarchiveObject(with: decoded as Data) as? NaturalPerson
            return naturalPerson
        }
        return nil
    }
    
    // MARK: - Get Juridical Person
    static func getJuridicalPerson() -> JuridicalPerson? {
        if let decoded = userDefaults.object(forKey: Constants.juridicalPersonPath) as? NSData {
            let juridicalPerson = NSKeyedUnarchiver.unarchiveObject(with: decoded as Data) as? JuridicalPerson
            return juridicalPerson
        }
        return nil
    }
    
    // MARK: - Get Contract
    static func getContract() -> Contract? {
        if let decoded = userDefaults.object(forKey: Constants.contractPath) as? NSData {
            let contract = NSKeyedUnarchiver.unarchiveObject(with: decoded as Data) as? Contract
            return contract
        }
        return nil
    }
    
    // MARK: - Get Contract Price
    static func getContractPrice() -> ContractPrice? {
        if let decoded = userDefaults.object(forKey: Constants.contractPricePath) as? NSData {
            let contractPrice = NSKeyedUnarchiver.unarchiveObject(with: decoded as Data) as? ContractPrice
            return contractPrice
        }
        return nil
    }
    
    // MARK: - Clear data
    static func clearData() {
        
        userDefaults.removeObject(forKey: Constants.operationID)
        userDefaults.set(false, forKey: Constants.isLoggedInOnceADay)
        userDefaults.set(nil, forKey: Constants.participantInfoPath)
        
        /*let manager = FileManager.default
         let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first
         do {
         try manager.removeItem(atPath: url!.appendingPathComponent(Constants.participantPath).path)
         } catch {
         print("Error while deleting participant from data store!")
         }*/
        
    }
    
    static func clearTempData() {
        userDefaults.set("", forKey: Constants.operationName)
        userDefaults.set(nil, forKey: Constants.carInfoPath)
        userDefaults.set(nil, forKey: Constants.naturalPersonPath)
        userDefaults.set(nil, forKey: Constants.juridicalPersonPath)
        userDefaults.set(nil, forKey: Constants.contractPath)
    }
    
    // MARK: - Set max length for text field
    static func setMaxLength(forTextField textField: UITextField, withRange range: NSRange, withString string: String, maxLength lenght: Int) -> Bool {
        
        let maxLength = lenght
        let currentString: NSString = textField.text! as NSString
        let newString: NSString = currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
        
    }
    
}
