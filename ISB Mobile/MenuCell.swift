//
//  MenuCell.swift
//  ISB Mobile
//
//  Created by Jeyhun Danyalov on 9/19/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

class MenuCell: UITableViewCell {
    
    let menuLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.mainTextColor
        label.font = UIFont(name: Constants.helveticaMedium, size: 13)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 8
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        return label
    }()
    
    let icon: UIImageView = {
        let i = UIImageView()
        i.translatesAutoresizingMaskIntoConstraints = false
        i.image = UIImage(named: "house")
        i.contentMode = .scaleAspectFit
        return i
    }()
    
    var menuCode: String?
    
    func config(menuName: String, menuCode: String) {
        menuLabel.text = menuName
        self.menuCode = menuCode
        
        switch menuCode {
        case Constants.STANDARD_CMTPL_CONTRACT_OPERATION,
             Constants.BORDER_CMTPL_CONTRACT_OPERATION,
             Constants.GREENCARD_CMTPL_CONTRACT_OPERATION:
            icon.image = #imageLiteral(resourceName: "add_rect")
        case Constants.STANDARD_CMTPL_TERMINATE_CONTRACT_OPERATION,
             Constants.BORDER_CMTPL_TERMINATE_CONTRACT_OPERATION,
             Constants.GREENCARD_CMTPL_TERMINATE_CONTRACT_OPERATION:
            icon.image = #imageLiteral(resourceName: "delete_rect")
        case Constants.STANDARD_CMTPL_VIEW_PARTICIPANT_CONTRACTS,
             Constants.BORDER_CMTPL_VIEW_PARTICIPANT_CONTRACTS,
             Constants.GREENCARD_CMTPL_VIEW_PARTICIPANT_CONTRACTS:
            icon.image = #imageLiteral(resourceName: "view_rect")
        default:
            icon.image = #imageLiteral(resourceName: "house")
        }
        
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupUI()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func setupUI() {
        
        addSubview(menuLabel)
        addSubview(icon)
        
        menuLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        menuLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 72).isActive = true
        menuLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -16).isActive = true
        
        icon.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        icon.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.3).isActive = true
        icon.widthAnchor.constraint(equalTo: icon.heightAnchor, multiplier: 1).isActive = true
        icon.leftAnchor.constraint(equalTo: leftAnchor, constant: 25).isActive = true

        
    }
    
}
