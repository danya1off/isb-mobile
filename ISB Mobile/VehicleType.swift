//
//  VehicleType.swift
//  ISB Mobile
//
//  Created by  Simberg on 10/4/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import Foundation

struct VehicleType {
    
    var name: String!
    var code: String!
    var codeNumber: Int!
    
    init(name: String, code: String, codeNumber: Int) {
        self.name = name
        self.code = code
        self.codeNumber = codeNumber
    }
    
    static func getVehicleTypes() -> [VehicleType] {
        
        return [VehicleType(name: "Avtobus", code: "BUS_VEH_TYPE", codeNumber: 1),
        VehicleType(name: "Yük", code: "TRUCK_VEH_TYPE", codeNumber: 2),
        VehicleType(name: "Minik", code: "PASSENGER_VEH_TYPE", codeNumber: 3),
        VehicleType(name: "Motosikl", code: "MOTORCYCLE_VEH_TYPE", codeNumber: 4),
        VehicleType(name: "Qoşqu", code: "TRAILER_VEH_TYPE", codeNumber: 5),
        VehicleType(name: "Xüsusi təyinatlı", code: "SPECIAL_VEH_TYPE", codeNumber: 6),
        VehicleType(name: "Elektromobil", code: "ELECTRO_VEH_TYPE", codeNumber: 7)]
        
    }
    
}
