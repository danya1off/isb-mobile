//
//  EmployeeManager.swift
//  ISB Mobile
//
//  Created by  Simberg on 11/7/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD

class EmployeeManager {
    
    private var vc: UIViewController!
    
    init(view: UIViewController) {
        self.vc = view
    }
    
    func confirmPersonInfo() {
        
        SVProgressHUD.show(withStatus: "Gözləyin...")
        let appCode = UUID().uuidString
        userDefaults.set(appCode, forKey: Constants.appCode)
        
        Services.registerApp { result in
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                SVProgressHUD.dismiss()
                return
            }
            Services.startAuthentication(completion: { result in
                guard result.isSuccess, result.data != nil else {
                    self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                    SVProgressHUD.dismiss()
                    return
                }
                userDefaults.set(result.data!, forKey: Constants.operationID)
                let phoneNumber = userDefaults.string(forKey: Constants.phoneNumber)
                let asanID = userDefaults.string(forKey: Constants.asanID)
                guard let phone = phoneNumber, let asan = asanID else {
                    self.vc.fireErrorAlert(withMessage: "Telefon nömrəsi və asan İD boş ola bilməz!")
                    SVProgressHUD.dismiss()
                    return
                }
                Services.authorizations(byPhoneNumber: phone, andID: asan, completion: { result in
                    SVProgressHUD.dismiss()
                    guard result.isSuccess else {
                        self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                        return
                    }
                    guard let participantAuth = result.data else {
                        self.vc.fireErrorAlert(withMessage: Constants.dataError)
                        return
                    }
                    if participantAuth.operations.count > 0 {
                        ApplicationManager.saveParticipantInfo(pinCode: participantAuth.pinCode, operations: participantAuth.operations)
                        
                        self.vc.dismiss(animated: true, completion: {
                            ApplicationManager.redirectToMainMenu()
                        })
                    }
                    userDefaults.set(true, forKey: Constants.isPhoneRegistered)
                })
            })
            
        }
        
    }
    
    func mismatchPersonInfo() {
        Services.mismatchPersonInfo { result in
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            self.vc.dismiss(animated: true, completion: nil)
        }
    }
    
}
