//
//  ParticipantInfo.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/23/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation

class ParticipantInfo: NSObject, NSCoding {
    
    private var _pinCode = ""
    private var _participantName = ""
    private var _participantTin = ""
    private var _insurerName = ""
    private var _insurerTin = ""
    private var _hasAsanImza = false
    private var _operation = Operation()
    private var _operations = [Operation]()
    private var _isParticipantSaved = false
    
    init(pinCode: String, participantName: String, participantTin: String, insurerName: String, insurerTin: String, hasAsanImza: Bool, operation: Operation, operations: [Operation], isParticipantSaved: Bool) {
        _pinCode = pinCode
        _participantName = participantName
        _participantTin = participantTin
        _insurerName = insurerName
        _insurerTin = insurerTin
        _hasAsanImza = hasAsanImza
        _operation = operation
        _operations = operations
        _isParticipantSaved = isParticipantSaved
    }
    
    override init() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        if let data = aDecoder.decodeObject(forKey: Keys.pinCode) as? String {
            _pinCode = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.participantName) as? String {
            _participantName = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.participantTin) as? String {
            _participantTin = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.insurerName) as? String {
            _insurerName = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.insurerTin) as? String {
            _insurerTin = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.hasAsanImza) as? Bool {
            _hasAsanImza = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.operation) as? Operation {
            _operation = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.operations) as? [Operation] {
            _operations = data
        }
        _isParticipantSaved = aDecoder.decodeBool(forKey: Keys.isParticipantSaved)
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(_pinCode, forKey: Keys.pinCode)
        aCoder.encode(_participantName, forKey: Keys.participantName)
        aCoder.encode(_participantTin, forKey: Keys.participantTin)
        aCoder.encode(_insurerName, forKey: Keys.insurerName)
        aCoder.encode(_insurerTin, forKey: Keys.insurerTin)
        aCoder.encode(_hasAsanImza, forKey: Keys.hasAsanImza)
        aCoder.encode(_operation, forKey: Keys.operation)
        aCoder.encode(_operations, forKey: Keys.operations)
        aCoder.encode(_isParticipantSaved, forKey: Keys.isParticipantSaved)
    }
    
    struct Keys {
        static let pinCode = "pinCode"
        static var participantName = "participantName"
        static var participantTin = "participantTin"
        static var insurerName = "insurerName"
        static var insurerTin = "insurerTin"
        static var hasAsanImza = "hasAsanImza"
        static var operation = "operation"
        static var operations = "operations"
        static var isParticipantSaved = "isParticipantSaved"
    }
    
    class var filePath: String {
        let manager = FileManager.default
        let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first
        return url!.appendingPathComponent(Constants.participantInfoPath).path
    }
    
    var pinCode: String {
        get {
            return _pinCode
        }
        set {
            _pinCode = newValue
        }
    }
    
    var participantName: String {
        get {
            return _participantName
        }
        set {
            _participantName = newValue
        }
    }
    
    var participantTin: String {
        get {
            return _participantTin
        }
        set {
            _participantTin = newValue
        }
    }
    
    var insurerName: String {
        get {
            return _insurerName
        }
        set {
            _insurerName = newValue
        }
    }
    
    var insurerTin: String {
        get {
            return _insurerTin
        }
        set {
            _insurerTin = newValue
        }
    }
    
    var hasAsanImza: Bool {
        get {
            return _hasAsanImza
        }
        set {
            _hasAsanImza = newValue
        }
    }
    
    var operation: Operation {
        get {
            return _operation
        }
        set {
            _operation = newValue
        }
    }
    
    var operations: [Operation] {
        get {
            return _operations
        }
        set {
            _operations = newValue
        }
    }
    
    var isParticipantSaved: Bool {
        get {
            return _isParticipantSaved
        }
        set {
            _isParticipantSaved = newValue
        }
    }
    
    
}
