//
//  Services.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/18/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class Services {
    
    
    // Configure Alamofire manager for SSL Connection
    private static var Manager : Alamofire.SessionManager = {
        
        let pathToCert = Bundle.main.path(forResource: "deleted.crt", ofType: "crt")
        
        let serverTrustPolicy = ServerTrustPolicy.pinCertificates(
            certificates: ServerTrustPolicy.certificates(in: Bundle(identifier: "az.isb.mobile.ISB-Mobile")!),
            validateCertificateChain: true,
            validateHost: true
        )
        
        let serverTrustPolicies: [String: ServerTrustPolicy] = [
            Constants.URL: .disableEvaluation
        ]
        
        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        let manager = Alamofire.SessionManager(
            configuration: URLSessionConfiguration.default,
            serverTrustPolicyManager: ServerTrustPolicyManager(policies: serverTrustPolicies)
        )
        return manager
    }()
    
    // MARK: - Start Registration
    // If phone is not registered yet.
    static func startRegistration(completion: @escaping(AppResult<String>) -> Void) {
        print("\(Constants.startRegistration) called...")
        
        Manager.request(Router.startRegistration).validate(statusCode: 200..<300).responseString { response in
            
            let result = responseString(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                completion(AppResult.success(data: result.data!))
            }
        }
    }
    
    
    // MARK: - Auth Params
    static func authParams(byPhoneNumber phone: String, andID asanID: String, completion: @escaping(AppResult<Transaction>) -> Void) {
        print("\(Constants.authParams) called...")
        let params = ["phoneNumber": phone, "asanID": asanID]
        
        Manager.request(Router.authParams(params)).validate().responseJSON { response in
            
            let result = responseJSON(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                let transaction = Transaction(withData: result.data!)
                completion(AppResult.success(data: transaction))
            }
            
        }
        
    }
    
    // MARK: - Auth Result
    static func authResult(byTransactionID id: String, completion: @escaping(AppResult<EmployeeInfo>) -> Void) {
        print("\(Constants.authResult) called...")
        
        Manager.request(Router.authResult(id)).validate().responseJSON { response in
            
            if let headers = response.response?.allHeaderFields as? [String: String]{
                let token = headers["Token"]
                userDefaults.set(token, forKey: Constants.token)
            }
            
            let result = responseJSON(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                let employee = EmployeeInfo(withData: result.data!)
                completion(AppResult.success(data: employee))
            }
            
        }
        
    }
    
    // MARK: - Mismatch Person Info
    static func mismatchPersonInfo(completion: @escaping(AppResult<Any>) -> Void) {
        print("\(Constants.mismatchPersonInfo) called...")
        
        Manager.request(Router.mismatchPersonInfo).validate().response { response in
            
            completion(defaultResponse(response: response))
            
        }
        
    }
    
    // MARK: - Register App
    static func registerApp(completion: @escaping(AppResult<Any>) -> Void) {
        print("\(Constants.registerApp) called...")
        
        Manager.request(Router.registerApp).validate().response { response in
            
            if let headers = response.response?.allHeaderFields as? [String: String]{
                let token = headers["Token"]
                userDefaults.set(token, forKey: Constants.token)
            }
            
            completion(defaultResponse(response: response))
            
        }
        
    }
    
    // MARK: - Start Authentication
    static func startAuthentication(completion: @escaping(AppResult<String>) -> Void) {
        print("\(Constants.startAuthentication) called...")
        
        Manager.request(Router.startAuthentication).validate().responseString { response in
            
            let result = responseString(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                completion(AppResult.success(data: result.data!))
            }
            
        }
        
    }
    
    // MARK: - Authorizations
    static func authorizations(byPhoneNumber number: String, andID asanID: String, completion: @escaping(AppResult<ParticipantAuth>) -> Void) {
        print("\(Constants.authorizations) called...")
        
        Manager.request(Router.authorizations(number, asanID)).validate().responseJSON { response in
            
            let result = responseJSON(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                let participantAuth = ParticipantAuth(withData: result.data!)
                completion(AppResult.success(data: participantAuth))
            }
            
        }
        
    }
    
    // MARK: - End Authentication
    static func endAuthentication(completion: @escaping(AppResult<Any>) -> Void) {
        print("\(Constants.endAuthentication) called...")
        
        Manager.request(Router.endAuthentication).validate().response { response in
            
            completion(defaultResponse(response: response))
            
        }
        
    }
    
    // MARK: - Start Contract Operation
    static func startContractOperation(byType type: String, forParticipant participantTin: String, andInsurer insurerTin: String, completion: @escaping(AppResult<String>) -> Void) {
        print("\(Constants.startContractOperation) called...")
        let params = ["operationType": type, "participantTIN": participantTin, "insurerTIN": insurerTin]
        
        Manager.request(Router.startContractOperation(params)).validate().responseString { response in
            
            let result = responseString(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                completion(AppResult.success(data: result.data!))
            }
            
        }
        
    }
    
    // MARK: - Vehicle Info
    static func getVehicleInfo(byCertificationNumber certNumber: String, andCarNumber carNumber: String, completion: @escaping(AppResult<VehicleInfo>) -> Void) {
        print("\(Constants.vehicleInfo) called...")
        
        Manager.request(Router.getVehicleInfo(certNumber, carNumber)).validate().responseJSON { response in
            
            let result = responseJSON(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                let vehicleInfo = VehicleInfo(withData: result.data!)
                completion(AppResult.success(data: vehicleInfo))
            }
            
        }
        
    }
    
    // MARK: - Non Mia Vehicle Info
    static func saveNonMiaVehicleInfo(vehicleInfo: VehicleInfo, completion: @escaping(AppResult<VehicleInfo>) -> Void) {
        print("\(Constants.nonMiaVehicleInfo) called...")
        
        let params = [
            "bodyNumber": vehicleInfo.bodyNumber,
            "engineNumber": vehicleInfo.engineNumber,
            "carNumber": vehicleInfo.carNumber,
            "certificateNumber": vehicleInfo.certificateNumber,
            "yearOfManufacture": String(describing: vehicleInfo.yearOfManufacture),
            "vehicleColor": vehicleInfo.vehicleColor,
            "make": vehicleInfo.make,
            "model": vehicleInfo.model,
            "vehicleType": vehicleInfo.vehicleType,
            "vehicleTypeId": String(describing: vehicleInfo.vehicleTypeID),
            "engineCapacity": String(describing: vehicleInfo.engineCapacity),
            "horsePower": String(describing: vehicleInfo.horsePower)
        ]
        
        Manager.request(Router.saveNonMiaVehicleInfo(params)).validate().responseJSON { (response) in
            
            let result =  responseJSON(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                let vehicleInfo = VehicleInfo(withData: result.data!)
                completion(AppResult.success(data: vehicleInfo))
            }
            
        }
        
    }
    
    // MARK: - Mismatch Vehicle Info
    static func mismatchVehicleInfo(completion: @escaping(AppResult<Any>) -> Void) {
        print("\(Constants.mismatchVehicleInfo) called...")
        
        Manager.request(Router.mismatchVehicleInfo).validate().response { response in
            
            completion(defaultResponse(response: response))
            
        }
        
    }
    
    // MARK: - Natural Person Info
    static func getNaturalPersonInfo(byIDDocumentNumber number: String, completion: @escaping(AppResult<NaturalPerson>) -> Void) {
        print("\(Constants.naturalPersonInfo) called...")
        
        Manager.request(Router.getNaturalPersonInfo(number)).validate().responseJSON { response in
            
            let result = responseJSON(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                let naturalPerson = NaturalPerson(withData: result.data!)
                completion(AppResult.success(data: naturalPerson))
            }
            
        }
        
    }
    
    // MARK: - Non Resident Person Info
    static func getNonResidentPersonInfo(byRcNumber number: String, andDocType type: String, completion: @escaping(AppResult<NaturalPerson>) -> Void) {
        print("\(Constants.naturalPersonInfo) called...")
        
        Manager.request(Router.getNonResidentPersonInfo(number, type)).validate().responseJSON { response in
            
            let result = responseJSON(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                let naturalPerson = NaturalPerson(withData: result.data!)
                completion(AppResult.success(data: naturalPerson))
            }
            
        }
        
    }
    
    // MARK: - Non Resident Person Info
    static func saveNonResidentPersonInfo(withData data: NaturalPerson, completion: @escaping(AppResult<NaturalPerson>) -> Void) {
        print("\(Constants.naturalPersonInfo) called...")
        
        let params = ["lastName": data.lastName,
                      "firstName": data.firstName,
                      "patronymic": data.patronymic,
                      "pin": data.pin,
                      "idDocument": data.idDocument,
                      "idDocumentType": data.idDocumentType]
        
        Manager.request(Router.saveNonResidentPersonInfo(params)).validate().responseJSON { response in
            
            let result = responseJSON(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                let naturalPerson = NaturalPerson(withData: result.data!)
                completion(AppResult.success(data: naturalPerson))
            }
            
        }
        
    }
    
    // MARK: - Non Resident Person Info
    static func saveNonResidentPersonInfo(withData data: InsuredPerson, isJuridical: Bool, completion: @escaping(AppResult<NaturalPerson>) -> Void) {
        print("\(Constants.naturalPersonInfo) called...")
        
        let params: [String: String]!
        
        if isJuridical {
            params = ["fullName": data.fullName, "idDocument": data.idDocument]
        }  else {
            params = ["personType": data.personType,
                      "lastName": data.lastName,
                      "firstName": data.firstName,
                      "patronymic": data.patronymic,
                      "idDocument": data.idDocument]
        }
        
        Manager.request(Router.saveNonResidentPersonInfo(params)).validate().responseJSON { response in
            
            let result = responseJSON(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                let naturalPerson = NaturalPerson(withData: result.data!)
                completion(AppResult.success(data: naturalPerson))
            }
            
        }
        
    }
    
    // MARK: - Juridical Person Info
    static func getJuridicalPersonInfo(byTin tin: String, completion: @escaping(AppResult<JuridicalPerson>) -> Void) {
        print("\(Constants.juridicalPersonInfo) called...")
        
        Manager.request(Router.getJuridicalPersonInfo(tin)).validate().responseJSON { response in
            
            let result = responseJSON(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                let juridicalPerson = JuridicalPerson(withData: result.data!)
                completion(AppResult.success(data: juridicalPerson))
            }
            
        }
        
    }
    
    // MARK: - Contract Price
    static func getContractPrice(withPeriod period: String = "", completion: @escaping(AppResult<ContractPrice>) -> Void) {
        print("\(Constants.contractPrice) called...")
        
        Manager.request(Router.getContractPrise(period)).validate().responseJSON { response in
            
            let result = responseJSON(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                let contractPrice = ContractPrice(withData: result.data!)
                completion(AppResult.success(data: contractPrice))
            }
            
        }
        
    }
    
    // MARK: - Sign Contract
    static func signContract(withOperatorPhone number1: String, operatorAsan asanID: String, insuredPhone number2: String, insuredEmail email: String, completion: @escaping(AppResult<Transaction>) -> Void) {
        print("\(Constants.signContract) called...")
        let params = ["operatorPhoneNumber": number1, "operatorAsanID": asanID, "insuredPhoneNumber": number2, "insuredEmail": email]
        
        Manager.request(Router.signContract(params)).validate().responseJSON { response in
            
            let result = responseJSON(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                let transaction = Transaction(withData: result.data!)
                completion(AppResult.success(data: transaction))
            }
            
        }
    }
    
    // MARK: - Green Card Contract
    static func greenCardContract(withInsuredPhone number: String, andInsuredEmail email: String, completion: @escaping(AppResult<String>) -> Void) {
        print("\(Constants.signContract) called...")
        let params = ["insuredPhoneNumber": number, "insuredEmail": email]
        
        Manager.request(Router.greenCardContract(params)).validate().responseJSON { response in
            
            let result = responseJSON(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                completion(AppResult.success(data: result.data!["data"].stringValue))
            }
            
        }
    }
    
    // MARK: - Signing Result
    static func signingResult(completion: @escaping(AppResult<String>) -> Void) {
        print("\(Constants.signingResult) called...")
        
        Manager.request(Router.signingResult).validate().responseJSON { response in
//            completion(defaultResponse(response: response))
            let result = responseJSON(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                completion(AppResult.success(data: result.data!["data"].stringValue))
            }
        }
        
    }
    
    // MARK: - Cancel Contract Operation
    static func cancelContractOperation(completion: @escaping(AppResult<Any>) -> Void) {
        print("\(Constants.cancelContractOperation) called...")
        
        Manager.request(Router.cancelContractOperation).validate().response { response in
            
            completion(defaultResponse(response: response))
            
        }
    }
    
    // MARK: - Search My Contracts
    static func searchMyContracts(completion: @escaping(AppResult<[Contract]>) -> Void) {
        print("\(Constants.searchMyContracts) called...")
        
        Manager.request(Router.searchMyContracts).validate().responseJSON { response in
            
            let result = responseJSON(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                let contracts = Contract.parseList(withData: result.data!)
                completion(AppResult.success(data: contracts))
            }
            
        }
    }
    
    // MARK: - Search Authorized Contracts
    static func searchAuthorizedContracts(byParticipantTin partTin: String, andInsurerTin insTin: String, forOperationType type: String, completion: @escaping(AppResult<[Contract]>) -> Void) {
        print("\(Constants.searchAuthorizedContracts) (type: \(type)) called...")
        let params = ["participantTIN": partTin, "insurerTIN": insTin, "operationType": type]
        
        Manager.request(Router.searchAuthorizedContracts(params)).validate().responseJSON { response in
            
            let result = responseJSON(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                let contracts = Contract.parseList(withData: result.data!)
                completion(AppResult.success(data: contracts))
            }
            
        }
    }
    
    // MARK: - Get Contract
    static func getContract(byContractNumber number: String, completion: @escaping(AppResult<Contract>) -> Void) {
        print("\(Constants.getContract) called...")
        
        Manager.request(Router.getContract(number)).validate().responseJSON { response in
            
            let result = responseJSON(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                let contract = Contract.parse(withData: result.data!)
                completion(AppResult.success(data: contract))
            }
            
        }
        
    }
    
    // MARK: - Get Contract Document
    static func getContractDocument(byContractNumber number: String, completion: @escaping(AppResult<String>) -> Void) {
        print("\(Constants.getContractDoc) called...")
        
        Manager.request(Router.getContractDocument(number)).validate().responseString { (response) in
            
            let result = responseString(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                completion(AppResult.success(data: result.data!))
            }
            
        }
        
    }
    
    // MARK: - Get Termination Contract
    static func getTerminationContract(byContractNumber number: String, andOperationType type: String, completion: @escaping(AppResult<Contract>) -> Void) {
        print("\(Constants.getContract) with \(type) type called...")
        
        Manager.request(Router.getTerminationContract(number, type)).validate().responseJSON { response in
            
            let result = responseJSON(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                let contract = Contract.parse(withData: result.data!)
                completion(AppResult.success(data: contract))
            }
            
        }
        
    }
    
    // MARK: - Terminate Contract
    static func terminateContract(byContractNumber number: String, forParticipant tin: String, withReason reason: String, fromDate date: String, completion: @escaping(AppResult<String>) -> Void) {
        print("\(Constants.terminateContract) called...")
        
        let params = ["contractNumber": number,
                      "terminationReason": reason,
                      "terminationDate": date,
                      "participantTIN": tin]
        
        Manager.request(Router.terminateContract(params)).validate().responseJSON { response in
            
            let result = responseJSON(response: response)
            if !result.isSuccess {
                completion(AppResult.error(message: result.errorMessage ?? ""))
            } else {
                let data = (result.data!)["resultCode"].stringValue
                completion(AppResult.success(data: data))
            }
            
        }
        
    }
    
    // MARK: - Blank
    static func saveBlank(withData data: [String: String], completion: @escaping(AppResult<Any>) -> Void) {
        print("\(Constants.blank) called...")
        
        Manager.request(Router.saveBlank(data)).validate().response { response in
            
            completion(defaultResponse(response: response))
            
        }
        
    }
    
    // MARK: - Vehicle horse power
    static func vehicleHorsePower(power: Int, completion: @escaping(AppResult<Any>) -> Void) {
        print("\(Constants.vehicleHorsePower) called...")
        
        Manager.request(Router.vehicleHorsePower(power)).validate().response { response in
            
            completion(defaultResponse(response: response))
            
        }
        
    }
    
    // MARK: - Response JSON
    fileprivate static func responseJSON(response: DataResponse<Any>) -> AppResult<JSON> {
        
        var result = AppResult<JSON>()
        
        if response.response?.statusCode == 401 {
            
            ApplicationManager.redirectToLogin()
            return AppResult(isSuccess: false, message: Constants.tokenError)
        } else {
            switch response.result {
                
            case .failure(_):
                if let data = response.data {
                    result = AppResult(errorWithData: JSON(data))
                    return result
                } else {
                    result.isSuccess = false
                    result.errorMessage = response.result.error?.localizedDescription
                    return result
                }
            case .success(_):
                if let data = response.result.value {
                    
                    let json = JSON(data)
                    if let errorCode = json["code"].string {
                        result.isSuccess = false
                        result.resultCode = errorCode
                        result.errorMessage = json["error"].string!
                        return result
                    } else {
                        result.isSuccess = true
                        result.data = json
                        return result
                        
                    }
                    
                } else {
                    result.isSuccess = false
                    result.errorMessage = Constants.dataError
                    return result
                }
            }
        }
        
    }
    
    // MARK: - Response String
    fileprivate static func responseString(response: DataResponse<String>) -> AppResult<String> {
        
        var result = AppResult<String>()
        
        if response.response?.statusCode == 401 {
            
            ApplicationManager.redirectToLogin()
            return AppResult(isSuccess: false, message: Constants.tokenError)
        } else {
            switch response.result {
                
            case .failure(_):
                if let data = response.data {
                    result = AppResult(errorWithData: JSON(data))
                    return result
                } else {
                    result.isSuccess = false
                    result.errorMessage = response.result.error?.localizedDescription
                    return result
                }
            case .success(_):
                if let data = response.result.value {
                    let stringData = data.replacingOccurrences(of: "\"", with: "")
                    result.isSuccess = true
                    result.data = stringData
                    return result
                } else {
                    result.isSuccess = false
                    result.errorMessage = Constants.dataError
                    return result
                }
                
            }
        }
        
    }
    
    // MARK: - Default Response
    fileprivate static func defaultResponse(response: DefaultDataResponse) -> AppResult<Any> {
        
        var result = AppResult<Any>()
        
        if response.response?.statusCode == 401 {
            
            ApplicationManager.redirectToLogin()
            return AppResult(isSuccess: false, message: Constants.tokenError)
        } else {
            
            if let error = response.error {
                result.isSuccess = false
                result.errorMessage = error.localizedDescription
                return result
            } else {
                result.isSuccess = true
                return result
            }
            
        }
        
        
    }
    
}
