//
//  UserDetailsCell.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/21/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

class UserDetailsCell: UITableViewCell {

    let infoLbl: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "LABEL"
        label.font = UIFont(name: Constants.helveticaBold, size: 11)
        label.textColor = UIColor.labelColor
        return label
    }()
    
    let dataLbl: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Label"
        label.font = UIFont(name: Constants.helveticaBold, size: 17)
        label.textColor = UIColor.mainTextColor
        label.numberOfLines = 0
        return label
    }()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupCustomCell(infoLabel: infoLbl, dataLabel: dataLbl)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func config(withCellCustomizer data: CellDataCustomizer) {
        infoLbl.text = data.label
        dataLbl.text = data.data as? String
    }

}
