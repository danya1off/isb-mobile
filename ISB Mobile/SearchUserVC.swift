//
//  SearchUserVC.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/20/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD

class SearchUserVC: UIViewController, UITextFieldDelegate {
    
    var navigationTitle = ""
    var navigationSubTitle = ""

    let formView = UIView()
    let residentView = UIView()
    let nonResidentView = UIView()
    
    let personTypeToggleBtn = UISwitch()
    let residentTypeToggleBtn = UISwitch()
    var naturalPersonLbl: UILabel!
    var juridicalPersonLbl: UILabel!
    var residentPersonLbl: UILabel!
    var nonResidentPersonLbl: UILabel!
    
    var idNumberTxtField: CustomTextField!
    var idNumberLbl: UILabel!
    
    var lastNameTxtField: CustomTextField!
    var firstNameTxtField: CustomTextField!
    var patronymicTxtField: CustomTextField!
    var pinTxtField: CustomTextField!
    var idDocumentTxtField: CustomTextField!
    
    var searchBtn: UIButton!
    let toggleBtn = UISwitch()
    let toggleLbl = UILabel()
    let toggleModeLbl = UILabel()
    
    var personSelectIndex: Int?
    var residencySelectorIndex: Int?
    var idDocumentValue: String?
    var idDocumentType: String?
    var operationCode: String?
    
    private var userManager: UserManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userManager = UserManager(view: self)
        if let operCode = userDefaults.string(forKey: Constants.operationCode) {
            operationCode = operCode
        } else {
            fireErrorAlert(withMessage: "Əməliyyat kodu tapılmadı!")
        }

        //create main UI
        createUI()
        residentTypeToggleChanged()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userDefaults.set(String(describing: self.classForCoder) , forKey: Constants.operationName)
    }
    
    @objc func searchUser() {
        switch checkInputs() {
        case .valid:
            if !personTypeToggleBtn.isOn {
                if !residentTypeToggleBtn.isOn {
                    userManager.getPersonInfo(byPersonType: .natural, andIdentificator: idNumberTxtField.text!)
                } else {
                    var docType = ""
                    if toggleBtn.isOn {
                        docType = "TRC"
                    } else {
                        docType = "PRC"
                    }
                    userManager.getPersonInfo(byPersonType: .nonResident(docType), andIdentificator: idNumberTxtField.text!)
                }
            } else {
                userManager.getPersonInfo(byPersonType: .juridical, andIdentificator: idNumberTxtField.text!)
            }
        case .failure(let error):
            fireErrorAlert(withMessage: error)
        }
    }
    
    func checkInputs() -> Validation {
        if residentTypeToggleBtn.isOn {
            if operationCode == Constants.BORDER_CMTPL_CONTRACT_OPERATION {
                if lastNameTxtField.text == "" {
                    return .failure("Soyadı boş ola bilməz!")
                }
                
                if firstNameTxtField.text == "" {
                    return .failure("Adı boş ola bilməz!")
                }
                
                if pinTxtField.text == "" {
                    return .failure("FİN boş ola bilməz!")
                }
                
                if idDocumentTxtField.text == "" {
                    return .failure("Yaşama icazə vəsiqəsi boş ola bilməz!")
                }
            } else {
                if idNumberTxtField.text == "" {
                    return .failure("Yaşama icazə vəsiqəsi boş ola bilməz!")
                }
            }
            return .valid
        } else {
            if idNumberTxtField.text != "" {
                let input = idNumberTxtField.text!
                if !personTypeToggleBtn.isOn {
                    if !residentTypeToggleBtn.isOn {
                        if input.count > 11 || input.count < 11 {
                            return .failure("Şəxsiyyət vəsiqənin nömrəsi 11 simvoldan ibarət olmalıdır!")
                        }
                        
                        let serialCode = input.substring(to: 3)
                        let serialNumber = input.substring(from: 3)
                        
                        if !serialCode.latinCharactersOnly {
                            return .failure("Şəxsiyyət vəsiqənin seriya 3 hərfdən ibarət olmalıdır!")
                        }
                        
                        if !serialNumber.numbersOnly {
                            return .failure("Şəxsiyyət vəsiqənin nömrəsi 8 rəqəmdən ibarət olmalıdır!")
                        }
                    } else {
                        if input.count > 10 || input.count < 10 {
                            return .failure("VÖEN 11 simvoldan ibarət olmalıdır!")
                        }
                        
                        if !input.numbersOnly {
                            return .failure("VÖEN yalnız rəqəmlərdən ibarət olmalıdır!")
                        }
                    }
                }
            } else {
                let errorString = !personTypeToggleBtn.isOn ? "Şəxsiyyət vəsiqənin nömrəsi boş ola bilməz!" : "VÖEN boş ola bilməz!"
                return .failure(errorString)
            }
        }
        return .valid
    }
    
    // if switch from non  resident to juridical person's view
    var flag = false
    
    @objc func personTypeToggleChanged() {
        userDefaults.set(personTypeToggleBtn.isOn, forKey: Constants.personType)
        toggleBtn.removeFromSuperview()
        toggleLbl.removeFromSuperview()
        
        if personTypeToggleBtn.isOn {
            juridicalPersonLbl.textColor = UIColor.mainTextColor
            naturalPersonLbl.textColor = UIColor.labelColor
            
            residentTypeToggleBtn.removeFromSuperview()
            idNumberTxtField.removeFromSuperview()
            
            if flag && operationCode == Constants.BORDER_CMTPL_CONTRACT_OPERATION {
                toggleBtn.removeFromSuperview()
                toggleLbl.removeFromSuperview()
                lastNameTxtField.removeFromSuperview()
                firstNameTxtField.removeFromSuperview()
                patronymicTxtField.removeFromSuperview()
                pinTxtField.removeFromSuperview()
                idDocumentTxtField.removeFromSuperview()
            }
            
            searchBtn.removeFromSuperview()
            nonResidentView.removeFromSuperview()
            residentView.removeFromSuperview()
            createResidentView(mode: .juridicalPerson)
            
            idNumberTxtField.placeholder = "Ex.: 4444555566"
            idNumberLbl.text = "VÖEN"
            idNumberTxtField.keyboardType = .numberPad
            idNumberTxtField.text = "4444555566"
        } else {
            juridicalPersonLbl.textColor = UIColor.labelColor
            naturalPersonLbl.textColor = UIColor.mainTextColor
            
            createResidentTypeToggleBtn()
            idNumberTxtField.removeFromSuperview()
            searchBtn.removeFromSuperview()
            residentView.removeFromSuperview()
            nonResidentView.removeFromSuperview()
            createResidentView(mode: .naturalPerson)
            
            if !residentTypeToggleBtn.isOn {
                idNumberTxtField.placeholder = "Ex.: AZE82374289"
                idNumberLbl.text = "ŞƏXSİYYƏT VƏSİQƏ"
                idNumberTxtField.text = "AZE11112222"
            } else {
                idNumberTxtField.placeholder = "Ex.: 12345678"
                idNumberLbl.text = "YAŞAMA İCAZƏ VƏSİQƏSİ"
                idNumberTxtField.text = "12345678"
            }
        }
        
    }
    
    @objc func residentTypeToggleChanged() {
        userDefaults.set(residentTypeToggleBtn.isOn, forKey: Constants.residencyType)
        if !residentTypeToggleBtn.isOn {
            
            nonResidentPersonLbl.textColor = UIColor.labelColor
            residentPersonLbl.textColor = UIColor.mainTextColor
            
            flag = true
            
            if operationCode == Constants.BORDER_CMTPL_CONTRACT_OPERATION {
                lastNameTxtField.removeFromSuperview()
                firstNameTxtField.removeFromSuperview()
                patronymicTxtField.removeFromSuperview()
                pinTxtField.removeFromSuperview()
                idDocumentTxtField.removeFromSuperview()
            } else {
                idNumberTxtField.removeFromSuperview()
            }
            toggleBtn.removeFromSuperview()
            toggleLbl.removeFromSuperview()
            searchBtn.removeFromSuperview()
            
            nonResidentView.removeFromSuperview()
            createResidentView(mode: .naturalPerson)
            
            idNumberTxtField.placeholder = "Ex.: AZE82374289"
            idNumberLbl.text = "ŞƏXSİYYƏT VƏSİQƏ"
            idNumberTxtField.text = "AZE11112222"
        } else {
            nonResidentPersonLbl.textColor = UIColor.mainTextColor
            residentPersonLbl.textColor = UIColor.labelColor
            
            residentView.removeFromSuperview()
            idNumberTxtField.removeFromSuperview()
            searchBtn.removeFromSuperview()
            createToggleLbl()
            createToggleBtn()
            creteNonResidentView()
            
            idNumberTxtField.placeholder = "Ex.: 12345678"
            idNumberLbl.text = "YAŞAMA İCAZƏ VƏSİQƏSİ"
//            idNumberTxtField.text = "12345678"
        }
        
    }
    
    @objc func toggleValueChanged() {
        if toggleBtn.isOn {
            toggleModeLbl.text = "MÜVƏQQƏTİ"
        } else {
            toggleModeLbl.text = "DAİMİ"
        }
    }
    
    @objc func cancelOperation() {
        let contractManager = ContractManager(view: self)
        contractManager.cancelContractOperation()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField.tag == idNumberTxtField.tag {
            if !personTypeToggleBtn.isOn && !residentTypeToggleBtn.isOn {
                return ApplicationManager.setMaxLength(forTextField: textField, withRange: range, withString: string, maxLength: 11)
            }
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
