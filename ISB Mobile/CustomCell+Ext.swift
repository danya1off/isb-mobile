//
//  CustomCell+Ext.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/20/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

extension UITableViewCell {
    
    func setupCustomCell(infoLabel: UILabel, dataLabel: UILabel) {
        
        self.selectionStyle = .none
        
        addSubview(dataLabel)
        addSubview(infoLabel)
        
        dataLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        dataLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
        dataLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        
        infoLabel.leadingAnchor.constraint(equalTo: dataLabel.leadingAnchor).isActive = true
        infoLabel.trailingAnchor.constraint(equalTo: dataLabel.trailingAnchor).isActive = true
        infoLabel.bottomAnchor.constraint(equalTo: dataLabel.topAnchor, constant: -3).isActive = true
        
    }
    
}
