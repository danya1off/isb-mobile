//
//  GreenCardManager.swift
//  ISB Mobile
//
//  Created by  Simberg on 11/8/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD

class GreenCardManager {
    
    private var vc: UIViewController!
    
    init(view: UIViewController) {
        self.vc = view
    }
    
    func sendBlank(withParams params: Dictionary<String, String>) {
        let viewController = vc as! GreenCardNewBlankVC
        SVProgressHUD.show(withStatus: "Gözləyin...")
        Services.saveBlank(withData: params) { (result) in
            SVProgressHUD.dismiss()
            guard result.isSuccess else {
                viewController.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            
            let searchVehicleVC = SearchVehicleVC()
            searchVehicleVC.navigationTitle = viewController.navigationTitle
            searchVehicleVC.navigationSubTitle = viewController.navigationSubTitle
            self.vc.navigationController?.pushViewController(searchVehicleVC, animated: true)
        }
    }
    
}
