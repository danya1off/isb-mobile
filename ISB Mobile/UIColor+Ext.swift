//
//  UIColor+Ext.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/19/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var mainAppColor: UIColor {
        return UIColor(red:0.00, green:0.75, blue:0.81, alpha:1.0)
    }
    
    static var mainTextColor: UIColor {
        return UIColor(red:0.47, green:0.57, blue:0.60, alpha:1.0)
    }
    
    static var labelColor: UIColor {
        return UIColor(red:0.86, green:0.86, blue:0.86, alpha:1.0)
    }
    
    static var dimmedGray: UIColor {
        return UIColor(red:0.86, green:0.86, blue:0.86, alpha:1.0)
    }
    
    static var detailsColor: UIColor {
        return UIColor(red:0.71, green:0.73, blue:0.77, alpha:1.0)
    }
    
    static var detailsBgColor: UIColor {
        return UIColor(red:0.95, green:0.95, blue:0.95, alpha:1.0)
    }
    
}
