//
//  NaturalPerson.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/21/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import SwiftyJSON

class NaturalPerson: NSObject, NSCoding {
    
    private var _personType = ""
    private var _fullName = ""
    private var _lastName = ""
    private var _firstName = ""
    private var _patronymic = ""
    private var _birthPlace = ""
    private var _birthDate = ""
    private var _pin = ""
    private var _address = ""
    private var _idDocument = ""
    private var _idDocumentType = ""
    
    init(withData data: JSON) {
        
        _personType = data["personType"].string ?? ""
        _fullName = data["fullName"].string ?? ""
        _lastName = data["lastName"].string ?? ""
        _firstName = data["firstName"].string ?? ""
        _patronymic = data["patronymic"].string ?? ""
        _birthPlace = data["birthPlace"].string ?? ""
        _birthDate = data["birthDate"].string ?? ""
        _pin = data["pin"].string ?? ""
        _address = data["address"].string ?? ""
        _idDocument = data["idDocument"].string ?? ""
        _idDocumentType = data["idDocumentType"].string ?? ""
        
    }
    
    override init() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        if let data = aDecoder.decodeObject(forKey: Keys.personType) as? String {
            _personType = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.fullName) as? String {
            _fullName = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.lastName) as? String {
            _lastName = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.firstName) as? String {
            _firstName = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.patronymic) as? String {
            _patronymic = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.birthPlace) as? String {
            _birthPlace = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.birthDate) as? String {
            _birthDate = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.pin) as? String {
            _pin = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.address) as? String {
            _address = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.idDocument) as? String {
            _idDocument = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.idDocumentType) as? String {
            _idDocumentType = data
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(_personType, forKey: Keys.personType)
        aCoder.encode(_fullName, forKey: Keys.fullName)
        aCoder.encode(_lastName, forKey: Keys.lastName)
        aCoder.encode(_firstName, forKey: Keys.firstName)
        aCoder.encode(_patronymic, forKey: Keys.patronymic)
        aCoder.encode(_birthPlace, forKey: Keys.birthPlace)
        aCoder.encode(_birthDate, forKey: Keys.birthDate)
        aCoder.encode(_pin, forKey: Keys.pin)
        aCoder.encode(_address, forKey: Keys.address)
        aCoder.encode(_idDocument, forKey: Keys.idDocument)
        aCoder.encode(_idDocumentType, forKey: Keys.idDocumentType)
    }
    
    struct Keys {
        static var personType = "personType"
        static var fullName = "fullName"
        static var lastName = "lastName"
        static var firstName = "firstName"
        static var patronymic = "patronymic"
        static var birthPlace = "birthPlace"
        static var birthDate = "birthDate"
        static var pin = "pin"
        static var address = "address"
        static var idDocument = "idDocument"
        static var idDocumentType = "idDocumentType"
    }
    
    class var filePath: String {
        let manager = FileManager.default
        let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first
        return url!.appendingPathComponent(Constants.naturalPersonPath).path
    }
    
    var naturalPersonArray: [CellDataCustomizer] {
        var arr = [CellDataCustomizer]()
        let date = Date()
        
        var customizer: CellDataCustomizer
        
        if _lastName != "" {
            customizer = CellDataCustomizer(label: "SOYADI", data: _lastName)
            arr.append(customizer)
        }
        if _firstName != "" {
            customizer = CellDataCustomizer(label: "ADI", data: _firstName)
            arr.append(customizer)
        }
        if _patronymic != "" {
            customizer = CellDataCustomizer(label: "ATA ADI", data: _patronymic)
            arr.append(customizer)
        }
        if _birthPlace != "" {
            customizer = CellDataCustomizer(label: "DOĞULDUĞU YER", data: _birthPlace)
            arr.append(customizer)
        }
        if _birthDate != "" {
            customizer = CellDataCustomizer(label: "DOĞULDUĞU TARİX", data: date.localFormat(dateString: _birthDate))
            arr.append(customizer)
        }
        if _pin != "" {
            customizer = CellDataCustomizer(label: "PİN", data: _pin)
            arr.append(customizer)
        }
        if _address != "" {
            customizer = CellDataCustomizer(label: "ÜNVAN", data: _address)
            arr.append(customizer)
        }
        
        return arr
    }
    
    
    var personType : String {
        get {
            return _personType
        }
        set {
            _personType = newValue
        }
    }
    
    var fullName : String {
        get {
            return _fullName
        }
        set {
            _fullName = newValue
        }
    }
    
    var lastName : String {
        get {
            return _lastName
        }
        set {
            _lastName = newValue
        }
    }
    
    var firstName : String {
        get {
            return _firstName
        }
        set {
            _firstName = newValue
        }
    }
    
    var patronymic : String {
        get {
            return _patronymic
        }
        set {
            _patronymic = newValue
        }
    }
    
    var birthPlace : String {
        get {
            return _birthPlace
        }
        set {
            _birthPlace = newValue
        }
    }
    
    var birthDate : String {
        get {
            return _birthDate
        }
        set {
            _birthDate = newValue
        }
    }
    
    var pin : String {
        get {
            return _pin
        }
        set {
            _pin = newValue
        }
    }
    
    var address : String {
        get {
            return _address
        }
        set {
            _address = newValue
        }
    }
    
    var idDocument : String {
        get {
            return _idDocument
        }
        set {
            _idDocument = newValue
        }
    }
    
    var idDocumentType : String {
        get {
            return _idDocumentType
        }
        set {
            _idDocumentType = newValue
        }
    }
    
    
}
