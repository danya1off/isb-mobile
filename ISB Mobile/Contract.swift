//
//  Contract.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/31/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import SwiftyJSON

class Contract: NSObject, NSCoding {
    
    private var _contractNumber = ""
    private var _insuranceCompanyTIN = ""
    private var _insuranceCompanyName = ""
    private var _creationDate = ""
    private var _effectiveDate = ""
    private var _expiryDate = ""
    private var _contractOperator = Operator()
    private var _vehicle = VehicleInfo()
    private var _insuredPerson = InsuredPerson()
    private var _contractPrice = ContractPrice()
    private var _status = Status()
    private var _operationType = ""
    private var _terminationReturningAmount = 0.0
    
    static func parseList (withData data: JSON) -> [Contract] {
        var contracts = [Contract]()
        
        for item in data["data"].arrayValue {
            
            let contract = Contract()
            let insuredPerson = InsuredPerson()
            let status = Status()
            contract.contractNumber = item["contractNumber"].string ?? ""
            insuredPerson.fullName = item["insuredName"].string ?? ""
            contract.insuredPerson = insuredPerson
            let contractPrice = ContractPrice(price: item["price"].double ?? 0.0, bmCoefficient: 0.0, calculatedPrice: 0.0, isPriceFromLastContract: false)
            contract.contractPrice = contractPrice
            contract.vehicle.carNumber = item["carNumber"].string ?? ""
            status.code = item["status"]["code"].string ?? ""
            status.name = item["status"]["name"].string ?? ""
            contract.status = status
            contract.creationDate = item["creationDate"].string ?? ""
            contract.operationType = item["cmtplType"].string ?? ""
            contracts.append(contract)
            
        }
        
        return contracts
    }
    
    static func parse (withData data: JSON) -> Contract {
        let contract = Contract()
        
        contract.contractNumber = data["contractNumber"].string ?? ""
        contract.insuranceCompanyTIN = data["insuranceCompanyTIN"].string ?? ""
        contract.insuranceCompanyName = data["insuranceCompanyName"].string ?? ""
        contract.creationDate = data["creationDate"].string ?? ""
        contract.effectiveDate = data["effectiveDate"].string ?? ""
        contract.expiryDate = data["expiryDate"].string ?? ""
        contract.terminationReturningAmount = data["terminationReturningAmount"].double ?? 0.0
        
        let oper = Operator()
        oper.pinCode = data["operator"]["pinCode"].string ?? ""
        oper.fullName = data["operator"]["fullName"].string ?? ""
        contract.contractOperator = oper
        
        contract.vehicle = VehicleInfo(withData: data["vehicle"])
        
        let insuredPerson = InsuredPerson()
        insuredPerson.fullName = data["insuredPerson"]["fullName"].string ?? ""
        insuredPerson.lastName = data["insuredPerson"]["lastName"].string ?? ""
        insuredPerson.firstName = data["insuredPerson"]["firstName"].string ?? ""
        insuredPerson.patronymic = data["insuredPerson"]["patronymic"].string ?? ""
        insuredPerson.pin = data["insuredPerson"]["pin"].string ?? ""
        insuredPerson.address = data["insuredPerson"]["address"].string ?? ""
        insuredPerson.idDocument = data["insuredPerson"]["idDocument"].string ?? ""
        contract.insuredPerson = insuredPerson
        
        contract.contractPrice = ContractPrice(withData: data["contractPrice"])
        
        let status = Status()
        status.code = data["status"]["code"].string ?? ""
        status.name = data["status"]["name"].string ?? ""
        contract.status = status
        
        return contract
    }
    
    override init() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        if let data = aDecoder.decodeObject(forKey: Keys.contractNumber) as? String {
            _contractNumber = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.insuranceCompanyTIN) as? String {
            _insuranceCompanyTIN = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.insuranceCompanyName) as? String {
            _insuranceCompanyName = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.creationDate) as? String {
            _creationDate = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.effectiveDate) as? String {
            _effectiveDate = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.expiryDate) as? String {
            _expiryDate = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.operationType) as? String {
            _operationType = data
        }
        
        _terminationReturningAmount = aDecoder.decodeDouble(forKey: Keys.terminationReturningAmount)
        
        if let data = aDecoder.decodeObject(forKey: Keys.contractOperator) as? Operator {
            _contractOperator = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.vehicle) as? VehicleInfo {
            _vehicle = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.insuredPerson) as? InsuredPerson {
            _insuredPerson = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.contractPrice) as? ContractPrice {
            _contractPrice = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.status) as? Status {
            _status = data
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(_contractNumber, forKey: Keys.contractNumber)
        aCoder.encode(_insuranceCompanyTIN, forKey: Keys.insuranceCompanyTIN)
        aCoder.encode(_insuranceCompanyName, forKey: Keys.insuranceCompanyName)
        aCoder.encode(_creationDate, forKey: Keys.creationDate)
        aCoder.encode(_effectiveDate, forKey: Keys.effectiveDate)
        aCoder.encode(_expiryDate, forKey: Keys.expiryDate)
        aCoder.encode(_contractOperator, forKey: Keys.contractOperator)
        aCoder.encode(_vehicle, forKey: Keys.vehicle)
        aCoder.encode(_insuredPerson, forKey: Keys.insuredPerson)
        aCoder.encode(_contractPrice, forKey: Keys.contractPrice)
        aCoder.encode(_status, forKey: Keys.status)
        aCoder.encode(_operationType, forKey: Keys.operationType)
        aCoder.encode(_terminationReturningAmount, forKey: Keys.terminationReturningAmount)
    }
    
    struct Keys {
        static let contractNumber = "contractNumber"
        static let insuranceCompanyTIN = "insuranceCompanyTIN"
        static let insuranceCompanyName = "insuranceCompanyName"
        static let creationDate = "creationDate"
        static let effectiveDate = "effectiveDate"
        static let expiryDate = "expiryDate"
        static let contractOperator = "contractOperator"
        static let vehicle = "vehicle"
        static let insuredPerson = "insuredPerson"
        static let contractPrice = "contractPrice"
        static let status = "status"
        static let operationType = "operationType"
        static let terminationReturningAmount = "terminationReturningAmount"
    }
    
    class var filePath: String {
        let manager = FileManager.default
        let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first
        return url!.appendingPathComponent(Constants.contractPath).path
    }
    
    var contractDetailsArray: [CellDataCustomizer] {
        var arr = [CellDataCustomizer]()
        
        var customizer: CellDataCustomizer
        
        if _insuranceCompanyName != "" {
            customizer = CellDataCustomizer(label: "SIĞORTA ŞİRKƏTİ", data: _insuranceCompanyName)
            arr.append(customizer)
        }
        if _insuredPerson.fullName != "" {
            customizer = CellDataCustomizer(label: "SIĞORTALI ŞƏXS", data: _insuredPerson.fullName)
            arr.append(customizer)
        }
        if _vehicle.make != "" {
            var carMake = _vehicle.make
            if _vehicle.model != "" {
                carMake += " \(_vehicle.model)"
            }
            customizer = CellDataCustomizer(label: "MAŞIN", data: carMake)
            arr.append(customizer)
        }
        customizer = CellDataCustomizer(label: "SIĞORTA HAQQI", data: "\(_contractPrice.price) AZN")
        arr.append(customizer)
        
        return arr
    }
    
    var contractNumber: String {
        get {
            return _contractNumber
        }
        set {
            _contractNumber = newValue
        }
    }
    
    var insuranceCompanyTIN: String {
        get {
            return _insuranceCompanyTIN
        }
        set {
            _insuranceCompanyTIN = newValue
        }
    }
    
    var insuranceCompanyName: String {
        get {
            return _insuranceCompanyName
        }
        set {
            _insuranceCompanyName = newValue
        }
    }
    
    var creationDate: String {
        get {
            return _creationDate
        }
        set {
            _creationDate = newValue
        }
    }
    
    var effectiveDate: String {
        get {
            return _effectiveDate
        }
        set {
            _effectiveDate = newValue
        }
    }
    
    var expiryDate: String {
        get {
            return _expiryDate
        }
        set {
            _expiryDate = newValue
        }
    }
    
    var contractOperator: Operator {
        get {
            return _contractOperator
        }
        set {
            _contractOperator = newValue
        }
    }
    
    var vehicle: VehicleInfo {
        get {
            return _vehicle
        }
        set {
            _vehicle = newValue
        }
    }
    
    var insuredPerson: InsuredPerson {
        get {
            return _insuredPerson
        }
        set {
            _insuredPerson = newValue
        }
    }
    
    var contractPrice: ContractPrice {
        get {
            return _contractPrice
        }
        set {
            _contractPrice = newValue
        }
    }
    
    var status: Status {
        get {
            return _status
        }
        set {
            _status = newValue
        }
    }
    
    var operationType: String {
        get {
            return _operationType
        }
        set {
            _operationType = newValue
        }
    }
    
    var terminationReturningAmount: Double {
        get {
            return _terminationReturningAmount
        }
        set {
            _terminationReturningAmount = newValue
        }
    }
    
    
}
