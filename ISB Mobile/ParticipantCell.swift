//
// Created by  Simberg on 9/28/17.
// Copyright (c) 2017  Simberg. All rights reserved.
//

import UIKit

class ParticipantCell: UITableViewCell {

    let nameLbl: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.text = "Name Label"
        label.font = UIFont(name: Constants.helveticaBold, size: 16)
        label.textColor = UIColor.mainTextColor
        return label
    }()

    let tinLbl: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .left
        label.text = "Tin Label"
        label.font = UIFont(name: Constants.helveticaRegular, size: 10)
        label.textColor = UIColor.lightGray
        return label
    }()

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("")
    }

    func setupView() {

        addSubview(nameLbl)
        addSubview(tinLbl)

        nameLbl.leftAnchor.constraint(equalTo: leftAnchor, constant: 20).isActive = true
        nameLbl.topAnchor.constraint(equalTo: topAnchor, constant: 8).isActive = true

        tinLbl.leftAnchor.constraint(equalTo: nameLbl.leftAnchor).isActive = true
        tinLbl.topAnchor.constraint(equalTo: nameLbl.bottomAnchor, constant: 5).isActive = true

    }
    
    func configure(withName name: String, andTin tin: String) {
        self.nameLbl.text = name
        self.tinLbl.text = "TIN: \(tin)"
    }

}
