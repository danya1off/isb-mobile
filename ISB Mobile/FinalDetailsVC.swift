//
//  FinalDetailsVC.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/21/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SCLAlertView
import SVProgressHUD

class FinalDetailsVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {

    let tableView = UITableView()
    var confirmBtn: UIButton!
    var finalDetailsArray = [CellDataCustomizer]()
    
    var isPriceFromLastContract = false
    
    var docController: UIDocumentInteractionController?
    private var contractManager: ContractManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contractManager = ContractManager(view: self)
        
        //create main UI
        customizeFinalData()
        createUI()
        
        if isPriceFromLastContract {
            _ = fireInfoAlert(withMessage: "NV-nin qeydiyyat şəhadətnaməsi məlumatları əvvəlki sığorta müqaviləsində daxil edilmiş məlumatlara uyğun gəlmir. Zəhmət olmazsa, DİN-lə əlaqə saxlayıb məlumatları dəqiqləşdirin.", showCloseButton: true, returnAlert: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userDefaults.set(String(describing: self.classForCoder) , forKey: Constants.operationName)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return finalDetailsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 100
        } else {
            return 70
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.finalDetailCell) as! FinalDetailsCell
        cell.config(withCellCustomizer: finalDetailsArray[indexPath.row])
        return cell
    }
    
    @objc func createContract() {
        guard let phone = userDefaults.string(forKey: Constants.phoneNumber) else {
            self.fireErrorAlert(withMessage: "Operatorun telefon nömrəsi boş ola bilməz!")
            return
        }
        
        guard let asan = userDefaults.string(forKey: Constants.asanID) else {
            self.fireErrorAlert(withMessage: "Operatorun asan nömrəsi boş ola bilməz!")
            return
        }
        
        guard let part = ApplicationManager.getParticipantInfo() else {
            self.fireErrorAlert(withMessage: "İştirakçı tapılmadı")
            return
        }
        signContract(operatorPhone: phone, asanID: asan, insuredPhone: "", participant: part, subViewTitle: "Məlumatlarınızı doldurun!")
    }
    
    fileprivate func signContract(operatorPhone: String, asanID: String, insuredPhone: String, participant: ParticipantInfo, subViewTitle: String) {
        
        self.fireEditAlert(withTitle: subViewTitle, phoneNumber: insuredPhone) { phoneNumber, email in
            if phoneNumber == "" {
                self.signContract(operatorPhone: operatorPhone, asanID: asanID, insuredPhone: phoneNumber, participant: participant, subViewTitle: "Sığortalının telefon nömrəsi boş ola bilməz!")
            } else if !phoneNumber.numbersOnly {
                self.signContract(operatorPhone: operatorPhone, asanID: asanID, insuredPhone: phoneNumber, participant: participant, subViewTitle: "Sığortalının telefon nömrəsi düzgün formatda deyil!")
            } else if phoneNumber.count > 12 {
                self.signContract(operatorPhone: operatorPhone, asanID: asanID, insuredPhone: phoneNumber, participant: participant, subViewTitle: "Sığortalının telefon nömrəsi 12 rəqəmdən çox ola bilməz!")
            } else if email != "" && !self.isValid(email: email) {
                self.signContract(operatorPhone: operatorPhone, asanID: asanID, insuredPhone: phoneNumber, participant: participant, subViewTitle: "Insured email in wrong format!")
            } else {
                guard let operationCode = userDefaults.string(forKey: Constants.operationCode) else {
                    return
                }
                if operationCode != Constants.GREENCARD_CMTPL_CONTRACT_OPERATION {
                    self.contractManager.signContract(withOperatorPhone: operatorPhone, operatorAsan: asanID, insuredPhone: insuredPhone, insuredEmail: email)
                } else {
                    self.contractManager.greenCardContract(withInsuredPhone: insuredPhone, andInsuredEmail: email)
                }
            }
        }
    }
    
    
    // email validation
    func isValid(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    @objc func cancelOperation() {
        contractManager.cancelContractOperation()
    }
    
    //Create custom table for final details
    fileprivate func customizeFinalData() {
        var cellDataCustomizer: CellDataCustomizer!
        
        if let vehicleInfo = ApplicationManager.getVehicleInfo() {
            if vehicleInfo.certificateNumber != ""  {
                cellDataCustomizer = CellDataCustomizer(label: Constants.certificateNumberLbl, data: vehicleInfo.certificateNumber)
                finalDetailsArray.append(cellDataCustomizer)
            }
            if vehicleInfo.carNumber != "" {
                cellDataCustomizer = CellDataCustomizer(label: Constants.carNumberLbl, data: vehicleInfo.carNumber)
                finalDetailsArray.append(cellDataCustomizer)
            }
            if vehicleInfo.make != "" && vehicleInfo.model != "" {
                cellDataCustomizer = CellDataCustomizer(label: "NƏQLİYYAT VASİTƏSİ", data: "\(vehicleInfo.make) \(vehicleInfo.model)")
                finalDetailsArray.append(cellDataCustomizer)
            }
        }
        
        if let naturalPerson = ApplicationManager.getNaturalPerson() {
            if naturalPerson.fullName != "" {
                cellDataCustomizer = CellDataCustomizer(label: "SIĞORTALI", data: naturalPerson.fullName)
                finalDetailsArray.append(cellDataCustomizer)
            }
        }
        
        if let juridicalPerson = ApplicationManager.getJuridicalPerson() {
            if juridicalPerson.fullName != "" {
                cellDataCustomizer = CellDataCustomizer(label: "SIĞORTALI", data: juridicalPerson.fullName)
                finalDetailsArray.append(cellDataCustomizer)
            }
        }
    }
    
    func openPDF(forContractNumber number: String) {
        contractManager.getContractDocument(withContractNumber: number) { (result) in
            guard
                var documentsURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last,
                let convertedData = Data(base64Encoded: result)
                else {
                    self.fireErrorAlert(withMessage: "Can't convert base64 to PDF!")
                    return
            }
            
            //name your file however you prefer
            documentsURL.appendPathComponent("\(number).pdf")
            
            do {
                try convertedData.write(to: documentsURL)
                
                self.docController = UIDocumentInteractionController(url: documentsURL)
                let url = NSURL(string:"com.adobe.adobe-reader://");
                if UIApplication.shared.canOpenURL(url! as URL) {
                    self.docController?.presentOpenInMenu(from: .zero, in: self.view, animated: true)
                } else {
                    self.fireErrorAlert(withMessage: "PDF faylı açmaq üçün Adobe Acrobat programı yukləmək lazımdır!")
                }
            } catch {
                self.fireErrorAlert(withMessage: "Error in writing file to local storage!")
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return ApplicationManager.setMaxLength(forTextField: textField, withRange: range, withString: string, maxLength: 12)
    }

}
