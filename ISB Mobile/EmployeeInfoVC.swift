//
//  EmployeeInfoVC.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/20/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD

class EmployeeInfoVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let tableView = UITableView()
    let mainView = UIView()
    var cancelBtn: UIButton!
    var confirmBtn: UIButton!
    
    var employeeInfoArray = [CellDataCustomizer]()
    private var employeeManager: EmployeeManager!

    override func viewDidLoad() {
        super.viewDidLoad()
        employeeManager = EmployeeManager(view: self)
        
        //create main UI
        createUI()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return employeeInfoArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 100
        } else {
            return 70
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.employeeInfoCell) as! EmployeeInfoCell
        let data = employeeInfoArray[indexPath.row]
        cell.config(withCellCustomizer: data)
        return cell
    }
    
    @objc func mismatchAction() {
        employeeManager.mismatchPersonInfo()
    }
    
    @objc func confirmAction() {
        employeeManager.confirmPersonInfo()
    }
    
}
