//
//  ViewController.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/19/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD

class LoginVC: UIViewController, UITextFieldDelegate {

    let headerView = UIView()
    let iconImage = UIImageView()
    let loginFormView = UIView()
    var loginBtn: UIButton!
    var phoneNumberTxtField: CustomTextField!
    var userIDTxtField: CustomTextField!
    
    private var loginManager: LoginManager!
    
    // MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        loginManager = LoginManager(view: self)
        
        //create main UI
        createUI()
        loginManager.authorization()
    }
    
    // MARK: - Login
    @objc func login() {
        switch checkInputs() {
        case .valid:
            loginManager.login()
        case .failure(let errorMessage):
            fireErrorAlert(withMessage: errorMessage)
        }
    }
    
    // MARK: - Check inputs
    private func checkInputs() -> Validation {
        if phoneNumberTxtField.text == "" {
            return .failure("Telefon nömrəsi boş ola bilməz!")
        } else {
            
            //TODO: Check number's length and other stuff...
            
        }
        if userIDTxtField.text == "" {
            return .failure("İstifadəçinin adı boş ola bilməz!")
        }
        return .valid
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}

