//
//  CellDataCustomizer.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/20/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import Foundation

struct CellDataCustomizer {
    
    var label: String
    var data: Any
    
}
