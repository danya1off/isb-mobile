//
//  VehicleInfo.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/21/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import SwiftyJSON

class VehicleInfo: NSObject, NSCoding {
    
    private var _bodyNumber = ""
    private var _chassisNumber = ""
    private var _engineNumber = ""
    private var _carNumber = ""
    private var _certificateNumber = ""
    private var _yearOfManufacture = 0
    private var _vehicleColor = ""
    private var _make = ""
    private var _model = ""
    private var _vehicleType = ""
    private var _vehicleTypeID = 0
    private var _engineCapacity = 0
    private var _maxPermissibleMass = 0.0
    private var _passengerSeatCount = 0
    private var _horsePower = 0
    
    init(withData data: JSON) {
        
        _bodyNumber = data["bodyNumber"].string ?? ""
        _chassisNumber = data["chassisNumber"].string ?? ""
        _engineNumber = data["engineNumber"].string ?? ""
        _carNumber = data["carNumber"].string ?? ""
        _certificateNumber = data["certificateNumber"].string ?? ""
        _yearOfManufacture = data["yearOfManufacture"].int ?? 0
        _vehicleColor = data["vehicleColor"].string ?? ""
        _make = data["make"].string ?? ""
        _model = data["model"].string ?? ""
        _vehicleType = data["vehicleType"].string ?? ""
        _vehicleTypeID = data["vehicleTypeId"].int ?? 0
        _engineCapacity = data["engineCapacity"].int ?? 0
        _maxPermissibleMass = data["maxPermissibleMass"].double ?? 0.0
        _passengerSeatCount = data["passengerSeatCount"].int ?? 0
        _horsePower = data["horsePower"].int ?? 0
        
    }
    
    override init() {}
    
    required init?(coder aDecoder: NSCoder) {
        if let data = aDecoder.decodeObject(forKey: Keys.bodyNumber) as? String {
            _bodyNumber = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.chassisNumber) as? String {
            _chassisNumber = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.engineNumber) as? String {
            _engineNumber = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.carNumber) as? String {
            _carNumber = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.certificateNumber) as? String {
            _certificateNumber = data
        }
        
        _yearOfManufacture = aDecoder.decodeInteger(forKey: Keys.yearOfManufacture)
        
        if let data = aDecoder.decodeObject(forKey: Keys.vehicleColor) as? String {
            _vehicleColor = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.make) as? String {
            _make = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.model) as? String {
            _model = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.vehicleType) as? String {
            _vehicleType = data
        }
        _vehicleTypeID = aDecoder.decodeInteger(forKey: Keys.vehicleTypeID)
        _engineCapacity = aDecoder.decodeInteger(forKey: Keys.engineCapacity)
        _maxPermissibleMass = aDecoder.decodeDouble(forKey: Keys.maxPermissibleMass)
        _passengerSeatCount = aDecoder.decodeInteger(forKey: Keys.passengerSeatCount)
        _horsePower = aDecoder.decodeInteger(forKey: Keys.horsePower)
        
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(_bodyNumber, forKey: Keys.bodyNumber)
        aCoder.encode(_chassisNumber, forKey: Keys.chassisNumber)
        aCoder.encode(_engineNumber, forKey: Keys.engineNumber)
        aCoder.encode(_carNumber, forKey: Keys.carNumber)
        aCoder.encode(_certificateNumber, forKey: Keys.certificateNumber)
        aCoder.encode(_yearOfManufacture, forKey: Keys.yearOfManufacture)
        aCoder.encode(_vehicleColor, forKey: Keys.vehicleColor)
        aCoder.encode(_make, forKey: Keys.make)
        aCoder.encode(_model, forKey: Keys.model)
        aCoder.encode(_vehicleType, forKey: Keys.vehicleType)
        aCoder.encode(_vehicleTypeID, forKey: Keys.vehicleTypeID)
        aCoder.encode(_engineCapacity, forKey: Keys.engineCapacity)
        aCoder.encode(_maxPermissibleMass, forKey: Keys.maxPermissibleMass)
        aCoder.encode(_passengerSeatCount, forKey: Keys.passengerSeatCount)
        aCoder.encode(_horsePower, forKey: Keys.horsePower)
    }
    
    class var filePath: String {
        let manager = FileManager.default
        let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first
        return url!.appendingPathComponent(Constants.carInfoPath).path
    }
    
    struct Keys {
        static let bodyNumber = "bodyNumber"
        static let chassisNumber = "chassisNumber"
        static let engineNumber = "engineNumber"
        static let carNumber = "carNumber"
        static let certificateNumber = "certificateNumber"
        static let yearOfManufacture = "yearOfManufacture"
        static let vehicleColor = "vehicleColor"
        static let make = "make"
        static let model = "model"
        static let vehicleType = "vehicleType"
        static let vehicleTypeID = "vehicleTypeID"
        static let engineCapacity = "engineCapacity"
        static let maxPermissibleMass = "maxPermissibleMass"
        static let passengerSeatCount = "passengerSeatCount"
        static let horsePower = "horsePower"
    }
    
    
    var vehicleInfoArray: [CellDataCustomizer] {
        var arr = [CellDataCustomizer]()
        
        var customizer: CellDataCustomizer
        
        if _carNumber != "" {
            customizer = CellDataCustomizer(label: Constants.carNumberLbl, data: _carNumber)
            arr.append(customizer)
        }
        if _certificateNumber != "" {
            customizer = CellDataCustomizer(label: Constants.certificateNumberLbl, data: _certificateNumber)
            arr.append(customizer)
        }
        if _bodyNumber != "" {
            customizer = CellDataCustomizer(label: Constants.bodyNumberLbl, data: _bodyNumber)
            arr.append(customizer)
        }
        if _engineNumber != "" {
            customizer = CellDataCustomizer(label: Constants.engineNumberLbl, data: _engineNumber)
            arr.append(customizer)
        }
        if _chassisNumber != "" {
            customizer = CellDataCustomizer(label: Constants.chassisNumberLbl, data: _chassisNumber)
            arr.append(customizer)
        }
        if _vehicleType != "" {
            customizer = CellDataCustomizer(label: Constants.vehicleTypeLbl, data: _vehicleType)
            arr.append(customizer)
        }
        if _engineCapacity != 0 {
            customizer = CellDataCustomizer(label: Constants.engineCapacityLbl, data: _engineCapacity)
            arr.append(customizer)
        }
        if _passengerSeatCount != 0 {
            customizer = CellDataCustomizer(label: Constants.passengerSeatCountLbl, data: _passengerSeatCount)
            arr.append(customizer)
        }
        if _maxPermissibleMass != 0.0 {
            customizer = CellDataCustomizer(label: Constants.maxPermissibleMassLbl, data: _maxPermissibleMass)
            arr.append(customizer)
        }
        if _make != "" {
            customizer = CellDataCustomizer(label: Constants.makeLbl, data: _make)
            arr.append(customizer)
        }
        if _model != "" {
            customizer = CellDataCustomizer(label: Constants.modelLbl, data: _model)
            arr.append(customizer)
        }
        if _vehicleColor != "" {
            customizer = CellDataCustomizer(label: Constants.vehicleColorLbl, data: _vehicleColor)
            arr.append(customizer)
        }
        if _yearOfManufacture != 0 {
            customizer = CellDataCustomizer(label: Constants.yearOfManufacturerLbl, data: _yearOfManufacture)
            arr.append(customizer)
        }
        if _horsePower != 0 {
            customizer = CellDataCustomizer(label: Constants.horsePowerLbl, data: _horsePower)
            arr.append(customizer)
        }
        
        return arr
    }
    
    var bodyNumber: String {
        get {
            return _bodyNumber
        }
        set {
            _bodyNumber = newValue
        }
    }
    var chassisNumber: String {
        get {
            return _chassisNumber
        }
        set {
            _chassisNumber = newValue
        }
    }
    var engineNumber: String {
        get {
            return _engineNumber
        }
        set {
            _engineNumber = newValue
        }
    }
    var carNumber: String {
        get {
            return _carNumber
        }
        set {
            _carNumber = newValue
        }
    }
    var certificateNumber: String {
        get {
            return _certificateNumber
        }
        set {
            _certificateNumber = newValue
        }
    }
    var yearOfManufacture: Int {
        get {
            return _yearOfManufacture
        }
        set {
            _yearOfManufacture = newValue
        }
    }
    var vehicleColor: String {
        get {
            return _vehicleColor
        }
        set {
            _vehicleColor = newValue
        }
    }
    var make: String {
        get {
            return _make
        }
        set {
            _make = newValue
        }
    }
    var model: String {
        get {
            return _model
        }
        set {
            _model = newValue
        }
    }
    var vehicleType: String {
        get {
            return _vehicleType
        }
        set {
            _vehicleType = newValue
        }
    }
    var vehicleTypeID: Int {
        get {
            return _vehicleTypeID
        }
        set {
            _vehicleTypeID = newValue
        }
    }
    var engineCapacity: Int {
        get {
            return _engineCapacity
        }
        set {
            _engineCapacity = newValue
        }
    }
    var maxPermissibleMass: Double {
        get {
            return _maxPermissibleMass
        }
        set {
            _maxPermissibleMass = newValue
        }
    }
    var passengerSeatCount: Int {
        get {
            return _passengerSeatCount
        }
        set {
            _passengerSeatCount = newValue
        }
    }
    
    var horsePower: Int {
        get {
            return _horsePower
        }
        set {
            _horsePower = newValue
        }
    }
    
}
