//
//  ContractDetailsVC.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/25/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD

class ContractDetailsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    let tableView = UITableView()
    let detailsBgView = UIView()
    let contractNumberLbl = UILabel()
    let effectiveDateLbl = UILabel()
    let expiryDateLbl = UILabel()
    let statusLbl = UILabel()
    
    var contract = Contract()
    var contractDetailsArray = [CellDataCustomizer]()
    var fromCrashState = false
    
    private var docController: UIDocumentInteractionController?
    private var contractManager: ContractManager!
    
    //MARK: - viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        contractManager = ContractManager(view: self)
        
        //create main UI
        createUI()
        populateData()
        
    }
    
    //MARK: -viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        userDefaults.set(String(describing: self.classForCoder) , forKey: Constants.operationName)
    }
    
    
    //MARK: - Table view functions
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contractDetailsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.contractDetailsCell) as! ContractDetailsCell
        cell.config(withCellCustomizer: contractDetailsArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 100
        } else {
            return 70
        }
    }
    
    //MARK: - Open PDF
    @objc func openPDF() {
        guard contract.contractNumber != ""  else {
            self.fireErrorAlert(withMessage: "Müqavilə nömrə tapılmadı!")
            return
        }
        contractManager.getContractDocument(withContractNumber: contract.contractNumber) { (result) in
            guard
                var documentsURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last,
                let convertedData = Data(base64Encoded: result)
                else {
                    self.fireErrorAlert(withMessage: "Can't convert base64 to PDF!")
                    return
            }
            //name your file however you prefer
            documentsURL.appendPathComponent("\(self.contract.contractNumber).pdf")
            do {
                try convertedData.write(to: documentsURL)
                
                self.docController = UIDocumentInteractionController(url: documentsURL)
                let url = NSURL(string:"com.adobe.adobe-reader://");
                if UIApplication.shared.canOpenURL(url! as URL) {
                    self.docController?.presentOpenInMenu(from: .zero, in: self.view, animated: true)
                } else {
                    self.fireErrorAlert(withMessage: "PDF faylı açmaq üçün Adobe Acrobat programı yukləmək lazımdır!")
                }
            } catch {
                self.fireErrorAlert(withMessage: "Error in writing file to local storage!")
            }
        }
    }
    
    //MARK: - Close
    @objc func close() {
        ApplicationManager.redirectToMainMenu()
    }
    
    //MARK: - Populate Data
    func populateData() {
        let date = Date()
        contractNumberLbl.text = contract.contractNumber
        effectiveDateLbl.text = date.localFormat(dateString: contract.effectiveDate)
        expiryDateLbl.text = date.localFormat(dateString: contract.expiryDate)
        statusLbl.text = contract.status.name
    }

}
