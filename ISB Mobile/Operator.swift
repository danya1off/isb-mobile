//
//  Operator.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/31/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation

class Operator: NSObject, NSCoding {
    
    private var _fullName: String = ""
    private var _pinCode: String = ""
    
    override init() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        if let data = aDecoder.decodeObject(forKey: Keys.fullName) as? String {
            _fullName = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.pinCode) as? String {
            _pinCode = data
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(_fullName, forKey: Keys.fullName)
        aCoder.encode(_pinCode, forKey: Keys.pinCode)
    }
    
    struct Keys {
        static var fullName = "fullName"
        static var pinCode = "pinCode"
    }
    
    var fullName: String {
        get {
            return _fullName
        }
        set {
            _fullName = newValue
        }
    }
    
    var pinCode: String {
        get {
            return _pinCode
        }
        set {
            _pinCode = newValue
        }
    }
    
}
