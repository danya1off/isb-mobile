//
//  TerminationCommitVC+Ext.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/25/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

extension TerminationCommitVC {
    
    func createUI() {
        
        view.backgroundColor = UIColor.white
        CustomProgressHUD.customize()
        self.hideKeyboardWhenTappedAround()
        
        createView()
        
        datePicker.minimumDate = Date()
        datePicker.datePickerMode = .date
        datePicker.locale = Locale(identifier: "AZE")
        datePicker.setValue(UIColor(red:0.44, green:0.55, blue:0.57, alpha:1.0), forKeyPath: "textColor")
        datePicker.setValue(false, forKeyPath: "highlightsToday")
        
        participantsPicker.dataSource = self
        participantsPicker.delegate = self
        
        if insuredPersonName != "" {
            insuredPersonLbl.text = insuredPersonName
        }
        if carNumber != "" {
            carNumberLbl.text = "\(carNumber.substring(to: 2))-\(carNumber.substring(with: 2..<4))-\(carNumber.substring(from: 4))"
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }

    private func createView() {
        
        let bottomView: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.white
            view.layer.cornerRadius = 5
            view.layer.shadowOpacity = 0.2
            view.layer.shadowRadius = 5
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOffset = CGSize.zero
            return view
        }()
        view.addSubview(bottomView)
        
        let topView: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.white
            view.layer.cornerRadius = 5
            view.layer.masksToBounds = true
            return view
        }()
        bottomView.addSubview(topView)
        
        let headerView: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.mainAppColor
            return view
        }()
        topView.addSubview(headerView)
        
        let headerLbl = UILabel.createLabel(withText: "XITAM OLUNMA", andSize: 15)
        headerLbl.textColor = UIColor.white
        headerView.addSubview(headerLbl)
        
        insuredPersonLbl = UILabel.createLabel(withText: "Label", andSize: 17)
        insuredPersonLbl.textColor = UIColor.mainTextColor
        insuredPersonLbl.textAlignment = .center
        topView.addSubview(insuredPersonLbl)
        
        carNumberLbl = UILabel.createLabel(withText: "Label", andSize: 15)
        carNumberLbl.textColor = UIColor.mainTextColor 
        carNumberLbl.textAlignment = .center
        topView.addSubview(carNumberLbl)
        
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        topView.addSubview(datePicker)
 
        terminationReasonTxtField = CustomTextField.createCustomTxtField(placeholder: "Səbəb", textSize: 25)
        terminationReasonTxtField.delegate = self
        topView.addSubview(terminationReasonTxtField)
        
        let reasonLbl = UILabel.createLabel(withText: "XITAM OLUNMA SƏBƏBİ", andSize: 12)
        topView.addSubview(reasonLbl)
        
        let terminationAmountLbl: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.font = UIFont(name: Constants.helveticaBold, size: 25)
            label.textColor = UIColor.mainTextColor
            label.text = String(describing: terminationReturningAmount!)
            return label
        }()
        topView.addSubview(terminationAmountLbl)
        
        let amountLbl = UILabel.createLabel(withText: "GERİ QAYTARILAN MƏBLƏĞ", andSize: 12)
        topView.addSubview(amountLbl)
        
        participantInfoLbl.translatesAutoresizingMaskIntoConstraints = false
        participantInfoLbl.font = UIFont(name: Constants.helveticaBold, size: 25)
        participantInfoLbl.textColor = UIColor.mainTextColor
        
        let participantLbl = UILabel.createLabel(withText: "İştirakçı", andSize: 12)
        
        let cancelButton = UIButton.createCustomButton(text: "IMTINA", textSize: 15)
        cancelButton.addTarget(self, action: #selector(TerminationCommitVC.cancelOperation), for: .touchUpInside)
        cancelButton.layer.cornerRadius = 0
        topView.addSubview(cancelButton)
        
        let terminateButton = UIButton.createCustomButton(text: "XITAM", textSize: 15)
        terminateButton.addTarget(self, action: #selector(TerminationCommitVC.terminateAction), for: .touchUpInside)
        terminateButton.layer.cornerRadius = 0
        topView.addSubview(terminateButton)
        
        let separator: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.white
            return view
        }()
        cancelButton.addSubview(separator)
        
        
        bottomView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        bottomView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        bottomView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.85).isActive = true
        bottomView.heightAnchor.constraint(greaterThanOrEqualToConstant: 200).isActive = true
        
        topView.centerYAnchor.constraint(equalTo: bottomView.centerYAnchor).isActive = true
        topView.centerXAnchor.constraint(equalTo: bottomView.centerXAnchor).isActive = true
        topView.widthAnchor.constraint(equalTo: bottomView.widthAnchor).isActive = true
        topView.heightAnchor.constraint(equalTo: bottomView.heightAnchor).isActive = true
        
        headerView.topAnchor.constraint(equalTo: topView.topAnchor).isActive = true
        headerView.centerXAnchor.constraint(equalTo: topView.centerXAnchor).isActive = true
        headerView.widthAnchor.constraint(equalTo: topView.widthAnchor).isActive = true
        headerView.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        headerLbl.centerXAnchor.constraint(equalTo: headerView.centerXAnchor).isActive = true
        headerLbl.centerYAnchor.constraint(equalTo: headerView.centerYAnchor).isActive = true
        
        insuredPersonLbl.topAnchor.constraint(equalTo: headerView.bottomAnchor, constant: 20).isActive = true
        insuredPersonLbl.leftAnchor.constraint(equalTo: topView.leftAnchor, constant: 20).isActive = true
        insuredPersonLbl.rightAnchor.constraint(equalTo: topView.rightAnchor, constant: -20).isActive = true
        
        carNumberLbl.topAnchor.constraint(equalTo: insuredPersonLbl.bottomAnchor, constant: 8).isActive = true
        carNumberLbl.leadingAnchor.constraint(equalTo: insuredPersonLbl.leadingAnchor).isActive = true
        carNumberLbl.trailingAnchor.constraint(equalTo: insuredPersonLbl.trailingAnchor).isActive = true
        
        datePicker.leadingAnchor.constraint(equalTo: carNumberLbl.leadingAnchor).isActive = true
        datePicker.trailingAnchor.constraint(equalTo: carNumberLbl.trailingAnchor).isActive = true
        datePicker.topAnchor.constraint(equalTo: carNumberLbl.bottomAnchor, constant: 25).isActive = true
        datePicker.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        terminationReasonTxtField.heightAnchor.constraint(equalToConstant: 60).isActive = true
        terminationReasonTxtField.leadingAnchor.constraint(equalTo: datePicker.leadingAnchor).isActive = true
        terminationReasonTxtField.trailingAnchor.constraint(equalTo: datePicker.trailingAnchor).isActive = true
        terminationReasonTxtField.topAnchor.constraint(equalTo: datePicker.bottomAnchor, constant: 25).isActive = true
        reasonLbl.leadingAnchor.constraint(equalTo: terminationReasonTxtField.leadingAnchor).isActive = true
        reasonLbl.topAnchor.constraint(equalTo: terminationReasonTxtField.topAnchor).isActive = true
        
        amountLbl.leadingAnchor.constraint(equalTo: terminationReasonTxtField.leadingAnchor).isActive = true
        amountLbl.topAnchor.constraint(equalTo: terminationReasonTxtField.bottomAnchor, constant: 25).isActive = true
        
        terminationAmountLbl.leadingAnchor.constraint(equalTo: terminationReasonTxtField.leadingAnchor).isActive = true
        terminationAmountLbl.trailingAnchor.constraint(equalTo: terminationReasonTxtField.trailingAnchor).isActive = true
        terminationAmountLbl.topAnchor.constraint(equalTo: amountLbl.bottomAnchor, constant: 5).isActive = true
        
        if let data = participants {
            if data.count == 1 {
                topView.addSubview(participantInfoLbl)
                topView.addSubview(participantLbl)
                
                participantLbl.leadingAnchor.constraint(equalTo: terminationAmountLbl.leadingAnchor).isActive = true
                participantLbl.topAnchor.constraint(equalTo: terminationAmountLbl.bottomAnchor, constant: 15).isActive = true
                
                participantInfoLbl.leadingAnchor.constraint(equalTo: participantLbl.leadingAnchor).isActive = true
                participantInfoLbl.trailingAnchor.constraint(equalTo: terminationAmountLbl.trailingAnchor).isActive = true
                participantInfoLbl.topAnchor.constraint(equalTo: participantLbl.bottomAnchor, constant: 5).isActive = true
                
            } else if data.count > 1 {
                participantsPicker.translatesAutoresizingMaskIntoConstraints = false
                participantsPicker.backgroundColor = UIColor(red:0.94, green:0.95, blue:0.97, alpha:1.0)
                participantsPicker.layer.cornerRadius = 5
                topView.addSubview(participantsPicker)
                
                participantsPicker.heightAnchor.constraint(equalTo: datePicker.heightAnchor).isActive = true
                participantsPicker.centerXAnchor.constraint(equalTo: topView.centerXAnchor).isActive = true
                participantsPicker.topAnchor.constraint(equalTo: terminationAmountLbl.bottomAnchor, constant: 15).isActive = true
                participantsPicker.widthAnchor.constraint(equalTo: datePicker.widthAnchor).isActive = true
            }
        }
        
        cancelButton.leadingAnchor.constraint(equalTo: topView.leadingAnchor).isActive = true
        cancelButton.bottomAnchor.constraint(equalTo: topView.bottomAnchor).isActive = true
        cancelButton.heightAnchor.constraint(equalTo: headerView.heightAnchor).isActive = true
        cancelButton.widthAnchor.constraint(equalTo: topView.widthAnchor, multiplier: 0.5).isActive = true
        if let data = participants {
            if data.count == 1 {
                cancelButton.topAnchor.constraint(equalTo: participantInfoLbl.bottomAnchor, constant: 25).isActive = true
            } else if data.count > 1 {
                cancelButton.topAnchor.constraint(equalTo: participantsPicker.bottomAnchor, constant: 25).isActive = true
            }
        } else {
            cancelButton.topAnchor.constraint(equalTo: terminationAmountLbl.bottomAnchor, constant: 25).isActive = true
        }
        
        terminateButton.trailingAnchor.constraint(equalTo: topView.trailingAnchor).isActive = true
        terminateButton.bottomAnchor.constraint(equalTo: topView.bottomAnchor).isActive = true
        terminateButton.heightAnchor.constraint(equalTo: cancelButton.heightAnchor).isActive = true
        terminateButton.widthAnchor.constraint(equalTo: cancelButton.widthAnchor).isActive = true
        
        separator.heightAnchor.constraint(equalTo: cancelButton.heightAnchor, multiplier: 0.6).isActive = true
        separator.widthAnchor.constraint(equalToConstant: 1).isActive = true
        separator.centerYAnchor.constraint(equalTo: cancelButton.centerYAnchor).isActive = true
        separator.rightAnchor.constraint(equalTo: cancelButton.rightAnchor).isActive = true
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height/2
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height/2
            }
        }
    }
    
}
