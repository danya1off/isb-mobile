//
//  Result.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/20/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import SwiftyJSON

struct AppResult<T> {
    
    private var _resultCode: String?
    private var _errorMessage: String?
    private var _data: T?
    private var _isSuccess: Bool!
    
    init() {
        
    }
    
    init(isSuccess: Bool, resultCode: String) {
        _isSuccess = isSuccess
        _resultCode = resultCode
    }
    
    init(isSuccess: Bool, data: T) {
        _isSuccess = isSuccess
        _data = data
    }
    
    init(isSuccess: Bool, message: String) {
        _isSuccess = isSuccess
        _errorMessage = message
    }
    
    init(isSuccess: Bool, message: String, data: T) {
        _isSuccess = isSuccess
        _errorMessage = message
        _data = data
    }
    
    init(isSuccess: Bool, resultCode: String, message: String, data: T) {
        _isSuccess = isSuccess
        _resultCode = resultCode
        _errorMessage = message
        _data = data
    }
    
    static func error(message: String) -> AppResult<T> {
        return AppResult(isSuccess: false, message: message)
    }
    
    static func success(data: T) -> AppResult<T> {
        return AppResult(isSuccess: true, data: data)
    }
    
    init(errorWithData data: JSON) {
        if let code = data["code"].string {
            _resultCode = code
        }
        if let errorMsg = data["error"].string {
            _errorMessage = errorMsg
        } else {
            _errorMessage = Constants.error
        }
        _isSuccess = false
    }
    
    var resultCode: String? {
        get {
            return _resultCode
        }
        set {
            _resultCode = newValue
        }
    }
    
    var errorMessage: String? {
        get {
            return _errorMessage
        }
        set {
            _errorMessage = newValue
        }
    }
    
    var data: T? {
        get {
            return _data
        }
        set {
            _data = newValue
        }
    }
    
    var isSuccess: Bool {
        get {
            return _isSuccess
        }
        set {
            _isSuccess = newValue
        }
    }
    
}
