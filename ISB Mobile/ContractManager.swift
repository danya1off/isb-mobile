//
//  ContractManager.swift
//  ISB Mobile
//
//  Created by  Simberg on 11/7/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit
import SVProgressHUD
import SCLAlertView

class ContractManager {
    
    private var vc: UIViewController!
    
    init(view: UIViewController) {
        self.vc = view
    }
    
    func getContract(withContractNumber number: String) {
        
        Services.getContract(byContractNumber: number) { (result) in
            
            SVProgressHUD.dismiss()
            
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            
            guard let contract = result.data else {
                self.vc.fireErrorAlert(withMessage: Constants.dataError)
                return
            }
            
            let detailsView = ContractDetailsVC()
            detailsView.contract = contract
            detailsView.contractDetailsArray = contract.contractDetailsArray
            self.vc.navigationController?.pushViewController(detailsView, animated: true)
            
        }
        
    }
    
    func getContractDocument(withContractNumber number: String, completion: @escaping(String) -> Void) {
        SVProgressHUD.show(withStatus: "Gözləyin...")
        Services.getContractDocument(byContractNumber: number) { (result) in
            
            SVProgressHUD.dismiss()
            
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            
            guard let base64String = result.data else {
                self.vc.fireErrorAlert(withMessage: Constants.dataError)
                return
            }
            completion(base64String)
        }
        
    }
    
    func cancelContractOperation() {
        SVProgressHUD.show(withStatus: "Gözləyin...")
        Services.cancelContractOperation { (result) in
            SVProgressHUD.dismiss()
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                ApplicationManager.redirectToMainMenu()
                return
            }
            ApplicationManager.redirectToMainMenu()
        }
    }
    
    func signContract(withOperatorPhone operatorPhone: String, operatorAsan asan: String, insuredPhone insPhone: String, insuredEmail email: String) {
        SVProgressHUD.show(withStatus: "Gözləyin...")
        Services.signContract(withOperatorPhone: operatorPhone, operatorAsan: asan, insuredPhone: insPhone, insuredEmail: email, completion: { (result) in
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            
            guard let transaction = result.data else {
                self.vc.fireErrorAlert(withMessage: Constants.dataError)
                return
            }
            
            if let verificationCode = transaction.verificationCode {
                // Show verification code in alert view (dismiss automatically)
                let verificationAlert = self.vc.alert(withTitle: "Verifikasiya", message: "Sizin verifikasiya kodunuz: \(verificationCode)", showButton: false)
//                self.vc.present(verificationAlert, animated: true, completion: nil)
                self.vc.present(verificationAlert, animated: true, completion: {
                    SVProgressHUD.dismiss()
                })
                
                Services.signingResult(completion: { (res) in
                    guard res.isSuccess else {
                        self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                        return
                    }
                    
                    guard let data = res.data else {
                        self.vc.fireErrorAlert(withMessage: Constants.dataError)
                        return
                    }
                    
                    verificationAlert.dismiss(animated: true, completion: {
                        self.showSuccessPopup(contractNumber: data)
                    })
                    
                })
            } else {
                self.vc.fireErrorAlert(withMessage: "Verifikasiya kodu tapılmadı!")
            }
        })
    }
    
    func greenCardContract(withInsuredPhone phone: String, andInsuredEmail email: String) {
        SVProgressHUD.show(withStatus: "Gözləyin...")
        Services.greenCardContract(withInsuredPhone: phone, andInsuredEmail: email, completion: { (result) in
            SVProgressHUD.dismiss()
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            
            guard let data = result.data else {
                self.vc.fireErrorAlert(withMessage: Constants.dataError)
                return
            }
            
            self.showSuccessPopup(contractNumber: data)
            
        })
    }
    
    func contractPrice(withPeriod period: String) {
        SVProgressHUD.show(withStatus: "Gözləyin...")
        Services.getContractPrice(withPeriod: period.substring(to: 1), completion: { (result) in
            
            SVProgressHUD.dismiss()
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            guard let data = result.data else {
                self.vc.fireErrorAlert(withMessage: Constants.dataError)
                return
            }
            
            ApplicationManager.saveContractPriceInfo(contractPriceInfo: data)
            let finalDetailsVC = FinalDetailsVC()
            finalDetailsVC.finalDetailsArray = data.contractPriseArray
            finalDetailsVC.isPriceFromLastContract = data.isPriceFromLastContract
            self.vc.navigationController?.pushViewController(finalDetailsVC, animated: true)
            
        })
    }
    
    func contractPrice() {
        SVProgressHUD.show(withStatus: "Gözləyin...")
        Services.getContractPrice(completion: { (result) in
            SVProgressHUD.dismiss()
            guard result.isSuccess else {
                self.vc.fireErrorAlert(withMessage: result.errorMessage!)
                return
            }
            guard let data = result.data else {
                self.vc.fireErrorAlert(withMessage: Constants.dataError)
                return
            }
            let finalDetailsVC = FinalDetailsVC()
            finalDetailsVC.finalDetailsArray = data.contractPriseArray
            finalDetailsVC.isPriceFromLastContract = data.isPriceFromLastContract
            self.vc.navigationController?.pushViewController(finalDetailsVC, animated: true)
        })
    }
    
    private func showSuccessPopup(contractNumber: String) {
        let appearance = SCLAlertView.SCLAppearance(
            kTitleFont: UIFont(name: "HelveticaNeue", size: 18)!,
            kTextFont: UIFont(name: "HelveticaNeue", size: 14)!,
            kButtonFont: UIFont(name: "HelveticaNeue-Medium", size: 14)!,
            showCloseButton: false
        )
        
        let alert = SCLAlertView(appearance: appearance)
        alert.addButton("Sənədə baxış") {
            (self.vc as! FinalDetailsVC).openPDF(forContractNumber: contractNumber)
            ApplicationManager.redirectToMainMenu()
        }
        alert.addButton("Menuya qayıt") {
            ApplicationManager.redirectToMainMenu()
        }
        alert.showSuccess("Müqavilə üğurla tamamlandl!", subTitle: "\(contractNumber) nömrəli sənədə baxıb çap edə bilərsiniz!")
    }
    
}
