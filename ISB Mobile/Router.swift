//
//  Router.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/18/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import Alamofire

enum Router : URLRequestConvertible {
    
    static let baseUrl = "https://\(Constants.URL):8884/mobile-proxy/"
    
    // requests
    case startRegistration
    case authParams([String: String])
    case authResult(String)
    case mismatchPersonInfo
    case registerApp
    case endRegistration
    case startAuthentication
    case authorizations(String, String)
    case endAuthentication
    case startContractOperation([String: String])
    case operationAllowance
    case getVehicleInfo(String, String)
    case mismatchVehicleInfo
    case getNaturalPersonInfo(String)
    case getJuridicalPersonInfo(String)
    case getNonResidentPersonInfo(String, String)
    case getContractPrise(String)
    case signContract([String: String])
    case signingResult
    case cancelContractOperation
    case searchMyContracts
    case searchAuthorizedContracts([String: String])
    case getContract(String)
    case getContractDocument(String)
    case getTerminationContract(String, String)
    case terminateContract([String: String])
    case saveNonResidentPersonInfo([String: String])
    case saveNonMiaVehicleInfo([String: String])
    case saveBlank([String: String])
    case greenCardContract([String: String])
    case vehicleHorsePower(Int)
    
    // customize request methods
    var method: HTTPMethod {
        
        switch self {
            
        case .startRegistration:
            return .post
        case .authParams:
            return .post
        case .authResult:
            return .get
        case .mismatchPersonInfo:
            return .post
        case .registerApp:
            return .post
        case .endRegistration:
            return .post
        case .startAuthentication:
            return .post
        case .authorizations:
            return .get
        case .endAuthentication:
            return .post
        case .startContractOperation:
            return .post
        case .operationAllowance:
            return .get
        case .getVehicleInfo:
            return .get
        case .mismatchVehicleInfo:
            return .post
        case .getNaturalPersonInfo:
            return .get
        case .getJuridicalPersonInfo:
            return .get
        case .getNonResidentPersonInfo:
            return .get
        case .getContractPrise:
            return .get
        case .signContract:
            return .post
        case .signingResult:
            return .get
        case .cancelContractOperation:
            return .post
        case .searchMyContracts:
            return .post
        case .searchAuthorizedContracts:
            return .post
        case .getContract:
            return .get
        case .getContractDocument:
            return .get
        case .getTerminationContract:
            return .get
        case .terminateContract:
            return .post
        case .saveNonResidentPersonInfo:
            return .post
        case .saveNonMiaVehicleInfo:
            return .post
        case .saveBlank:
            return .post
        case .greenCardContract:
            return .post
        case .vehicleHorsePower:
            return .post
            
        }
        
    }
    
    
    var path : String {
        
        switch self {
            
        case .startRegistration:
            return Constants.startRegistration
        case .authParams:
            return Constants.authParams
        case .authResult(let transactionID):
            return "\(Constants.authResult)/\(transactionID)"
        case .mismatchPersonInfo:
            return Constants.mismatchPersonInfo
        case .registerApp:
            return Constants.registerApp
        case .endRegistration:
            return Constants.endRegistration
        case .startAuthentication:
            return Constants.startAuthentication
        case .authorizations(let phoneNumber, let asanID):
            return "\(Constants.authorizations)?phoneNumber=\(phoneNumber)&asanID=\(asanID)".addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)!
        case .endAuthentication:
            return Constants.endAuthentication
        case .startContractOperation:
            return Constants.startContractOperation
        case .operationAllowance:
            return Constants.operationAllowance
        case .getVehicleInfo(let certNumber, let carNumber):
            return "\(Constants.vehicleInfo)?certificateNumber=\(certNumber)&carNumber=\(carNumber)"
        case .mismatchVehicleInfo:
            return Constants.mismatchVehicleInfo
        case .getNaturalPersonInfo(let idDocument):
            return "\(Constants.naturalPersonInfo)?idDocument=\(idDocument)"
        case .getJuridicalPersonInfo(let tin):
            return "\(Constants.juridicalPersonInfo)?tin=\(tin)"
        case .getNonResidentPersonInfo(let rcNumber, let docType):
            return "\(Constants.nonResidentPersonInfo)?rcNumber=\(rcNumber)&docType=\(docType)"
        case .getContractPrise(let period):
            if period != "" {
                return "\(Constants.contractPrice)?period=\(period)"
            } else {
                return Constants.contractPrice
            }
        case .signContract:
            return Constants.signContract
        case .signingResult:
            return Constants.signingResult
        case .cancelContractOperation:
            return Constants.cancelContractOperation
        case .searchMyContracts:
            return Constants.searchMyContracts
        case .searchAuthorizedContracts:
            return Constants.searchAuthorizedContracts
        case .getContract(let contractNumber):
            return "\(Constants.getContract)/\(contractNumber)"
        case .getContractDocument(let contractNumber):
            return "\(Constants.getContractDoc)/\(contractNumber)"
        case .getTerminationContract(let contractNumber, let operationType):
            return "\(Constants.getTerminationContract)?contractNumber=\(contractNumber)&operationType=\(operationType)"
        case .terminateContract:
            return Constants.terminateContract
        case .saveNonResidentPersonInfo:
            return Constants.saveNonResidentPersonInfo
        case .saveNonMiaVehicleInfo:
            return Constants.nonMiaVehicleInfo
        case .saveBlank:
            return Constants.blank
        case .greenCardContract:
            return Constants.greenCardContract
        case .vehicleHorsePower(let horsePower):
            return "\(Constants.vehicleHorsePower)/\(horsePower)"
            
        }
        
    }
    
    // requests configuration
    func asURLRequest() throws -> URLRequest {
        
        let url = Router.baseUrl + path
        
        var urlRequest = URLRequest(url: URL(string: url)!)
        urlRequest.httpMethod = method.rawValue
        
        switch self {
            
        case .authParams,
             .authResult,
             .mismatchPersonInfo:
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.setValue(getHeaderCredentials().operationID, forHTTPHeaderField: Constants.operationID)
            urlRequest.setValue(getHeaderCredentials().appCode, forHTTPHeaderField: Constants.appCode)
            
        case .registerApp,
             .endRegistration,
             .endAuthentication:
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.setValue(getHeaderCredentials().operationID, forHTTPHeaderField: Constants.operationID)
            urlRequest.setValue(getHeaderCredentials().appCode, forHTTPHeaderField: Constants.appCode)
            
        case .startAuthentication:
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.setValue(getHeaderCredentials().appCode, forHTTPHeaderField: Constants.appCode)
            
        case .searchMyContracts:
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.setValue(getHeaderCredentials().appCode, forHTTPHeaderField: Constants.appCode)
            urlRequest.setValue(getHeaderCredentials().token, forHTTPHeaderField: Constants.token)
            
        case .startContractOperation:
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.setValue(getHeaderCredentials().appCode, forHTTPHeaderField: Constants.appCode)
            urlRequest.setValue(getHeaderCredentials().token, forHTTPHeaderField: Constants.token)
            
        case .getContract,
             .getContractDocument,
             .getTerminationContract:
            urlRequest.setValue(getHeaderCredentials().appCode, forHTTPHeaderField: Constants.appCode)
            urlRequest.setValue(getHeaderCredentials().token, forHTTPHeaderField: Constants.token)
            
        case .authorizations,
             .getVehicleInfo,
             .mismatchVehicleInfo,
             .getNaturalPersonInfo,
             .getJuridicalPersonInfo,
             .getNonResidentPersonInfo,
             .saveNonResidentPersonInfo,
             .getContractPrise,
             .signContract,
             .signingResult,
             .cancelContractOperation,
             .terminateContract,
             .saveNonMiaVehicleInfo,
             .saveBlank,
             .greenCardContract,
             .vehicleHorsePower,
             .searchAuthorizedContracts:
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.setValue(getHeaderCredentials().operationID, forHTTPHeaderField: Constants.operationID)
            urlRequest.setValue(getHeaderCredentials().appCode, forHTTPHeaderField: Constants.appCode)
            urlRequest.setValue(getHeaderCredentials().token, forHTTPHeaderField: Constants.token)
            
        default:
            break
        }
        
        switch self {
            
        case .authParams(let params),
             .startContractOperation(let params),
             .signContract(let params),
             .searchAuthorizedContracts(let params),
             .terminateContract(let params),
             .saveNonResidentPersonInfo(let params),
             .saveNonMiaVehicleInfo(let params),
             .saveBlank(let params),
             .greenCardContract(let params):
            
            urlRequest = try JSONEncoding.default.encode(urlRequest, with: params)
        
        default:
            break
        }
        
        return urlRequest
    }
    
    
    func getHeaderCredentials() -> (operationID: String, appCode: String, token: String) {
        var operationID = ""
        var appCode = ""
        let userDefaults = UserDefaults.standard
        let operID = userDefaults.string(forKey: Constants.operationID)
        let code = userDefaults.string(forKey: Constants.appCode)
        if let id = operID {
            operationID = id
        }
        if let code = code {
            appCode = code
        }
        
        var token = "";
        if let t = userDefaults.string(forKey: Constants.token) {
            token = t
        }
        
        return (operationID, appCode, token)
    }
    
}
