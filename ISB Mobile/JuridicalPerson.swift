//
//  JuridicalPerson.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/21/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import SwiftyJSON

class JuridicalPerson: NSObject, NSCoding {
    
    private var _personType = ""
    private var _fullName = ""
    private var _birthPlace = ""
    private var _birthDate = ""
    private var _tin = ""
    private var _address = ""
    private var _idDocument = ""
    
    init(withData data: JSON) {
        
        _personType = data["personType"].string ?? ""
        _fullName = data["fullName"].string ?? ""
        _birthPlace = data["birthPlace"].string ?? ""
        _birthDate = data["birthDate"].string ?? ""
        _tin = data["tin"].string ?? ""
        _address = data["address"].string ?? ""
        _idDocument = data["idDocument"].string ?? ""
        
    }
    
    override init() {
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        if let data = aDecoder.decodeObject(forKey: Keys.personType) as? String {
            _personType = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.fullName) as? String {
            _fullName = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.birthPlace) as? String {
            _birthPlace = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.birthDate) as? String {
            _birthDate = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.tin) as? String {
            _tin = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.address) as? String {
            _address = data
        }
        if let data = aDecoder.decodeObject(forKey: Keys.idDocument) as? String {
            _idDocument = data
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(_personType, forKey: Keys.personType)
        aCoder.encode(_fullName, forKey: Keys.fullName)
        aCoder.encode(_birthPlace, forKey: Keys.birthPlace)
        aCoder.encode(_birthDate, forKey: Keys.birthDate)
        aCoder.encode(_tin, forKey: Keys.tin)
        aCoder.encode(_address, forKey: Keys.address)
        aCoder.encode(_idDocument, forKey: Keys.idDocument)
    }
    
    struct Keys {
        static var personType = "personType"
        static var fullName = "fullName"
        static var lastName = "lastName"
        static var firstName = "firstName"
        static var patronymic = "patronymic"
        static var birthPlace = "birthPlace"
        static var birthDate = "birthDate"
        static var tin = "tin"
        static var address = "address"
        static var idDocument = "idDocument"
    }
    
    class var filePath: String {
        let manager = FileManager.default
        let url = manager.urls(for: .documentDirectory, in: .userDomainMask).first
        return url!.appendingPathComponent(Constants.juridicalPersonPath).path
    }
    
    var juridicalPersonArray: [CellDataCustomizer] {
        var arr = [CellDataCustomizer]()
        
        var customizer: CellDataCustomizer
        
        if _fullName != "" {
            customizer = CellDataCustomizer(label: "TAM ADI", data: _fullName)
            arr.append(customizer)
        }
        if _birthPlace != "" {
            customizer = CellDataCustomizer(label: "DÖĞULDUĞU YER", data: _birthPlace)
            arr.append(customizer)
        }
        if _birthDate != "" {
            customizer = CellDataCustomizer(label: "DOĞULDUĞU TARİX", data: _birthDate)
            arr.append(customizer)
        }
        if _tin != "" {
            customizer = CellDataCustomizer(label: "VÖEN", data: _tin)
            arr.append(customizer)
        }
        if _address != "" {
            customizer = CellDataCustomizer(label: "ÜNVAN", data: _address)
            arr.append(customizer)
        }
        
        return arr
    }
    
    var personType : String {
        get {
            return _personType
        }
        set {
            _personType = newValue
        }
    }
    
    var fullName : String {
        get {
            return _fullName
        }
        set {
            _fullName = newValue
        }
    }
    
    var birthPlace : String {
        get {
            return _birthPlace
        }
        set {
            _birthPlace = newValue
        }
    }
    
    var birthDate : String {
        get {
            return _birthDate
        }
        set {
            _birthDate = newValue
        }
    }
    
    var tin : String {
        get {
            return _tin
        }
        set {
            _tin = newValue
        }
    }
    
    var address : String {
        get {
            return _address
        }
        set {
            _address = newValue
        }
    }
    
    var idDocument : String {
        get {
            return _idDocument
        }
        set {
            _address = newValue
        }
    }
    
}
