//
//  EmployeeInfo.swift
//  ISB Mobile
//
//  Created by  Simberg on 8/20/17.
//  Copyright © 2017 Jeyhun Danyalov. All rights reserved.
//

import Foundation
import SwiftyJSON

struct EmployeeInfo {
    
    var fullName: String
    var firstName: String
    var lastName: String
    var patronymic: String
    var address: String
    var idDocument: String
    var pinCode: String
    
    init (withData data: JSON) {
        fullName = data["fullName"].stringValue
        firstName = data["firstName"].stringValue
        lastName = data["lastName"].stringValue
        patronymic = data["patronymic"].stringValue
        address = data["address"].stringValue
        idDocument = data["idDocument"].stringValue
        pinCode = data["pinCode"].stringValue
    }
    
    var employeeInfoArray: [CellDataCustomizer] {
        
        var arr = [CellDataCustomizer]()
        
        var customizer = CellDataCustomizer(label: "TAM ADI", data: fullName)
        arr.append(customizer)
        customizer = CellDataCustomizer(label: "ADI", data: firstName)
        arr.append(customizer)
        customizer = CellDataCustomizer(label: "SOYADI", data: lastName)
        arr.append(customizer)
        customizer = CellDataCustomizer(label: "ATA ADI", data: patronymic)
        arr.append(customizer)
        customizer = CellDataCustomizer(label: "ÜNVAN", data: address)
        arr.append(customizer)
        customizer = CellDataCustomizer(label: "ŞƏXSİYYƏT VƏSİQƏ", data: idDocument)
        arr.append(customizer)
        customizer = CellDataCustomizer(label: "PİN", data: pinCode)
        arr.append(customizer)
        
        return arr
        
    }
    
}
