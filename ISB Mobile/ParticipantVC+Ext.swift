//
// Created by  Simberg on 9/28/17.
// Copyright (c) 2017  Simberg. All rights reserved.
//

import UIKit

extension ParticipantVC {

    func createUI() {

        tableView.tableFooterView = UIView()
        tableView.register(ParticipantCell.self, forCellReuseIdentifier: Constants.participantCell)
        
        CustomProgressHUD.customize()

        if let title = navigationTitle {
            navigationItem.title = title
        }
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "close"), style: .plain, target: self, action: #selector(close))
        createView()

    }

    private func createView() {

    }

}
