//
//  TerminationFormVC+Ext.swift
//  ISB Mobile
//
//  Created by Jeyhun Danyalov on 9/21/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

extension TerminationFormVC {
    
    func createUI() {
        view.backgroundColor = UIColor.white
        navigationItem.title = "XİTAM ƏMƏLİYYATI"
        let backItem = UIBarButtonItem()
        backItem.title = " "
        navigationItem.backBarButtonItem = backItem
//        hideKeyboardWhenTappedAround()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "close"), style: .plain, target: self, action: #selector(TerminationFormVC.backAction))
        
        CustomProgressHUD.customize()
        
        createForm()
        
        if let number = contractNumber {
            contractNumberTxtField.text = number
        }
        
        contractNumberTxtField.addTarget(self, action: #selector(saveContractNumber), for: .editingDidEnd)
        
    }

    private func createForm() {
        
        let formView: UIView = {
            let view = UIView()
            view.backgroundColor = UIColor.white
            view.translatesAutoresizingMaskIntoConstraints = false
            view.layer.cornerRadius = 5
            view.layer.shadowRadius = 5
            view.layer.shadowOffset = CGSize.zero
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOpacity = 0.2
            return view
        }()
        view.addSubview(formView)
        
        contractNumberTxtField = CustomTextField.createCustomTxtField(placeholder: "AC648513", textSize: 20)
        contractNumberTxtField.autocapitalizationType = .allCharacters
        contractNumberTxtField.delegate = self
        formView.addSubview(contractNumberTxtField)
        
        let contractNumberLbl = UILabel.createLabel(withText: "MÜQAVİLƏ NÖMRƏSİ", andSize: 12)
        formView.addSubview(contractNumberLbl)
        
        searchBtn = UIButton.createCustomButton(text: "AXTAR", textSize: 20)
        searchBtn.addTarget(self, action: #selector(TerminationFormVC.searchContract), for: .touchUpInside)
        formView.addSubview(searchBtn)
        
        // init constraints
        formView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        formView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        formView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.85).isActive = true
        formView.heightAnchor.constraint(greaterThanOrEqualToConstant: 100).isActive = true
        
        contractNumberTxtField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        contractNumberTxtField.widthAnchor.constraint(equalTo: formView.widthAnchor, constant: -40).isActive = true
        contractNumberTxtField.centerXAnchor.constraint(equalTo: formView.centerXAnchor).isActive = true
        contractNumberTxtField.topAnchor.constraint(equalTo: formView.topAnchor, constant: 25).isActive = true
        
        contractNumberLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
        contractNumberLbl.leadingAnchor.constraint(equalTo: contractNumberTxtField.leadingAnchor).isActive = true
        contractNumberLbl.trailingAnchor.constraint(equalTo: contractNumberTxtField.trailingAnchor).isActive = true
        contractNumberLbl.topAnchor.constraint(equalTo: contractNumberTxtField.topAnchor).isActive = true
        
        searchBtn.leadingAnchor.constraint(equalTo: contractNumberTxtField.leadingAnchor).isActive = true
        searchBtn.trailingAnchor.constraint(equalTo: contractNumberTxtField.trailingAnchor).isActive = true
        searchBtn.topAnchor.constraint(equalTo: contractNumberTxtField.bottomAnchor, constant: 20).isActive = true
        searchBtn.heightAnchor.constraint(equalTo: contractNumberTxtField.heightAnchor).isActive = true
        searchBtn.bottomAnchor.constraint(equalTo: formView.bottomAnchor, constant: -20).isActive = true
        
    }
    
}
