//
//  GreenCardNewBlankVC+Ext.swift
//  ISB Mobile
//
//  Created by  Simberg on 10/10/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

extension GreenCardNewBlankVC {
    
    func createUI() {
        
//        hideKeyboardWhenTappedAround()
        CustomProgressHUD.customize()
        
        view.backgroundColor = UIColor.white
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "close"), style: .plain, target: self, action: #selector(GreenCardNewBlankVC.cancelOperation))
        
        datePicker.minimumDate = Date()
        datePicker.datePickerMode = .date
        datePicker.locale = Locale(identifier: "AZE")
        datePicker.setValue(UIColor.mainAppColor, forKeyPath: "textColor")
        datePicker.setValue(false, forKeyPath: "highlightsToday")
        
        blankSeriesPicker.dataSource = self
        blankSeriesPicker.delegate = self
        
        createView()
        
        if let navTitle = userDefaults.string(forKey: Constants.navigationTitle) {
            navigationTitle = navTitle
        }
        if let navSubtitle = userDefaults.string(forKey: Constants.navigationSubtitle) {
            navigationSubTitle = navSubtitle
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    func createView() {
        
        let formView: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.white
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOpacity = 0.2
            view.layer.shadowOffset = CGSize.zero
            view.layer.shadowRadius = 5
            view.layer.cornerRadius = 5
            return view
        }()
        view.addSubview(formView)
        
        formView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        formView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        formView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.85).isActive = true
        formView.heightAnchor.constraint(greaterThanOrEqualToConstant: 100).isActive = true
        
        
        blankSeriesPicker.translatesAutoresizingMaskIntoConstraints = false
        blankSeriesPicker.backgroundColor = UIColor(red:0.94, green:0.95, blue:0.97, alpha:1.0)
        blankSeriesPicker.layer.cornerRadius = 5
        formView.addSubview(blankSeriesPicker)
        
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        formView.addSubview(datePicker)
        
        let datePickerLbl: UILabel = {
            let label = UILabel()
            label.translatesAutoresizingMaskIntoConstraints = false
            label.text = "Qüvvəyə minmə tarixi"
            label.font = UIFont(name: Constants.helveticaBold, size: 15)
            label.textColor = UIColor.mainTextColor
            return label
        }()
        formView.addSubview(datePickerLbl)
        
        blankNumberTxtField = CustomTextField.createCustomTxtField(placeholder: "6546534", textSize: 20)
        blankNumberTxtField.delegate = self
        formView.addSubview(blankNumberTxtField)
        
        let blankLbl = UILabel.createLabel(withText: "BLANK SERİYASI", andSize: 12)
        formView.addSubview(blankLbl)
        
        let searchBtn = UIButton.createCustomButton(text: "YADDA SAXLA", textSize: 20)
        searchBtn.addTarget(self, action: #selector(GreenCardNewBlankVC.sendBlankInfo), for: .touchUpInside)
        formView.addSubview(searchBtn)
        
        
        blankSeriesPicker.topAnchor.constraint(equalTo: formView.topAnchor, constant: 20).isActive = true
        blankSeriesPicker.centerXAnchor.constraint(equalTo: formView.centerXAnchor).isActive = true
        blankSeriesPicker.heightAnchor.constraint(equalToConstant: 80).isActive = true
        blankSeriesPicker.widthAnchor.constraint(equalTo: formView.widthAnchor, multiplier: 0.8).isActive = true
        
        datePickerLbl.leadingAnchor.constraint(equalTo: blankSeriesPicker.leadingAnchor).isActive = true
        datePickerLbl.topAnchor.constraint(equalTo: blankSeriesPicker.bottomAnchor, constant: 20).isActive = true
        
        datePicker.topAnchor.constraint(equalTo: datePickerLbl.bottomAnchor, constant: 10).isActive = true
        datePicker.widthAnchor.constraint(equalTo: blankSeriesPicker.widthAnchor).isActive = true
        datePicker.heightAnchor.constraint(equalTo: blankSeriesPicker.heightAnchor).isActive = true
        datePicker.centerXAnchor.constraint(equalTo: blankSeriesPicker.centerXAnchor).isActive = true
        
        blankNumberTxtField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        blankNumberTxtField.centerXAnchor.constraint(equalTo: blankSeriesPicker.centerXAnchor).isActive = true
        blankNumberTxtField.widthAnchor.constraint(equalTo: blankSeriesPicker.widthAnchor).isActive = true
        blankNumberTxtField.topAnchor.constraint(equalTo: datePicker.bottomAnchor, constant: 15).isActive = true
        
        blankLbl.heightAnchor.constraint(equalToConstant: 21).isActive = true
        blankLbl.leadingAnchor.constraint(equalTo: blankNumberTxtField.leadingAnchor).isActive = true
        blankLbl.trailingAnchor.constraint(equalTo: blankNumberTxtField.trailingAnchor).isActive = true
        blankLbl.topAnchor.constraint(equalTo: blankNumberTxtField.topAnchor).isActive = true
        
        searchBtn.widthAnchor.constraint(equalTo: blankSeriesPicker.widthAnchor).isActive = true
        searchBtn.centerXAnchor.constraint(equalTo: formView.centerXAnchor).isActive = true
        searchBtn.topAnchor.constraint(equalTo: blankNumberTxtField.bottomAnchor, constant: 15).isActive = true
        searchBtn.heightAnchor.constraint(equalTo: blankNumberTxtField.heightAnchor).isActive = true
        searchBtn.bottomAnchor.constraint(equalTo: formView.bottomAnchor, constant: -20).isActive = true
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height/2
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0{
                self.view.frame.origin.y += keyboardSize.height/2
            }
        }
    }
    
    
}
