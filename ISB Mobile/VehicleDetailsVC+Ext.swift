//
//  VehicleDetailsVC+Ext.swift
//  ISB Mobile
//
//  Created by  Simberg on 9/20/17.
//  Copyright © 2017  Simberg. All rights reserved.
//

import UIKit

extension VehicleDetailsVC {
    
    func creatUI() {

        navigationItem.hidesBackButton = true
        CustomProgressHUD.customize()
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "close"), style: .plain, target: self, action: #selector(VehicleDetailsVC.cancelOperation))
        
        if let navTitle = userDefaults.string(forKey: Constants.navigationTitle) {
            if navTitle != "" {
                navigationTitle = navTitle
            }
        }
        if let navSubtitle = userDefaults.string(forKey: Constants.navigationSubtitle) {
            if navSubtitle != "" {
                navigationSubTitle = navSubtitle
            }
        }
        
        navigationItem.createCustomNavigationItem(title: navigationTitle, subTitle: navigationSubTitle)
        
        view.backgroundColor = UIColor.white
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(VehicleDetailsCell.self, forCellReuseIdentifier: Constants.vehicleInfoCell)
        tableView.tableFooterView = UIView()
        tableView.separatorInset.left = 20
        tableView.separatorInset.right = 20
        
        createMainView()
        
    }

    private func createMainView() {
        
        wrongDataBtn = UIButton.createCustomButton(text: "YANLIŞ MƏLUMAT", textSize: 20)
        wrongDataBtn.layer.cornerRadius = 0
        wrongDataBtn.addTarget(self, action: #selector(VehicleDetailsVC.mismatchVehicleData), for: .touchUpInside)
        view.addSubview(wrongDataBtn)        
        
        confirmBtn = UIButton.createCustomButton(text: "TƏSDİQLƏ", textSize: 20)
        confirmBtn.layer.cornerRadius = 0
        confirmBtn.addTarget(self, action: #selector(VehicleDetailsVC.confirmAction), for: .touchUpInside)
        view.addSubview(confirmBtn)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(tableView)
        
        let separator: UIView = {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            view.backgroundColor = UIColor.white
            return view
        }()
        wrongDataBtn.addSubview(separator)
        
        // init constraints
        wrongDataBtn.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.07).isActive = true
        wrongDataBtn.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5).isActive = true
        wrongDataBtn.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        wrongDataBtn.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        separator.centerYAnchor.constraint(equalTo: wrongDataBtn.centerYAnchor).isActive = true
        separator.widthAnchor.constraint(equalToConstant: 1).isActive = true
        separator.rightAnchor.constraint(equalTo: wrongDataBtn.rightAnchor).isActive = true
        separator.heightAnchor.constraint(equalTo: wrongDataBtn.heightAnchor, multiplier: 0.6).isActive = true
        
        confirmBtn.heightAnchor.constraint(equalTo: wrongDataBtn.heightAnchor).isActive = true
        confirmBtn.widthAnchor.constraint(equalTo: wrongDataBtn.widthAnchor).isActive = true
        confirmBtn.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        confirmBtn.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        
        tableView.topAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: wrongDataBtn.topAnchor).isActive = true
        tableView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        tableView.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
        
    }
    
}
